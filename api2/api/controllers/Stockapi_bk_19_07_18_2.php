<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');
class Stockapi extends CI_Controller
{ 
	public function add_new_stock_trade()
	{
		if(isset($_POST['cate_id']) && isset($_POST['symbol']) && isset($_POST['recommended_price']) && isset($_POST['max_price']) && isset($_POST['stop_loss']) && isset($_POST['target_1']) && isset($_POST['target_2']) && isset($_POST['stock_type']))
		{
			$cate_id=$_POST['cate_id'];
			$stock_type=$_POST['stock_type'];
			$symbol=$_POST['symbol'];
			$recommended_price=$_POST['recommended_price'];
			$max_price=$_POST['max_price'];
			$stop_loss=$_POST['stop_loss'];
			$target_1=$_POST['target_1'];
			$target_2=$_POST['target_2'];

			$userid=$this->db->get('wp_users')->result_array();
			$data=array(
						'cate_id' =>$cate_id,
						'stock_type' =>$stock_type,
						'symbol'=>$symbol,
						'recommended_price'=>$recommended_price, 
						'max_price'=>$max_price, 
						'stop_loss'=>$stop_loss, 
						'target_1'=>$target_1, 
						'target_2'=>$target_2, 
						);
			$insert=$this->db->insert('stock',$data);
			$stock=$this->db->get('stock')->result_array();

			if($stock){
				foreach ($stock as $value) {

					
					$value2['StockID']=$value['id'];
                                        $value2['Userid']=$userid[0]['ID'];
					$value2['Category']=$cate_id;
					$value2['StockType']=$stock_type;
					$value2['Symbol']=$symbol;
					$value2['RecommendedPrice']=$recommended_price;
					$value2['MaxPrice']=$max_price;
					$value2['StopLoss']=$stop_loss;
					$value2['Target1']=$target_1;
					$value2['Target2']=$target_2;
					
					
				}
				$data2[] = $value2;
		    	$msg="Stock list get Successfully ";
				$suc=1;
				echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data2).'}';
			}

		}else{
		    $suc=0;
			$msg="No any stock list";
			echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
		}
	}

	public function add_trade_single_call()
	{
		if(isset($_POST['Category']) && isset($_POST['StockType']) && isset($_POST['Symbol']) && isset($_POST['RecommendedPrice']) && isset($_POST['MaxPrice']) && isset($_POST['StopLoss']) && isset($_POST['Target1']) && isset($_POST['Target2']) && isset($_POST['ExpirationDate']) && isset($_POST['Quantity']))
		{
			$cate_id=$_POST['Category'];
			$StockType=$_POST['StockType'];
			$Symbol=$_POST['Symbol'];
			$RecommendedPrice=$_POST['RecommendedPrice'];
			$MaxPrice=$_POST['MaxPrice'];
			$StopLoss=$_POST['StopLoss'];
			$Target1=$_POST['Target1'];
			$Target2=$_POST['Target2'];
			$ExpirationDate=$_POST['ExpirationDate'];
			$Quantity=$_POST['Quantity'];

			$userid=$this->db->get('wp_users')->result_array();

			$insert=array(
				'Category' => $cate_id,
				'StockType' => $StockType,
				'Symbol' => $Symbol,
				'RecommendedPrice' => $RecommendedPrice,
				'MaxPrice' => $MaxPrice,
				'StopLoss' => $StopLoss,
				'Target1' => $Target1,
				'Target2' => $Target2,
				'ExpirationDate' => $ExpirationDate,
				'Quantity' => $Quantity,
				);

			$trade=$this->db->insert('single_option_trade',$insert);
			
			$trade_data=$this->db->get('single_option_trade')->result_array();
			if($trade_data)
			{
				foreach ($trade_data as $key) {

					$key['Category']=$cate_id;
					$key['Userid']=$userid[0]['ID'];
				}
				$data2[] = $key;
		    	$msg="singel cell trade list get Successfully ";
				$suc=1;
				echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data2).'}';
			}else
			{
				$suc=0;
				$msg="No any singel cell trade list";
				echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
			}
		}
	}

	public function add_trade_vertical_call()
	{
		if(isset($_POST['Category']) && isset($_POST['StockType']) && isset($_POST['Symbol']) && isset($_POST['StrikeInterval']) && isset($_POST['BuyStrike']) && isset($_POST['SellStrike']) && isset($_POST['NetDebtLimitPrice']) && isset($_POST['StopLoss']) && isset($_POST['ExpirationDate']) && isset($_POST['Quantity']) && isset($_POST['T1Stock']) && isset($_POST['T1Option']) && isset($_POST['T2Stock']) && isset($_POST['T2Option']))
		{
			$cate_id=$_POST['Category'];
			$StockType=$_POST['StockType'];
			$Symbol=$_POST['Symbol'];
			$StrikeInterval=$_POST['StrikeInterval'];
			$BuyStrike=$_POST['BuyStrike'];
			$SellStrike=$_POST['SellStrike'];
			$NetDebtLimitPrice=$_POST['NetDebtLimitPrice'];
			$StopLoss=$_POST['StopLoss'];
			$ExpirationDate=$_POST['ExpirationDate'];
			$Quantity=$_POST['Quantity'];
			$T1Stock=$_POST['T1Stock'];
			$T1Option=$_POST['T1Option'];
			$T2Stock=$_POST['T2Stock'];
			$T2Option=$_POST['T2Option'];

			$userid=$this->db->get('wp_users')->result_array();

			$insert=array(
				'Category' => $cate_id,
				'StockType' => $StockType,
				'Symbol' => $Symbol,
				'StrikeInterval' => $StrikeInterval,
				'BuyStrike' => $BuyStrike,
				'SellStrike' => $SellStrike,
				'NetDebtLimitPrice' => $NetDebtLimitPrice,
				'StopLoss' => $StopLoss,
				'ExpirationDate' => $ExpirationDate,
				'Quantity' => $Quantity,
				'T1Stock' => $T1Stock,
				'T1Option' => $T1Option,
				'T2Stock' => $T2Stock,
				'T2Option' => $T2Option,
				);

			$trade=$this->db->insert('single_option_trade_vertical',$insert);
			
			$trade_data=$this->db->get('single_option_trade_vertical')->result_array();
			if($trade_data)
			{
				foreach ($trade_data as $key) {

					$key['Userid']=$userid[0]['ID'];
					//$key['ID']=$key['id'];
					$key['Category']=$cate_id;
					$data2[] = $key;
				}
				
		    	$msg="vertical cell trade list get Successfully ";
				$suc=1;
				echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data2).'}';
			}else
			{
				$suc=0;
				$msg="No any vertical cell trade list";
				echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
			}
		}
	}
}
?>