<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Closeoutapi extends CI_Controller
{
    public function stock_trade()
    {
        if (isset($_POST['UserId']) && isset($_POST['TradeId']) && isset($_POST['Symbol']) && isset($_POST['RecommendedPrice']) && isset($_POST['MinPrice']) && isset($_POST['Description']) && isset($_POST['CreateTime'])) {
            $insert['UserId'] = $UserId = $_POST['UserId'];
            $insert['TradeId'] = $TradeId = $_POST['TradeId'];
            $insert['Symbol'] = $Symbol = $_POST['Symbol'];
            $insert['RecommendedPrice'] = $RecommendedPrice = $_POST['RecommendedPrice'];
            $insert['MinPrice'] = $MinPrice = $_POST['MinPrice'];
            $insert['Description'] = $Description = $_POST['Description'];
            $insert['CreateTime'] = date('Y-m-d H:i:s');
            $insert['status'] = 5; // rhl 12-10-2018 //

            $this->db->insert('CloseOut', $insert);

            $insertId = $this->db->insert_id();
            if ($insertId) {
                $data3['status'] = $insert['status'];
                $data3['UpdateTime'] =date('Y-m-d H:i:s');

                $this->db->where('id', $insert['TradeId']);
                $update=$this->db->update('Stock', $data3);
            }

            $this->db->select('id as CloseOutId,UserId,TradeId,Symbol,RecommendedPrice,MinPrice,Description,CreateTime,status');
            $closeout_data = $this->db->get_where('CloseOut', array('id'=>$insertId))->result_array();

            if ($closeout_data) {
                $data = $closeout_data;

                $msg="Stock Trade Successfully Closed Out";
                $suc=1;
                echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data).'}';
            } else {
                $msg="Stock Trade Not Closed Out";
                $suc=1;
                echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
            }
        } else {
            $msg="Please Enter vadid data";
            $suc=0;
            echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
        }
    }
    public function singal_option_trade()
    {
        if (isset($_POST['UserId']) && isset($_POST['TradeId']) && isset($_POST['Symbol']) && isset($_POST['StockType']) && isset($_POST['MinPrice']) && isset($_POST['Description']) && isset($_POST['ExpirationDate']) && isset($_POST['MaxPrice']) && isset($_POST['CreateTime']) && isset($_POST['StrikePrice'])) {
            $insert['UserId'] = $UserId = $_POST['UserId'];
            $insert['TradeId'] = $TradeId = $_POST['TradeId'];
            $insert['Symbol'] = $Symbol = $_POST['Symbol'];
            $insert['StockType'] = $StockType = $_POST['StockType'];
            $insert['MinPrice'] = $MinPrice = $_POST['MinPrice'];
            $insert['Description'] = $Description = $_POST['Description'];
            $insert['ExpirationDate'] = $ExpirationDate = $_POST['ExpirationDate'];
            $insert['MaxPrice'] = $MaxPrice = $_POST['MaxPrice'];
            $insert['CreateTime'] = date('Y-m-d H:i:s');
            $insert['StrikePrice'] = $StrikePrice = $_POST['StrikePrice'];
            $insert['status'] = 5; // rhl 12-10-2018 //

            $this->db->insert('CloseOut', $insert);
            $insertId = $this->db->insert_id();

            if ($insertId) {
                $data3['status'] = $insert['status'];
                $data3['UpdateTime'] =date('Y-m-d H:i:s');

                $this->db->where('id', $insert['TradeId']);
                $update=$this->db->update('Stock', $data3);
            }

            $this->db->select('id as CloseOutId,UserId,TradeId,Symbol,StockType,MinPrice,MaxPrice,ExpirationDate,Description,CreateTime,StrikePrice,status');
            $closeout_data = $this->db->get_where('CloseOut', array('id'=>$insertId))->result_array();

            if ($closeout_data) {
                $data = $closeout_data;

                $msg="Stock Trade Successfully Closed Out";
                $suc=1;
                echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data).'}';
            } else {
                $msg="Stock Trade Not Closed Out";
                $suc=1;
                echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
            }
        } else {
            $msg="Please Enter vadid data";
            $suc=0;
            echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
        }
    }
    public function vertical_spread()
    {
        if (isset($_POST['UserId']) && isset($_POST['TradeId']) && isset($_POST['Symbol']) && isset($_POST['StockType']) && isset($_POST['Description']) && isset($_POST['ExpirationDate']) && isset($_POST['NetCreditLimitPrice']) && isset($_POST['CreateTime']) && isset($_POST['MinPrice']) && isset($_POST['MaxPrice']) && isset($_POST['StrikePrice'])&& isset($_POST['BuyStrike']) && isset($_POST['SellStrike'])) {
            $insert['UserId'] = $UserId = $_POST['UserId'];
            $insert['TradeId'] = $TradeId = $_POST['TradeId'];
            $insert['Symbol'] = $Symbol = $_POST['Symbol'];
            $insert['StockType'] = $StockType = $_POST['StockType'];
            $insert['Description'] = $Description = $_POST['Description'];
            $insert['ExpirationDate'] = $ExpirationDate = $_POST['ExpirationDate'];
            $insert['NetCreditLimitPrice'] = $NetCreditLimitPrice = $_POST['NetCreditLimitPrice'];
            $insert['CreateTime'] = date('Y-m-d H:i:s');
            $insert['MinPrice'] = $MinPrice = $_POST['MinPrice'];
            $insert['MaxPrice'] = $MaxPrice = $_POST['MaxPrice'];
            $insert['StrikePrice'] = $StrikePrice = $_POST['StrikePrice'];
            $insert['status'] = 5; // rhl 12-10-2018 //

            $this->db->insert('CloseOut', $insert);
            $insertId = $this->db->insert_id();

            if ($insertId) {
                $data3['status'] = $insert['status'];
                $data3['UpdateTime'] =date('Y-m-d H:i:s');
                $data3['BuyStrike']  = $_POST['BuyStrike'];
                $data3['SellStrike']  = $_POST['SellStrike'];

                $this->db->where('id', $insert['TradeId']);
                $update=$this->db->update('Stock', $data3);
            }

            $this->db->select('id as CloseOutId,UserId,TradeId,Symbol,StockType,ExpirationDate,NetCreditLimitPrice,Description,CreateTime,MinPrice,MaxPrice,StrikePrice,status');
            $closeout_data = $this->db->get_where('CloseOut', array('id'=>$insertId))->result_array();

            if ($closeout_data) {
                $data = $closeout_data;

                $msg="Stock Trade Successfully Closed Out";
                $suc=1;
                echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data).'}';
            } else {
                $msg="Stock Trade Not Closed Out";
                $suc=1;
                echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
            }
        } else {
            $msg="Please Enter valid data";
            $suc=0;
            echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
        }
    }
    public function closeout_list()
    {
        
        $this->db->where('view_status', 0);
        $this->db->where_in('status', array('3','5','7'));
        $this->db->order_by('CreateTime', 'desc');
        $stocklist=$this->db->get('Stock')->result_array();
        $data2 = array();
        if ($stocklist) {
            foreach ($stocklist as $row) {
                //$row['CloseOut'] = $row['Stop_Loss'] = array();
                
                $row['PartialCloseOut'] = $row['CloseOut'] = $row['Stop_Loss'] = array();
                
                $this->db->select('id as CloseOutId,UserId,TradeId,Symbol,RecommendedPrice,MinPrice,Description,CreateTime,MaxPrice,StockType,view_status,UpdateTime,ExpirationDate,NetCreditLimitPrice,status');
                $this->db->where('TradeId', $row['id']);
                $sql = $this->db->get('PartialCloseOut');
                $partial_closeout_data = $sql->result_array();
                if ($partial_closeout_data) {
                    $row['PartialCloseOut']=$partial_closeout_data;
                }
                $row['CategoryName']='';
                $this->db->select('CategoryName');
                $this->db->where('id', $row['CategoryId']);
                $this->db->where('view_status', 0);
                $category=$this->db->get('Category')->result_array();
                if ($category) {
                    $row['CategoryName']=$category[0]['CategoryName'];
                }
                // StopLoss //
                $this->db->select('id as StopLossId,Symbol,StopLowPrice,StopHighPrice,Description,TradeId,UserId,CreateTime,StockType,ExpirationDate,NetCreditLimitPrice,view_status,UpdateTime,status'); // rhl 12-10-2018 //
                $this->db->where('TradeId', $row['id']);
                $stoploss=$this->db->get('StopLoss')->result_array();
                if ($stoploss) {
                    $row['Stop_Loss']=$stoploss;
                }
                // end StopLoss //

                // CloseOut //
                $this->db->select('id as CloseOutId,UserId,TradeId,Symbol,RecommendedPrice,MinPrice,Description,CreateTime,MaxPrice,StockType,ExpirationDate,NetCreditLimitPrice,view_status,UpdateTime,status'); // rhl 12-10-2018 //
                $this->db->where('TradeId', $row['id']);
                $sql = $this->db->get('CloseOut');
                $closeout_data = $sql->result_array();
                if ($closeout_data) {
                    $row['CloseOut']=$closeout_data;
                    $data2[] = $row;
                } else {
                    // StopLoss //
                    $this->db->select('id as StopLossId,Symbol,StopLowPrice,StopHighPrice,Description,TradeId,UserId,CreateTime,StockType,ExpirationDate,NetCreditLimitPrice,view_status,UpdateTime');
                    $this->db->where('TradeId', $row['id']);
                    $stoploss=$this->db->get('StopLoss')->result_array();
                    if ($stoploss) {
                        $data2[] = $row;
                    }
                    // end StopLoss //
                }
                // end CloseOut //
                //$data2[] = $row;
            }
        }
        if ($data2) {
                $msg="CloseOut list get Successfully ";
            $suc=1;
            echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data2).'}';
        } else {
            $suc=0;
            $msg="No any CloseOut list";
            echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
        }
    }
}
