<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Commentaryapi extends CI_Controller
{

    public function commentary()
    {
        if (isset($_POST['UserId']) && isset($_POST['TradeId']) && isset($_POST['CommentaryDescription']) && isset($_POST['CommentaryId']) && isset($_POST['Subject'])) {
            $CommentaryId=$_POST['CommentaryId'];

            $insert['UserId']=$UserId=$_POST['UserId'];
            $insert['TradeId']=$TradeId=$_POST['TradeId'];
            $insert['CommentaryDescription']=$CommentaryDescription=$_POST['CommentaryDescription'];
            $insert['Subject']=$Subject=$_POST['Subject'];
            $insert['UpdateTime']=$UpdateTime=date('Y-m-d H:i:s');

            if ($CommentaryId==0) {
                $insert['CreateTime']=$CreateTime=date('Y-m-d H:i:s');
                $this->db->insert('Commentary', $insert);
                $insertId = $this->db->insert_id();
                $data3['UpdateTime'] =date('Y-m-d H:i:s');

                $this->db->where('id', $insert['TradeId']);
                $update=$this->db->update('Stock', $data3);

                $this->db->select('id as CommentaryId,UserId,TradeId,Subject,CommentaryDescription,CreateTime,UpdateTime');
                
                $commentary_data = $this->db->get_where('Commentary', array('id'=>$insertId))->row_array();
                

                if ($commentary_data) {
                    $data = $commentary_data;

                    $msg="Commentary Successfully Added.";
                    $suc=1;
                    echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data).'}';
                } else {
                    $msg="Commentary not Added.";
                    $suc=1;
                    echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
                }
            } else {
                $this->db->where('id', $CommentaryId);
                $up=$this->db->update('Commentary', $insert);

                if ($up==1) {
                    $this->db->select('id as CommentaryId,UserId,Subject,CommentaryDescription,CreateTime');
                    //$this->db->order_by('CreateTime','desc');
                    $commentary_data = $this->db->get_where('Commentary', array('id'=>$CommentaryId,'view_status'=>0))->row_array();
                    //print_r($commentary_data);
                     $data3['UpdateTime'] =date('Y-m-d H:i:s');

                    $this->db->where('id', $insert['TradeId']);
                    $update=$this->db->update('Stock', $data3);
                    if ($commentary_data) {
                        $data = $commentary_data;

                        $msg="Commentary Successfully Edited.";
                        $suc=1;
                        echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data).'}';
                    } else {
                        $msg="Commentary not Edited.";
                        $suc=1;
                        echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
                    }
                }
            }
        } else {
            $msg="Please Enter valid data";
            $suc=0;
            echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
        }
    }

    public function commentary_list()
    {
        $this->db->select('id as CommentaryId,Subject,CommentaryDescription,UserId,CreateTime');
        $this->db->where('view_status', 0);
        $this->db->order_by('CreateTime', 'desc');
        $commentary_list=$this->db->get('Commentary')->result_array();

        if ($commentary_list) {
            $data = $commentary_list;

            $msg="Commentary Successfully Edited.";
            $suc=1;
            echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data).'}';
        } else {
            $msg="Commentary not Edited.";
            $suc=1;
            echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
        }
    }
}
