<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');
class Login extends CI_Controller
{ 
	
		/* FUNCTION: Loads Homepage*/
		public function index()
		{ 

			if(isset($_POST['user_email']) && isset($_POST['password'])){
			$user_email = $_POST['user_email'];
			//$password =  $this->hash($_POST['password']);

			include 'class-phpass.php';

			//$conn = mysqli_connect('localhost','root','','1234investor');

			$wp_hasher = new PasswordHash(16, true);

			$password = $_POST['password'];

			//echo $password;exit;
			
			$data = $data1 = $data2 = array();
		
			if(isset($password)){

				//$this->db->select('studentID,name,email,phone,password,active');

				$this->db->select('ID,user_login,user_pass,user_email');
				$query = $this->db->get_where('wp_users', array(
					'user_email'=>$user_email
					//'active'=>1
					));
				$result = $query->result_array();
				
				if($result){
					foreach ($result as $value) {
						if($wp_hasher->CheckPassword($password, $value['user_pass'])) {

							$query1 = $this->db->get_where('wp_usermeta', array(
								'user_id'=>$value['ID'],
							));
							$result1 = $query1->result_array();
							foreach ($result1 as $row1) {
								if($row1['meta_key'] == 'first_name'){
									$value['FirstName'] = $row1['meta_value'];
								}elseif($row1['meta_key']=='last_name'){
									$value['LastName']=$row1['meta_value'];
								}elseif($row1['meta_key']=='shipping_address_1'){
									$value['Address']=$row1['meta_value'];
								}elseif($row1['meta_key']=='transax_recurring_id'){
									$value['transax_recurring_id']=$row1['meta_value'];
								}elseif($row1['meta_key']=='billing_phone'){
									$value['Phone']=$row1['meta_value'];
								}elseif($row1['meta_key']=='wp_capabilities'){
									$datax = unserialize($row1['meta_value']);
									$d = array_keys($datax);
									$value['Role']= $d[0];
								}elseif($row1['meta_key']=='subscribe_history'){
									$subscribe_history = unserialize($row1['meta_value']);
									$subscribe_history = array_reverse($subscribe_history);
									$planId = $subscribe_history[0]['plan_id'];
									switch ($planId) {
										case '3':
											$value['plan'] = 'Platinium';
											break;
										case '2':
											$value['plan'] = 'Gold';
											break;
										default:
											$value['plan'] = 'Silver';
											break;
									}
								}
								
							}
							if($value['Role'] != 'administrator' && (!isset($value['transax_recurring_id']) || $value['transax_recurring_id']=='')){
								 $suc=0;
								$msg="Your MemberShip is not active please reactivate now.";
								echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
								die;
							}
							$value['UserId']=$value['ID'];
							$value['Email']=$value['user_email'];
							$data[] = $value;
					    	$msg="Login Successfully ";
							$suc=1;
							echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data).'}';
						} else {
						    $suc=0;
							$msg="Invalid Username or Password";
							echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
						}
					}	
					exit;			
				}
				else
				{
					$suc=0;
					$msg="Invalid Username or Password";
					echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
				}
				
				
			}else{
					$suc=0;
					$msg="Password is required";
					echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
			}
			}else{
					$suc=0;
					$msg="Email id and password are required";
					echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
			}
		}

		public function edit_profile(){
			//echo "hello";
			if(isset($_POST['UserId'])  && isset($_POST['FirstName']) && isset($_POST['LastName']) && isset($_POST['Phone']) && isset($_POST['Address'])){

				$id = $_POST['UserId'];
				// $user_email = $_POST['Email'];
				$first_name = $_POST['FirstName'];
				$last_name = $_POST['LastName'];
				$shipping_address_1 = $_POST['Address'];
				$billing_phone = $_POST['Phone'];

				//$this->db->where('ID',$id);
				//$this->db->update('wp_users',array('user_email'=>$user_email));
				
				$this->db->select('ID,user_email');
				$update_data = $this->db->get_where('wp_users',array('ID'=>$id))->result_array();

				$data['UserId'] = $update_data[0]['ID'];
				$data['Email'] = $update_data[0]['user_email'];

				$my_meta = $this->db->get_where('wp_usermeta',array('user_id'=>$id))->result_array();

				foreach ($my_meta as $meta_val) {
					if($meta_val['meta_key'] == 'first_name'){
						$m_id = $meta_val['umeta_id'];
						$this->db->where('umeta_id',$m_id);
						$this->db->update('wp_usermeta',array('meta_value'=>$first_name));
					}elseif($meta_val['meta_key']=='last_name'){
						$m_id = $meta_val['umeta_id'];
						$this->db->where('umeta_id',$m_id);
						$this->db->update('wp_usermeta',array('meta_value'=>$last_name));
					}elseif($meta_val['meta_key']=='shipping_address_1'){
						$m_id = $meta_val['umeta_id'];
						$this->db->where('umeta_id',$m_id);
						$this->db->update('wp_usermeta',array('meta_value'=>$shipping_address_1));
					}elseif($meta_val['meta_key']=='billing_phone'){
						$m_id = $meta_val['umeta_id'];
						$this->db->where('umeta_id',$m_id);
						$this->db->update('wp_usermeta',array('meta_value'=>$billing_phone));
					}
				}
				$updated_meta = $this->db->get_where('wp_usermeta',array('user_id'=>$id))->result_array();

				if($updated_meta){
					foreach ($updated_meta as $datas) {
						if($datas['meta_key'] == 'first_name'){
							$data['FirstName'] = $datas['meta_value'];
						}elseif($datas['meta_key']=='last_name'){
							$data['LastName']=$datas['meta_value'];
						}elseif($datas['meta_key']=='shipping_address_1'){
							$data['Address']=$datas['meta_value'];
						}elseif($datas['meta_key']=='billing_phone'){
							$data['Phone']=$datas['meta_value'];
						}
					}

			    	$msg="Data Updated Successfully ";
					$suc=1;
					echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data).'}';
				}
				else{
					$suc=0;
					$msg="Data not Updated";
					echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
				}
			}else{
				$suc=0;
				$msg="Data not Updated";
				echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
			}
		}
}
?>