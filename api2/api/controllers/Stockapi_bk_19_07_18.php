<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');
class Stockapi extends CI_Controller
{ 
	public function stock()
	{
		if(isset($_POST['cate_id']) && isset($_POST['symbol']) && isset($_POST['recommended_price']) && isset($_POST['max_price']) && isset($_POST['stop_loss']) && isset($_POST['target_1']) && isset($_POST['target_2']) && isset($_POST['stock_type']))
		{
			$cate_id=$_POST['cate_id'];
			$stock_type=$_POST['stock_type'];
			$symbol=$_POST['symbol'];
			$recommended_price=$_POST['recommended_price'];
			$max_price=$_POST['max_price'];
			$stop_loss=$_POST['stop_loss'];
			$target_1=$_POST['target_1'];
			$target_2=$_POST['target_2'];

			$data=array(
						'cate_id' =>$cate_id,
						'stock_type' =>$stock_type,
						'symbol'=>$symbol,
						'recommended_price'=>$recommended_price, 
						'max_price'=>$max_price, 
						'stop_loss'=>$stop_loss, 
						'target_1'=>$target_1, 
						'target_2'=>$target_2, 
						);
			$insert=$this->db->insert('stock',$data);


			$stock=$this->db->get('stock')->result_array();
			if($stock){
				foreach ($stock as $value) {
					//$data1=$value;
					$this->db->select('id,categories_name');
					$this->db->where('id',$cate_id);
					$cate=$this->db->get('categories');
					$categories_data=$cate->result_array();

					$value2['Category']=$categories_data[0]['categories_name'];
					$value2['StockType']=$stock_type;
					$value2['Symbol']=$symbol;
					$value2['RecommendedPrice']=$recommended_price;
					$value2['MaxPrice']=$max_price;
					$value2['StopLoss']=$stop_loss;
					$value2['Target1']=$target_1;
					$value2['Target2']=$target_2;
					
					
				}
				$data2[] = $value2;
		    	$msg="Stock list get Successfully ";
				$suc=1;
				echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data2).'}';
			}

		}else{
		    $suc=0;
			$msg="No any stock list";
			echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
		}
	}

}
?>