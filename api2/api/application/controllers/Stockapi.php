<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Stockapi extends CI_Controller
{

    public function add_new_stock_trade()
    {
        //echo "hello";
        if (isset($_POST['CategoryId']) && isset($_POST['Symbol']) && isset($_POST['RecommendedPrice']) && isset($_POST['MaxPrice']) && isset($_POST['StopLoss']) && isset($_POST['Target1']) && isset($_POST['Target2']) && isset($_POST['StockType']) && isset($_POST['TradeType']) && isset($_POST['UserId']) && isset($_POST['StockId'])) {
            $StockId=$_POST['StockId'];
            $CategoryId=$_POST['CategoryId'];
            $StockType=$_POST['StockType'];
            $Symbol=$_POST['Symbol'];
            $RecommendedPrice=$_POST['RecommendedPrice'];
            $MaxPrice=$_POST['MaxPrice'];
            $StopLoss=$_POST['StopLoss'];
            $Target1=$_POST['Target1'];
            $Target2=$_POST['Target2'];
            $TradeType=$_POST['TradeType'];
            $UserId=$_POST['UserId'];
            $CreateTime=$_POST['CreateTime'];

            $data=array(
                'CategoryId' =>$CategoryId,
                'StockType' =>$StockType,
                'Symbol'=>$Symbol,
                'RecommendedPrice'=>$RecommendedPrice,
                'MaxPrice'=>$MaxPrice,
                'StopLoss'=>$StopLoss,
                'Target1'=>$Target1,
                'Target2'=>$Target2,
                'TradeType'=>$TradeType,
                'UserId'=>$UserId,
                'CreateTime' =>date('Y-m-d H:i:s'),
                'UpdateTime' =>date('Y-m-d H:i:s'),
            );
            $userid=$this->db->get('wp_users')->result_array();
            
            if ($StockId==0) {
                $insert=$this->db->insert('Stock', $data);
                $insertId = $this->db->insert_id();

                $this->db->select('id as StockId,UserId,CategoryId,StockType,Symbol,RecommendedPrice,MaxPrice,StopLoss,Target1,Target2,TradeType');
                $this->db->where('id', $insertId);
                $stock=$this->db->get('Stock')->result_array();
            } else {
                $this->db->where('id', $StockId);
                $update=$this->db->update('Stock', $data);

                if ($update==1) {
                    $this->db->select('id as StockId,UserId,CategoryId,StockType,Symbol,RecommendedPrice,MaxPrice,StopLoss,Target1,Target2,TradeType');
                    $this->db->where('id', $StockId);
                    $stock=$this->db->get('Stock')->result_array();
                }
            }

            if ($stock) {
                foreach ($stock as $value) {
                    $data2[] = $value;
                }
                $msg="Stock list get Successfully ";
                $suc=1;
                echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data2).'}';
            } else {
                $msg="No Stock list";
                $suc=1;
                echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
            }
        } else {
            $suc=0;
            $msg="No any stock list";
            echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
        }
    }

    public function add_trade_single_call()
    {
        if (isset($_POST['Category']) && isset($_POST['StockType']) && isset($_POST['Symbol']) && isset($_POST['RecommendedPrice']) && isset($_POST['MaxPrice']) && isset($_POST['StopLoss']) && isset($_POST['Target1']) && isset($_POST['Target2']) && isset($_POST['ExpirationDate']) && isset($_POST['Quantity']) && isset($_POST['TradeType']) && isset($_POST['StockId']) && isset($_POST['StrikePrice'])) {
            $cate_id=$_POST['Category'];
            $StockType=$_POST['StockType'];
            $Symbol=$_POST['Symbol'];
            $RecommendedPrice=$_POST['RecommendedPrice'];
            $MaxPrice=$_POST['MaxPrice'];
            $StopLoss=$_POST['StopLoss'];
            $Target1=$_POST['Target1'];
            $Target2=$_POST['Target2'];
            $ExpirationDate=$_POST['ExpirationDate'];
            $Quantity=$_POST['Quantity'];
            $TradeType=$_POST['TradeType'];
            $StockId=$_POST['StockId'];
            $CreateTime=$_POST['CreateTime'];
            $StrikePrice=$_POST['StrikePrice'];
            // echo $ExpirationDate;
            $data=array(
                'CategoryId' => $cate_id,
                'StockType' => $StockType,
                'Symbol' => $Symbol,
                'RecommendedPrice' => $RecommendedPrice,
                'MaxPrice' => $MaxPrice,
                'StopLoss' => $StopLoss,
                'Target1' => $Target1,
                'Target2' => $Target2,
                'ExpirationDate' => $ExpirationDate,
                'Quantity' => $Quantity,
                'TradeType' => $TradeType,
                'CreateTime' =>date('Y-m-d H:i:s'),
                'UpdateTime' =>date('Y-m-d H:i:s'),
                'StrikePrice' =>$StrikePrice,
            );

            $userid=$this->db->get('wp_users')->result_array();

            if ($StockId==0) {
                $trade=$this->db->insert('Stock', $data);

                $insertId = $this->db->insert_id();
                
                $this->db->select('id as StockId,CategoryId,StockType,Symbol,RecommendedPrice,MaxPrice,StopLoss,Target1,Target2,ExpirationDate,Quantity,TradeType');
                $this->db->where('id', $insertId);
                $trade_data=$this->db->get('Stock')->result_array();
            } else {
                $this->db->where('id', $StockId);
                $update=$this->db->update('Stock', $data);

                if ($update==1) {
                    $this->db->select('id as StockId,CategoryId,StockType,Symbol,RecommendedPrice,MaxPrice,StopLoss,Target1,Target2,ExpirationDate,Quantity,TradeType');
                    $this->db->where('id', $StockId);
                    $trade_data=$this->db->get('Stock')->result_array();
                }
            }
            
            if ($trade_data) {
                foreach ($trade_data as $key) {
                    $category = $this->db->get_where('Category', array('id'=>$cate_id))->result_array();

                    //$key['CategoryId']=$category[0]['id'];
                    $key['CategoryName']=$category[0]['CategoryName'];
                    $key['UserId']=$userid[0]['ID'];
                    $data2[] = $key;
                }
                
                $msg="singel cell trade list get Successfully ";
                $suc=1;
                echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data2).'}';
            } else {
                $suc=1;
                $msg="No singel cell trade list..";
                echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
            }
        } else {
            $suc=0;
            $msg="No any singel cell trade list";
            echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
        }
    }

    public function add_trade_vertical_call()
    {
        if (isset($_POST['Category']) && isset($_POST['StockType']) && isset($_POST['Symbol']) && isset($_POST['StrikeInterval']) && isset($_POST['BuyStrike']) && isset($_POST['SellStrike']) && isset($_POST['NetDebtLimitPrice']) && isset($_POST['StopLoss']) && isset($_POST['ExpirationDate']) && isset($_POST['Quantity']) && isset($_POST['T1Stock']) && isset($_POST['T1Option']) && isset($_POST['T2Stock']) && isset($_POST['T2Option']) && isset($_POST['TradeType']) && isset($_POST['StockId'])) {
            $cate_id=$_POST['Category'];
            $StockType=$_POST['StockType'];
            $Symbol=$_POST['Symbol'];
            $StrikeInterval=$_POST['StrikeInterval'];
            $BuyStrike=$_POST['BuyStrike'];
            $SellStrike=$_POST['SellStrike'];
            $NetDebtLimitPrice=$_POST['NetDebtLimitPrice'];
            $StopLoss=$_POST['StopLoss'];
            $ExpirationDate=$_POST['ExpirationDate'];
            $Quantity=$_POST['Quantity'];
            $T1Stock=$_POST['T1Stock'];
            $T1Option=$_POST['T1Option'];
            $T2Stock=$_POST['T2Stock'];
            $T2Option=$_POST['T2Option'];
            $TradeType=$_POST['TradeType'];
            $StockId=$_POST['StockId'];
            $CreateTime=$_POST['CreateTime'];
            $data=array(
                'CategoryId' => $cate_id,
                'StockType' => $StockType,
                'Symbol' => $Symbol,
                'StrikeInterval' => $StrikeInterval,
                'BuyStrike' => $BuyStrike,
                'SellStrike' => $SellStrike,
                'NetDebtLimitPrice' => $NetDebtLimitPrice,
                'StopLoss' => $StopLoss,
                'ExpirationDate' => $ExpirationDate,
                'Quantity' => $Quantity,
                'T1Stock' => $T1Stock,
                'T1Option' => $T1Option,
                'T2Stock' => $T2Stock,
                'T2Option' => $T2Option,
                'TradeType' => $TradeType,
                'CreateTime' =>date('Y-m-d H:i:s'),
                'UpdateTime' =>date('Y-m-d H:i:s'),
            );
            $userid=$this->db->get('wp_users')->result_array();

            if ($StockId==0) {
                $trade=$this->db->insert('Stock', $data);

                $insertId = $this->db->insert_id();
                
                $this->db->select('id as StockId,CategoryId,StockType,Symbol,StrikeInterval,BuyStrike,SellStrike,NetDebtLimitPrice,StopLoss,ExpirationDate,Quantity,T1Stock,T1Option,T2Stock,T2Option,TradeType');
                $this->db->where('id', $insertId);
                $trade_data=$this->db->get('Stock')->result_array();
            } else {
                $this->db->where('id', $StockId);
                $update=$this->db->update('Stock', $data);

                if ($update==1) {
                    $this->db->select('id as StockId,CategoryId,StockType,Symbol,StrikeInterval,BuyStrike,SellStrike,NetDebtLimitPrice,StopLoss,ExpirationDate,Quantity,T1Stock,T1Option,T2Stock,T2Option,TradeType');
                    $this->db->where('id', $StockId);
                    $trade_data=$this->db->get('Stock')->result_array();
                }
            }

            
            if ($trade_data) {
                foreach ($trade_data as $key) {
                    $category = $this->db->get_where('Category', array('id'=>$cate_id))->result_array();

                    //$key['CategoryId']=$category[0]['id'];
                    $key['CategoryName']=$category[0]['CategoryName'];

                    $key['Userid']=$userid[0]['ID'];
                    //$key['ID']=$key['id'];
                    //$key['Category']=$cate_id;
                    $data2[] = $key;
                }
                
                $msg="vertical cell trade list get Successfully ";
                $suc=1;
                echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data2).'}';
            } else {
                $suc=1;
                $msg="No vertical cell trade list";
                echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
            }
        } else {
            $suc=0;
            $msg="No any vertical cell trade list";
            echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
        }
    }

    public function stock_list()
    {
        /*if(isset($_POST['UserId']))
        {*/
            //$UserId=$_POST['UserId'];
            $this->db->where('view_status', 0);
            $not_array=['3','5','7'];
            $this->db->where_not_in('status', $not_array);
            //$this->db->where('UserId',$UserId);
            $stocklist=$this->db->order_by("UpdateTime", "DESC")->get('Stock')->result_array();
            //print_r($stocklist);
        if ($stocklist) {
            foreach ($stocklist as $row) {
                $this->db->select('CategoryName');
                $this->db->where('id', $row['CategoryId']);
                $category=$this->db->get('Category')->result_array();
                if ($category) {
                    $row['CategoryName']=$category[0]['CategoryName'];
                } else {
                    $row['CategoryName'] ='';
                }
                $curdate = date('Y-m-d H:i', strtotime($row['CreateTime']));
                $date4 = new DateTime($curdate);

                $cdate = date('Y-m-d H:i', strtotime($row['UpdateTime']));
                $tdate = date('Y-m-d', strtotime($row['UpdateTime']));

                $date2 = new DateTime($cdate);
                $date3 = new DateTime($tdate);

                $row['PartialCloseOut'] = $row['CloseOut'] = $row['Stop_Loss'] =$row['Commentary'] = array();


                // PartialCloseOut //
                $this->db->select('id as CloseOutId,UserId,TradeId,Symbol,RecommendedPrice,MinPrice,Description,CreateTime,MaxPrice,StockType,view_status,UpdateTime,ExpirationDate,NetCreditLimitPrice,status');
                //$this->db->where("DATE_FORMAT(CreateTime,'%Y-%m-%d')",$CurrentDate);
                $this->db->where('TradeId', $row['id']);
                $this->db->order_by("CreateTime", "DESC");
                $sql = $this->db->get('PartialCloseOut');
                $partial_closeout_data = $sql->result_array();
                if ($partial_closeout_data) {
                    $row['PartialCloseOut']=$partial_closeout_data;
                }
                // CloseOut //
                $this->db->select('id as CloseOutId,UserId,TradeId,Symbol,RecommendedPrice,MinPrice,Description,CreateTime,status');
                $this->db->where('TradeId', $row['id']);
                //$this->db->where("DATE_FORMAT(CreateTime,'%Y-%m-%d')",$CurrentDate);
                $this->db->order_by("CreateTime", "DESC");
                $sql = $this->db->get('CloseOut');
                $closeout_data = $sql->result_array();
                if ($closeout_data) {
                    $row['CloseOut']=$closeout_data;
                }
                // Stop_Loss //
                $this->db->select('id as StopLossId,Symbol,StopLowPrice,StopHighPrice,Description,TradeId,UserId,CreateTime,status');
                $this->db->where('TradeId', $row['id']);
                $this->db->order_by("CreateTime", "DESC");
                //$this->db->where("DATE_FORMAT(CreateTime,'%Y-%m-%d')",$CurrentDate);
                $stoploss=$this->db->get('StopLoss')->result_array();

             // Commentary //
                $this->db->select('id as CommentaryId,CommentaryDescription,Subject,view_status,TradeId,UserId,CreateTime,UpdateTime');
                $this->db->where('TradeId', $row['id']);
                $this->db->order_by("CreateTime", "DESC");
                //$this->db->where("DATE_FORMAT(CreateTime,'%Y-%m-%d')",$CurrentDate);
                $commentary=$this->db->get('Commentary')->result_array();
                if ($commentary) {
                    $row['Commentary']=$commentary;
                }
                $data2[] = $row;
            }

            $msg="Stock list get Successfully ";
            $suc=1;
            echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data2).'}';
        } else {
            $suc=0;
            $msg="No any stock list";
            echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
        }
        //}
    }

    public function existing_trades()
    {
        if (isset($_POST['UserId'])) {
            $UserId=$_POST['UserId'];
            //echo $UserId;
            $this->db->select('id as StockId,TradeType,CreateTime,StockType,CategoryId,Symbol');
            $existing=$this->db->get('Stock')->result_array();
            //print_r($existing);

            if ($existing) {
                foreach ($existing as $row) {
                    $this->db->select('CategoryName');
                    $this->db->where('id', $row['CategoryId']);
                    $category=$this->db->get('Category')->result_array();
                        
                    $row['CategoryName']=$category[0]['CategoryName'];
                    $data2[]=$row;
                }

                $msg="Stock list get Successfully ";
                $suc=1;
                echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data2).'}';
            } else {
                $msg="No Stock list";
                $suc=1;
                echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
            }
        } else {
            $suc=0;
            $msg="No any stock list";
            echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
        }
    }
    function update_trade_status()
    {
        if (isset($_POST['StockId']) && isset($_POST['status'])) {
            $data['status'] = $_POST['status'];
            $data['UpdateTime'] =date('Y-m-d H:i:s');

            if ($_POST['status']=='2') {
                $msg="Stock Trade Filled successfully";
                $data['FilledTime'] =date('Y-m-d H:i:s');
            } elseif ($_POST['status']=="3") {
                $msg="Stock Trade Cancel successfully";
                $data['CancelTime'] =date('Y-m-d H:i:s');
            }
            $this->db->where('id', $_POST['StockId']);
            $update=$this->db->update('Stock', $data);
            $suc=1;
            echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
            die;
        }
    }

    public function stockDetails()
    {
        if (isset($_POST['StockId']) && $_POST['StockId']>0) {
            $stockId =$_POST['StockId'];
            $sql= "SELECT 'PartialCloseOut' as module,
                PartialCloseOut.Symbol as Symbol,
                PartialCloseOut.RecommendedPrice as RecommendedPrice,
                PartialCloseOut.MinPrice as MinPrice,
                PartialCloseOut.MaxPrice as MaxPrice,
                PartialCloseOut.StockType as StockType,
                PartialCloseOut.view_status as view_status,
                PartialCloseOut.StrikePrice as StrikePrice,
                PartialCloseOut.status as status,
                PartialCloseOut.createTime as sortDatetime,
                PartialCloseOut.ExpirationDate  as ExpirationDate,
                PartialCloseOut.NetCreditLimitPrice  as NetCreditLimitPrice,
                PartialCloseOut.StockType  as Subject,
                PartialCloseOut.Description as Description
                FROM PartialCloseOut WHERE PartialCloseOut.TradeId=$stockId
                UNION ALL 
                SELECT 'CloseOut' as module,
                CloseOut.Symbol as Symbol,
                CloseOut.RecommendedPrice as RecommendedPrice,
                CloseOut.MinPrice as MinPrice,
                CloseOut.MaxPrice as MaxPrice,
                CloseOut.StockType as StockType,
                CloseOut.view_status as view_status,
                CloseOut.StrikePrice as StrikePrice,
                CloseOut.status as status,
                CloseOut.createTime as sortDatetime,
                CloseOut.ExpirationDate  as ExpirationDate,
                CloseOut.NetCreditLimitPrice  as NetCreditLimitPrice,
                CloseOut.StockType  as Subject,
                CloseOut.Description as Description
                FROM CloseOut WHERE CloseOut.TradeId=$stockId 
                UNION  ALL
                SELECT 'StopLoss' as module,
                StopLoss.Symbol as Symbol,
                StopLoss.StrikePrice as RecommendedPrice,
                StopLoss.StopLowPrice as MinPrice,
                StopLoss.StopHighPrice as MaxPrice,
                StopLoss.StockType as StockType,
                StopLoss.view_status as view_status,
                StopLoss.StrikePrice as StrikePrice,
                StopLoss.status as status,
                StopLoss.createTime as sortDatetime,
                StopLoss.ExpirationDate  as ExpirationDate,
                StopLoss.NetCreditLimitPrice  as NetCreditLimitPrice,
                StopLoss.view_status  as Subject,
                StopLoss.Description as Description
                FROM StopLoss WHERE StopLoss.TradeId=$stockId
                UNION ALL 
                SELECT 'Commentary' as module,
                Commentary.view_status as Symbol,
                Commentary.view_status as RecommendedPrice,
                Commentary.view_status as MinPrice,
                Commentary.view_status as MaxPrice,
                Commentary.view_status as view_status,
                Commentary.view_status as StrikePrice,
                Commentary.view_status as StockType,
                '8' as status,
                Commentary.createTime as sortDatetime,
                Commentary.view_status  as ExpirationDate,
                Commentary.view_status  as NetCreditLimitPrice,
                Commentary.Subject  as Subject,
                Commentary.CommentaryDescription as  Description 
                FROM Commentary WHERE Commentary.TradeId=$stockId 
                UNION ALL 
                SELECT 'Filled' as module,
                Stock.Symbol as Symbol,
                Stock.view_status as RecommendedPrice,
                Stock.view_status as MinPrice,
                Stock.view_status as MaxPrice,
                Stock.view_status as view_status,
                Stock.view_status as StrikePrice,
                Stock.view_status as StockType,
                '2' as status,
                Stock.FilledTime as sortDatetime,
                Stock.view_status  as ExpirationDate,
                Stock.view_status  as NetCreditLimitPrice,
                Stock.view_status  as Subject,
                Stock.view_status as  Description 
                FROM Stock WHERE Stock.id=$stockId AND  Stock.FilledTime <> ''
                UNION ALL 
                SELECT 'Cancel' as module,
                Stock.Symbol as Symbol,
                Stock.view_status as RecommendedPrice,
                Stock.view_status as MinPrice,
                Stock.view_status as MaxPrice,
                Stock.view_status as view_status,
                Stock.view_status as StrikePrice,
                Stock.view_status as StockType,
                '3' as status,
                Stock.CancelTime as sortDatetime,
                Stock.view_status  as ExpirationDate,
                Stock.view_status  as NetCreditLimitPrice,
                Stock.view_status  as Subject,
                Stock.view_status as  Description 
                FROM Stock WHERE Stock.id=$stockId AND Stock.CancelTime <> ''
                ORDER by sortDatetime DESC";
            $query=$this->db->query($sql);
            $data['activity']=$query->result_array();
            $this->db->select('Stock.*,Category.CategoryName')->join('Category', 'Stock.CategoryId=Category.id')->where('Stock.id', $stockId);
            $existing=$this->db->get('Stock')->row_array();
            $data['stockDetails']=$existing;
            $suc=1;
            $msg="Stock list get Successfully ";
            echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data).'}';
        } else {
            $suc=0;
            $msg="No any stock list";
            echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
        }
    }
}
