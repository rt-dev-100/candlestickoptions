<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');
class Categoriesapi extends CI_Controller
{ 
	public function categories()
	{
		//$this->db->where('user_id',$value['ID']);
		$cate=$this->db->get('Category');
		$categories_data=$cate->result_array();
		if($categories_data)
		{
			foreach ($categories_data as $value) {
				//$data=$value;
				$value1['ID']=$value['id'];
				$value1['Userid']=$value['UserId'];
				$value1['Category']=$value['CategoryName'];
				//$value['']
				$data1[] = $value1;
			}
			
	    	$msg="categories list get Successfully ";
			$suc=1;
			echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data1).'}';
		}
		else
		{
			$suc=0;
			$msg="No any categories list";
			echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
		}
	}
	public function commentry()
	{
		//$this->db->where('user_id',$value['ID']);
		$sql="SELECT 
    post.ID,
    post.post_title,
     post.post_content,
     post.post_name,
    post.post_date,
    post.category_name,
    post.category_slug,
    post.category_id,
    CONCAT( 'wp-content/uploads','/', thumb.meta_value) as thumbnail,
    post.post_type
FROM (
    SELECT  p.ID,   
          p.post_title, 
     p.post_content,
     p.post_name,
          p.post_date,
          p.post_type,
          MAX(CASE WHEN pm.meta_key = '_thumbnail_id' then pm.meta_value ELSE NULL END) as thumbnail_id,
      term.name as category_name,
      term.slug as category_slug,
      term.term_id as category_id
    FROM wp_posts as p 
    LEFT JOIN wp_postmeta as pm ON ( pm.post_id = p.ID)
    LEFT JOIN wp_term_relationships as tr ON tr.object_id = p.ID
    LEFT JOIN wp_terms as term ON tr.term_taxonomy_id = term.term_id
    WHERE 1  AND p.post_status = 'publish'
    GROUP BY p.ID ORDER BY p.post_date DESC
  ) as post
  LEFT JOIN wp_postmeta AS thumb 
    ON thumb.meta_key = '_wp_attached_file' 
    AND thumb.post_id = post.thumbnail_id
   where post.thumbnail_id is not null and post.post_type = 'post'
    GROUP BY  post.category_id
    order by post.post_date";
		$cate=$this->db->query($sql);
		$categories_data=$cate->result_array();
		if($categories_data)
		{
			
			
	    	$msg="commentry list get Successfully ";
			$suc=1;
			echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($categories_data).'}';
		}
		else
		{
			$suc=0;
			$msg="No any commentry list";
			echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
		}
	}
	public function option()
	{
		
		//$option_data=$this->db->get('optiontb')->result_array();
		if($option_data){
			foreach ($option_data as $key) {
				//$data=$key;
				$key1['ID']=$key['id'];
				$key1['OptionName']=$key['option_name'];
				$data1[] = $key1;
			}
			
	    	$msg="option list get Successfully ";
			$suc=1;
			echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data1).'}';
		}
		else{
		    $suc=0;
			$msg="No any option list";
			echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
		}
	}
	public function test($value='')
	{
			echo date_default_timezone_get();
			echo date('Y-m-d H:i:s T', time()) . "<br>\n";
			date_default_timezone_set('asia/kolkata');
			echo date('Y-m-d H:i:s T', time()) . "<br>\n";


	}
}
?>