<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Partialcloseout extends CI_Controller
{

    public function stock_trade()
    {
        
        if (isset($_POST['UserId']) && isset($_POST['TradeId']) && isset($_POST['Symbol']) && isset($_POST['RecommendedPrice']) && isset($_POST['MinPrice']) && isset($_POST['Description']) && isset($_POST['CreateTime'])) {
            $insert['UserId'] = $UserId = $_POST['UserId'];
            $insert['TradeId'] = $TradeId = $_POST['TradeId'];
            $insert['Symbol'] = $Symbol = $_POST['Symbol'];
            $insert['RecommendedPrice'] = $RecommendedPrice = $_POST['RecommendedPrice'];
            $insert['MinPrice'] = $MinPrice = $_POST['MinPrice'];
            $insert['Description'] = $Description = $_POST['Description'];
            $insert['CreateTime'] = date('Y-m-d H:i:s');
            $insert['status'] = 6; // rhl 12-10-2018 //

            $this->db->insert('PartialCloseOut', $insert);

            $insertId = $this->db->insert_id();

            if ($insertId) {
                $data3['status'] = $insert['status'];
                $data3['UpdateTime'] =date('Y-m-d H:i:s');


                $this->db->where('id', $insert['TradeId']);
                $update=$this->db->update('Stock', $data3);
            }

            $this->db->select('id as CloseOutId,UserId,TradeId,Symbol,RecommendedPrice,MinPrice,Description,CreateTime,status');
            $partial_closeout_data = $this->db->get_where('PartialCloseOut', array('id'=>$insertId))->result_array();

            if ($partial_closeout_data) {
                $data = $partial_closeout_data;

                $msg="Stock Trade Successfully Closed Out";
                $suc=1;
                echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data).'}';
            } else {
                $msg="Stock Trade Not Closed Out";
                $suc=1;
                echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
            }
        } else {
            $msg="Please Enter valid data";
            $suc=0;
            echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
        }
    }

    public function singal_option_trade()
    {

        if (isset($_POST['UserId']) && isset($_POST['TradeId']) && isset($_POST['Symbol']) && isset($_POST['StockType']) && isset($_POST['MinPrice']) && isset($_POST['Description']) && isset($_POST['ExpirationDate']) && isset($_POST['MaxPrice']) && isset($_POST['CreateTime']) && isset($_POST['StrikePrice'])) {
            $insert['UserId'] = $UserId = $_POST['UserId'];
            $insert['TradeId'] = $TradeId = $_POST['TradeId'];
            $insert['Symbol'] = $Symbol = $_POST['Symbol'];
            $insert['StockType'] = $StockType = $_POST['StockType'];
            $insert['MinPrice'] = $MinPrice = $_POST['MinPrice'];
            $insert['Description'] = $Description = $_POST['Description'];
            $insert['ExpirationDate'] = $ExpirationDate = $_POST['ExpirationDate'];
            $insert['MaxPrice'] = $MaxPrice = $_POST['MaxPrice'];
            $insert['CreateTime'] = date('Y-m-d H:i:s');
            $insert['StrikePrice'] = $StrikePrice = $_POST['StrikePrice'];
            $insert['status'] = 6; // rhl 12-10-2018 //

            $this->db->insert('PartialCloseOut', $insert);
            $insertId = $this->db->insert_id();

            if ($insertId) {
                $data3['status'] = $insert['status'];
                $data3['UpdateTime'] =date('Y-m-d H:i:s');


                $this->db->where('id', $insert['TradeId']);
                $update=$this->db->update('Stock', $data3);
            }

            $this->db->select('id as CloseOutId,UserId,TradeId,Symbol,StockType,MinPrice,MaxPrice,ExpirationDate,Description,CreateTime,StrikePrice,status');
            $partial_closeout_data = $this->db->get_where('PartialCloseOut', array('id'=>$insertId))->result_array();

            if ($partial_closeout_data) {
                $data = $partial_closeout_data;

                $msg="Stock Trade Successfully Closed Out";
                $suc=1;
                echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data).'}';
            } else {
                $msg="Stock Trade Not Closed Out";
                $suc=1;
                echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
            }
        } else {
            $msg="Please Enter valid data";
            $suc=0;
            echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
        }
    }

    public function vertical_spread()
    {

        if (isset($_POST['UserId']) && isset($_POST['TradeId']) && isset($_POST['Symbol']) && isset($_POST['StockType']) && isset($_POST['Description']) && isset($_POST['ExpirationDate']) && isset($_POST['NetCreditLimitPrice']) && isset($_POST['CreateTime']) && isset($_POST['MinPrice']) && isset($_POST['MaxPrice']) && isset($_POST['StrikePrice']) && isset($_POST['BuyStrike']) && isset($_POST['SellStrike'])) {
            $insert['UserId'] = $UserId = $_POST['UserId'];
            $insert['TradeId'] = $TradeId = $_POST['TradeId'];
            $insert['Symbol'] = $Symbol = $_POST['Symbol'];
            $insert['StockType'] = $StockType = $_POST['StockType'];
            $insert['Description'] = $Description = $_POST['Description'];
            $insert['ExpirationDate'] = $ExpirationDate = $_POST['ExpirationDate'];
            $insert['NetCreditLimitPrice'] = $NetCreditLimitPrice = $_POST['NetCreditLimitPrice'];
            $insert['CreateTime'] = date('Y-m-d H:i:s');
            $insert['MinPrice'] = $MinPrice = $_POST['MinPrice'];
            $insert['MaxPrice'] = $MaxPrice = $_POST['MaxPrice'];
            $insert['StrikePrice'] = $StrikePrice = $_POST['StrikePrice'];
            $insert['status'] = 6; // rhl 12-10-2018 //
            
            $this->db->insert('PartialCloseOut', $insert);
            $insertId = $this->db->insert_id();

            if ($insertId) {
                $data3['status'] = $insert['status'];
                $data3['UpdateTime'] =date('Y-m-d H:i:s');

                  $data3['BuyStrike']  = $_POST['BuyStrike'];
                $data3['SellStrike']  = $_POST['SellStrike'];

                $this->db->where('id', $insert['TradeId']);
                $update=$this->db->update('Stock', $data3);
            }

            $this->db->select('id as CloseOutId,UserId,TradeId,Symbol,StockType,ExpirationDate,NetCreditLimitPrice,Description,CreateTime,MinPrice,MaxPrice,StrikePrice,status');
            $partial_closeout_data = $this->db->get_where('PartialCloseOut', array('id'=>$insertId))->result_array();

            if ($partial_closeout_data) {
                $data = $partial_closeout_data;

                $msg="Stock Trade Successfully Closed Out";
                $suc=1;
                echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data).'}';
            } else {
                $msg="Stock Trade Not Closed Out";
                $suc=1;
                echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
            }
        } else {
            $msg="Please Enter valid data";
            $suc=0;
            echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
        }
    }
    /*public function partial_closeout_list()
    {
        //$this->db->select('id as CloseOutId,UserId,TradeId,Symbol,RecommendedPrice,MinPrice,Description,CreateTime');
        $this->db->where('view_status',0);
        $this->db->order_by('CreateTime','desc');
        $sql = $this->db->get('PartialCloseOut');
        $partial_closeout_data = $sql->result_array();
        $data2 = array();
        if($partial_closeout_data)
        {
            foreach ($partial_closeout_data as $row) {

                $data2[] = $row;
            }

            $msg="Partial Closeout list get Successfully";
            $suc=1;
            echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data2).'}';
        }else{
            $suc=0;
            $msg="No any Partial Closeout";
            echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
        }
    }*/
}
