<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');
class Feedback extends CI_Controller
{ 
	public function index()
	{
		if(isset($_POST['UserId']) && isset($_POST['Message']))
		{
			
			$UserId=$_POST['UserId'];
			$Message=$_POST['Message'];
			
			$data['UserId'] = $UserId;
			$data['Message'] = $Message;
			
			$this->db->insert('Feedback',$data);
			$insert_id = $this->db->insert_id();
	
			$data2 = array();
			if($insert_id)
			{

				$this->db->where('id',$insert_id);
				$Feedback=$this->db->get('Feedback')->result_array();
				$data2[] = $Feedback[0];
			}
			if($data2){	
		    	$msg="Feedback list is got Successfully ";
				$suc=1;
				echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data2).'}';
			}else
			{
				$suc=0;
				$msg="No any Feedback list";
				echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
			}
		}else{
			$suc=0;
			$msg="Parameters are required.";
			echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
		}
	}
}
?>