<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');
class Favourite extends CI_Controller
{ 
	public function index()
	{
		if(isset($_POST['UserId']) && isset($_POST['TradeId']) && isset($_POST['status']))
		{
			
			$UserId = $_POST['UserId'];
			$TradeId= $_POST['TradeId'];
			$status = $_POST['status'];
			$insert_id = 0;
			$fav_users = $fav_users2 = array();
			
	        $Stock = $this->db->get_where('Stock',array('id'=>$TradeId))->result_array();

	        if($Stock[0]['fav_users'] == ''){
	            $fav_users = array();
	        }else{
	            $fav_users = json_decode($Stock[0]['fav_users'],true);
	        }
	        

	        if($status == 1){

	            array_push($fav_users, $UserId);

	        }else if(in_array($UserId, $fav_users)){

	            $pos = array_search($UserId, $fav_users);

	            unset($fav_users[$pos]);

	            foreach ($fav_users as $key => $value) {
	                $fav_users[] = $value;
	            }
	        }

	        if($fav_users){
	        	$fav_users = array_unique($fav_users);
	        }
	       

	        //print_r($fav_users);
	        
	        $data['fav_users'] = json_encode($fav_users);
	        $data['UpdateTime'] = $Stock[0]['UpdateTime'];
	       
	        $this->db->where('id',$TradeId);
	        $insert_id = $this->db->update('Stock',$data);
			$data2 = array();
			if($insert_id)
			{

				$Stock = $this->db->get_where('Stock',array('id'=>$TradeId))->result_array();
				if($Stock){
					$fav_users = json_decode($Stock[0]['fav_users'],true);
					if($fav_users){
			           $Stock[0]['fav_users'] = $fav_users;
			        }else{
			          $Stock[0]['fav_users'] = array();  
			        }
					$data2[] = $Stock[0];
				}
				
			}
			if($data2){	
		    	$msg="Favourite status is updated.";
				$suc=1;
				echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data2).'}';
			}else
			{
				$suc=0;
				$msg="Favourite status is not updated.";
				echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
			}
		}else{
			$suc=0;
			$msg="Parameters are required.";
			echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
		}
	}
	public function fav_list()
	{
		if(isset($_POST['UserId']))
		{
			
			$UserId = $_POST['UserId'];
			$userCategories = $this->getUserCategories($UserId);

			$this->db->where('view_status',0);
			$this->db->where_in('CategoryId',$userCategories);

			$this->db->order_by('UpdateTime','desc');
			$stocklist=$this->db->get('Stock')->result_array();
			$data2 = array();
			if($stocklist)
			{
				foreach ($stocklist as $row) {
					$fav_users = json_decode($row['fav_users'],true);
					if($fav_users){
					 	$row['fav_users'] = $fav_users;
					 	$row['fav_status'] = 0;
					 	if(in_array($UserId, $fav_users)){

					 		$row['PartialCloseOut'] = $row['CloseOut'] = $row['Stop_Loss'] = array();

					 		$this->db->select('CategoryName');
					 		$this->db->where('id',$row['CategoryId']);
					 		$this->db->where('view_status',0);
					 		$category=$this->db->get('Category')->result_array();
					 		$row['CategoryName']='';
					 		if($category){
					 			$row['CategoryName']=$category[0]['CategoryName'];
					 		}

					// PartialCloseOut //
					 		$this->db->select('id as CloseOutId,UserId,TradeId,Symbol,RecommendedPrice,MinPrice,Description,CreateTime,MaxPrice,StockType,view_status,UpdateTime,ExpirationDate,NetCreditLimitPrice,status');
					 		$this->db->where('TradeId',$row['id']);
					 		$sql = $this->db->get('PartialCloseOut');
					 		$partial_closeout_data = $sql->result_array();
					 		if($partial_closeout_data){
					 			$row['PartialCloseOut']=$partial_closeout_data;
					 		}
					// CloseOut //
					 		$this->db->select('id as CloseOutId,UserId,TradeId,Symbol,RecommendedPrice,MinPrice,Description,CreateTime,status');
					 		$this->db->where('TradeId',$row['id']);
					 		$sql = $this->db->get('CloseOut');
					 		$closeout_data = $sql->result_array();
					 		if($closeout_data){
					 			$row['CloseOut']=$closeout_data;
					 		}
					// Stop_Loss //
					 		$this->db->select('id as StopLossId,Symbol,StopLowPrice,StopHighPrice,Description,TradeId,UserId,CreateTime,status');
					 		$this->db->where('TradeId',$row['id']);
					 		$stoploss=$this->db->get('StopLoss')->result_array();
					 		if($stoploss){
					 			$row['Stop_Loss']=$stoploss;
					 		}
					 		


					 		$row['fav_status'] = 1;
					 		$data2[] = $row;
					 	}
					 }
				}

			}
			if($data2){	
		    	$msg="Favourite Stock list get Successfully";
				$suc=1;
				echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data2).'}';
			}else
			{
				$suc=0;
				$msg="No Favourite Stock list found.";
				echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
			}
		}else{
			$suc=0;
			$msg="Parameters are required.";
			echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
		}
	}
		private function getUserCategories($user_id='')
	{
			$query = $this->db->get_where('wp_usermeta', array('user_id'=>$user_id));
			$userData = $query->result_array();
			$userCategories =[];
			if($userData){
				foreach ($userData as $metaData) {
					if($metaData['meta_key']=='subscribe_history'){
						$subscribe_history = unserialize($metaData['meta_value']);
						$subscribe_history = array_reverse($subscribe_history);
						$planId = $subscribe_history[0]['plan_id'];
						switch ($planId) {
							case '3':
								// 'Platinium';
								$userCategories= ['1','2','3','4','5'];
								break;
							case '2':
								// 'Gold';
								$userCategories= ['1','2','3'];
								
								break;
							default:
								// 'Silver';
								$userCategories= ['1','2'];
								
								break;
						}
				}
			}
			}else{
				$suc=0;
				$msg="User data are not available.";
				echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
				die;
			}
			return $userCategories;
	}
}
?>