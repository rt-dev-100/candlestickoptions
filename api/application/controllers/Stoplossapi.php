<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');
class Stoplossapi extends CI_Controller
{ 
	public function stoploss_stock_trade()
	{
		if(isset($_POST['Symbol']) && isset($_POST['StopLowPrice']) && isset($_POST['StopHighPrice']) && isset($_POST['Description']) && isset($_POST['TradeId']) && isset($_POST['UserId']))
		{
			$Symbol=$_POST['Symbol'];
			$StopLowPrice=$_POST['StopLowPrice'];
			$StopHighPrice=$_POST['StopHighPrice'];
			$Description=$_POST['Description'];
			$TradeId=$_POST['TradeId'];
			$UserId=$_POST['UserId'];
			$status = 7; 
			$CreateTime = date('Y-m-d H:i:s');

			$update=array(
				'Symbol'=>$Symbol,
				'StopLowPrice'=>$StopLowPrice,
				'StopHighPrice'=>$StopHighPrice,
				'Description'=>$Description,
				'TradeId'=>$TradeId,
				'UserId'=>$UserId,
				'status'=>$status,
				'CreateTime'=>$CreateTime
				);
			
			$up=$this->db->insert('StopLoss',$update);

			$insertId = $this->db->insert_id();
			if($insertId){
				$data3['status'] = $update['status'];
				$data3['UpdateTime'] =date('Y-m-d H:i:s');
				$this->db->where('id',$update['TradeId']);
				$update=$this->db->update('Stock',$data3);
			}
			$this->db->select('id as StopLossId,Symbol,StopLowPrice,StopHighPrice,Description,TradeId,UserId');
			$this->db->where('id',$insertId);
			$stoploss=$this->db->get('StopLoss')->result_array();
			if($stoploss)
			{
				foreach ($stoploss as $row) {

					$data[]=$row;
					
				}
				$msg="Stock Stop Loss list Updated Successfully ";
				$suc=1;
				echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data).'}';
			}else{
			    $suc=1;
				$msg="No stock Stop Loss list Updated";
				echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
			}
		}else{
			$msg="No Stock Stop Loss list";
			$suc=0;
			echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
		}	
	}

	public function stoploss_single_option_trade()
	{
		if(isset($_POST['StockType']) && isset($_POST['Symbol']) && isset($_POST['ExpirationDate']) && isset($_POST['StopLowPrice']) && isset($_POST['StopHighPrice']) && isset($_POST['Description']) && isset($_POST['TradeId']) && isset($_POST['UserId']))
		{
			$StockType=$_POST['StockType'];
			$ExpirationDate=$_POST['ExpirationDate'];
			$Symbol=$_POST['Symbol'];
			$StopLowPrice=$_POST['StopLowPrice'];
			$StopHighPrice=$_POST['StopHighPrice'];
			$Description=$_POST['Description'];
			$TradeId=$_POST['TradeId'];
			$UserId=$_POST['UserId'];
			$status = 7; 
			$CreateTime = date('Y-m-d H:i:s');

			$update=array(
				'StockType'=>$StockType,
				'ExpirationDate'=>$ExpirationDate,
				'Symbol'=>$Symbol,
				'StopLowPrice'=>$StopLowPrice,
				'StopHighPrice'=>$StopHighPrice,
				'Description'=>$Description,
				'TradeId'=>$TradeId,
				'UserId'=>$UserId,
				'status'=>$status,
				'CreateTime'=>$CreateTime
				);
			
			//$this->db->where('TradeType',$TradeType);
			$up=$this->db->insert('StopLoss',$update);

			$insertId = $this->db->insert_id();
			if($insertId){
				$data3['status'] = $update['status'];
				$data3['UpdateTime'] =date('Y-m-d H:i:s');
				$this->db->where('id',$update['TradeId']);
				$update=$this->db->update('Stock',$data3);
			}

			$this->db->select('id as StopLossId,StockType,Symbol,ExpirationDate,StopLowPrice,StopHighPrice,Description,TradeId,UserId');
			$this->db->where('id',$insertId);
			$trade=$this->db->get('StopLoss')->result_array();
			if($trade)
			{
				foreach ($trade as $value) {

					$data[]=$value;
				}
				
				$msg="Stock Stop Loss Single Option Trade list get Successfully ";
				$suc=1;
				echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data).'}';
			}else{
			    $suc=1;
				$msg="No stock Stop Loss Single Option Trade list";
				echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
			}
		}else{
		    $suc=0;
			$msg="No any stock Stop Loss Single Option Trade list";
			echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
		}
	}

	public function stoploss_vertical_spread()
	{
		if(isset($_POST['StockType']) && isset($_POST['Symbol']) && isset($_POST['ExpirationDate']) && isset($_POST['NetCreditLimitPrice']) && isset($_POST['Description']) && isset($_POST['TradeId']) && isset($_POST['UserId']))
		{
			$StockType=$_POST['StockType'];
			$ExpirationDate=$_POST['ExpirationDate'];
			$Symbol=$_POST['Symbol'];
			$NetCreditLimitPrice=$_POST['NetCreditLimitPrice'];
			$Description=$_POST['Description'];
			$TradeId=$_POST['TradeId'];
			$UserId=$_POST['UserId'];
			$status = 7; 
			$CreateTime = date('Y-m-d H:i:s');
			$update=array(
				'StockType'=>$StockType,
				'ExpirationDate'=>$ExpirationDate,
				'Symbol'=>$Symbol,
				'NetCreditLimitPrice'=>$NetCreditLimitPrice,
				'TradeId'=>$TradeId,
				'Description'=>$Description,
				'UserId'=>$UserId,
				'status'=>$status,
				'CreateTime'=>$CreateTime
				);
			
			$up=$this->db->insert('StopLoss',$update);

			$insertId = $this->db->insert_id();
			if($insertId){
				$data3['status'] = $update['status'];
				$data3['UpdateTime'] =date('Y-m-d H:i:s');
				$this->db->where('id',$update['TradeId']);
				$update=$this->db->update('Stock',$data3);
			}
			$this->db->select('id as StopLossId,StockType,Symbol,ExpirationDate,NetCreditLimitPrice,Description,TradeId,UserId');
			$this->db->where('id',$insertId);
			$trade=$this->db->get('StopLoss')->result_array();
			if($trade)
			{
				foreach ($trade as $row) {

					$data[]=$row;
				}
				
				$msg="Stock Stop Loss vertical spread list get Successfully ";
				$suc=1;
				echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data).'}';
			}else{
			    $suc=1;
				$msg="No stock Stop Loss vertical spread list";
				echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
			}
		}else{
		    $suc=0;
			$msg="No Any stock Stop Loss vertical spread list";
			echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
		}
	}
}
?>