<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Userapi extends CI_Controller
{

    public function todays_trade()
    {
        if (isset($_POST['UserId']) && isset($_POST['CurrentDate'])) {
            $UserId=$_POST['UserId'];
            
            //$CurrentDate = date('Y-m-d',strtotime($_POST['CurrentDate']));
            //$date1 = new DateTime($CurrentDate);
            
            $CurrentDate = date('Y-m-d');
            $date1 = new DateTime($CurrentDate);
            
            $serverdate1 = new DateTime(date('Y-m-d 15:30'));
            $serverdate2 = new DateTime(date('Y-m-d 16:15'));
            $userCategories = $this->getUserCategories($UserId);
            $this->db->where('view_status', 0);
            $this->db->where_in('CategoryId', $userCategories);
            //$this->db->where('UserId',$UserId);
            $this->db->order_by('UpdateTime', 'desc');
            $stocklist=$this->db->get('Stock')->result_array();
            $data2 = array();
            if ($stocklist) {
                foreach ($stocklist as $row) {
                    $fav_users = json_decode($row['fav_users'], true);
                    $row['fav_status'] = 0;
                    if ($fav_users) {
                        if (in_array($UserId, $fav_users)) {
                            $row['fav_status'] = 1;
                        } else {
                            $row['fav_status'] = 0;
                        }
                    }

                    $curdate = date('Y-m-d H:i', strtotime($row['CreateTime']));
                    $date4 = new DateTime($curdate);

                    $cdate = date('Y-m-d H:i', strtotime($row['UpdateTime']));
                    $tdate = date('Y-m-d', strtotime($row['UpdateTime']));

                    $date2 = new DateTime($cdate);
                    $date3 = new DateTime($tdate);

                    $row['PartialCloseOut'] = $row['CloseOut'] = $row['Stop_Loss'] = array();
        
                    $this->db->select('CategoryName');
                    $this->db->where('id', $row['CategoryId']);
                    $this->db->where('view_status', 0);
                    $category=$this->db->get('Category')->result_array();
                    $row['CategoryName']='';
                    if ($category) {
                        $row['CategoryName']=$category[0]['CategoryName'];
                    }
                    
                    // PartialCloseOut //
                    $this->db->select('id as CloseOutId,UserId,TradeId,Symbol,RecommendedPrice,MinPrice,Description,CreateTime,MaxPrice,StockType,view_status,UpdateTime,ExpirationDate,NetCreditLimitPrice,status');
                    //$this->db->where("DATE_FORMAT(CreateTime,'%Y-%m-%d')",$CurrentDate);
                    $this->db->where('TradeId', $row['id']);
                    $sql = $this->db->get('PartialCloseOut');
                    $partial_closeout_data = $sql->result_array();
                    if ($partial_closeout_data) {
                        $row['PartialCloseOut']=$partial_closeout_data;
                    }
                    // CloseOut //
                    $this->db->select('id as CloseOutId,UserId,TradeId,Symbol,RecommendedPrice,MinPrice,Description,CreateTime,status');
                    $this->db->where('TradeId', $row['id']);
                    //$this->db->where("DATE_FORMAT(CreateTime,'%Y-%m-%d')",$CurrentDate);
                    $sql = $this->db->get('CloseOut');
                    $closeout_data = $sql->result_array();
                    if ($closeout_data) {
                        $row['CloseOut']=$closeout_data;
                    }
                    // Stop_Loss //
                    $this->db->select('id as StopLossId,Symbol,StopLowPrice,StopHighPrice,Description,TradeId,UserId,CreateTime,status');
                    $this->db->where('TradeId', $row['id']);
                    //$this->db->where("DATE_FORMAT(CreateTime,'%Y-%m-%d')",$CurrentDate);
                    $stoploss=$this->db->get('StopLoss')->result_array();
                    if ($stoploss) {
                        $row['Stop_Loss']=$stoploss;
                    }
                    
                    // Start of 23-10-2018 //
                    if ($row['status'] == 3 || $row['status'] == 5 || $row['status'] == 7) {
                        // if($date1 == $date3 && $date2 <= $serverdate2){
                            // $data2[] = $row;
                        // }
                    } else {
                        $data2[] = $row;
                    }
                    
                    // end of 23-10-2018 //
                }
            }
            if ($data2) {
                $msg="Today's Trade list get Successfully ";
                $suc=1;
                echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data2).'}';
            } else {
                $suc=0;
                $msg="No any today's trade list";
                echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
            }
        } else {
            $suc=0;
            $msg="Parameters are required.";
            echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
        }
    }
    public function past_trade()
    {
        if (isset($_POST['UserId']) && isset($_POST['Month_Name'])) {
            $UserId=$_POST['UserId'];
            //$date1 = date('Y-m',strtotime($_POST['Month_Name']));
            
            $CurrentDate = date('Y-m-d');
            $date1 = new DateTime($CurrentDate);

            $serverdate1 = new DateTime(date('Y-m-d 15:30'));
            $serverdate2 = new DateTime(date('Y-m-d 16:15'));
            $userCategories = $this->getUserCategories($UserId);

            $this->db->where('view_status', 0);
            $this->db->where_in('CategoryId', $userCategories);
            //$this->db->where('UserId',$UserId);
            $this->db->order_by('UpdateTime', 'desc');
            $stocklist=$this->db->get('Stock')->result_array();
            $data2 = array();
            if ($stocklist) {
                foreach ($stocklist as $row) {
                    $fav_users = json_decode($row['fav_users'], true);
                    $row['fav_status'] = 0;
                    if ($fav_users) {
                        if (in_array($UserId, $fav_users)) {
                            $row['fav_status'] = 1;
                        } else {
                            $row['fav_status'] = 0;
                        }
                    }

                    $curdate = date('Y-m-d H:i', strtotime($row['CreateTime']));
                    $date4 = new DateTime($curdate);

                    $cdate = date('Y-m-d H:i', strtotime($row['UpdateTime']));
                    $tdate = date('Y-m-d', strtotime($row['UpdateTime']));

                    $date2 = new DateTime($cdate);
                    $date3 = new DateTime($tdate);

                    //$cdate = date('Y-m-d H:i',strtotime($row['UpdateTime']));

                    //$date2 = new DateTime($cdate);
                    
                    $row['PartialCloseOut'] = $row['CloseOut'] = $row['Stop_Loss'] = array();
        
                    $this->db->select('CategoryName');
                    $this->db->where('id', $row['CategoryId']);
                    $this->db->where('view_status', 0);
                    $category=$this->db->get('Category')->result_array();
                    $row['CategoryName']='';
                    if ($category) {
                        $row['CategoryName']=$category[0]['CategoryName'];
                    }
                    //$row['CategoryName']=$category[0]['CategoryName'];
                    // PartialCloseOut //
                    $this->db->select('id as CloseOutId,UserId,TradeId,Symbol,RecommendedPrice,MinPrice,Description,CreateTime,MaxPrice,StockType,view_status,UpdateTime,ExpirationDate,NetCreditLimitPrice,status');
                    //$this->db->where("DATE_FORMAT(CreateTime,'%Y-%m')",$date1);
                    $this->db->where('TradeId', $row['id']);
                    $sql = $this->db->get('PartialCloseOut');
                    $partial_closeout_data = $sql->result_array();
                    if ($partial_closeout_data) {
                        $row['PartialCloseOut']=$partial_closeout_data;
                    }
                    // CloseOut //
                    $this->db->select('id as CloseOutId,UserId,TradeId,Symbol,RecommendedPrice,MinPrice,Description,CreateTime,status');
                    $this->db->where('TradeId', $row['id']);
                    //$this->db->where("DATE_FORMAT(CreateTime,'%Y-%m')",$date1);
                    $sql = $this->db->get('CloseOut');
                    $closeout_data = $sql->result_array();
                    if ($closeout_data) {
                        $row['CloseOut']=$closeout_data;
                    }
                    // Stop_Loss //
                    $this->db->select('id as StopLossId,Symbol,StopLowPrice,StopHighPrice,Description,TradeId,UserId,CreateTime,status');
                    $this->db->where('TradeId', $row['id']);
                    //$this->db->where("DATE_FORMAT(CreateTime,'%Y-%m')",$date1);
                    $stoploss=$this->db->get('StopLoss')->result_array();
                    if ($stoploss) {
                        $row['Stop_Loss']=$stoploss;
                    }
                    
                    // Start of 23-10-2018 //
                    if ($row['status'] == 3 || $row['status'] == 5 || $row['status'] == 7) {
                        // if ($date2 < $serverdate2 && $date1 != $date3) {
                            $data2[] = $row;
                        // }
                    }
                    
                    // end of 23-10-2018 //
                }
            }
            if ($data2) {
                $msg="Past Trade list get Successfully ";
                $suc=1;
                echo '{"result": "'.$suc.'", "message": "'.$msg.'", "data":'.json_encode($data2).'}';
            } else {
                $suc=0;
                $msg="No any Past trade list";
                echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
            }
        } else {
            $suc=0;
            $msg="Parameters are required.";
            echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
        }
    }
    private function getUserCategories($user_id = '')
    {
            $query = $this->db->get_where('wp_usermeta', array('user_id'=>$user_id));
            $userData = $query->result_array();
            $userCategories =[];
        if ($userData) {
            foreach ($userData as $metaData) {
                if ($metaData['meta_key']=='subscribe_history') {
                    $subscribe_history = unserialize($metaData['meta_value']);
                    $subscribe_history = array_reverse($subscribe_history);
                    $planId = $subscribe_history[0]['plan_id'];
                    switch ($planId) {
                        case '3':
                            // 'Platinium';
                            $userCategories= ['1','2','3','4','5'];
                            break;
                        case '2':
                            // 'Gold';
                            $userCategories= ['1','2','3'];
                                
                            break;
                        default:
                            // 'Silver';
                            $userCategories= ['1','2'];
                                
                            break;
                    }
                }
            }
        } else {
            $suc=0;
            $msg="User data are not available.";
            echo '{"result": "'.$suc.'", "message": "'.$msg.'"}';
            die;
        }
            return $userCategories;
    }
}
