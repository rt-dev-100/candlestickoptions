<?php

function pippin_stripe_payment_form($atts, $content = null) {
	
	extract( shortcode_atts( array(
		'amount' => ''
	), $atts ) );
	
	global $stripe_options;
	$plans = pippin_get_stripe_plans();
	
	if(isset($_GET['payment']) && $_GET['payment'] == 'paid') {
				
		echo '<p class="registeration-success">' . __('Thank you for your Registration with us.', 'pippin_stripe') . '</p>';
	} else { ?>
		<div class="registeration-error"></div>
		<form action="" method="POST" id="stripe-payment-form">
			<div class="form-row row clearfix">
				<div class="col-md-6">
					<div class="membership-plan active monthly" data-plan="1" data-price="$<?php echo $plans[1]; ?>">
						<div class="sh-iconbox sh-iconbox-style2 sh-iconbox-left">
							<div class="sh-iconbox-icon">
								<div class="sh-iconbox-icon-shape sh-iconbox-square">
									<i class="sh-iconbox-hover ti-alarm-clock"></i>
								</div>
							</div>
							<div class="sh-iconbox-aside">
								<div class="sh-iconbox-title">
									<h3>Monthly Subscription Plan</h3>
								</div>
								<div class="sh-iconbox-seperator"></div> 
								<div class="sh-iconbox-content">
									<p><span style="font-weight: 400"><b><span class="plan-price">$<?php echo $plans[1]; ?></span></b> per month</span></p>
								</div>
								<div class="field-checkbox input-styled">
									<input type="checkbox" name="forex[]" value="2" data-price="$<?php echo $plans['2']; ?>" id="monthly_forex_plan">
									<label class="checkbox-label"><?php _e('Future and Forex', 'pippin_stripe'); ?></label>
								</div>
							</div>							
						</div>						
					</div>
				</div>
				<div class="col-md-6">
					<div class="membership-plan yearly" data-plan="3" data-price="$<?php echo $plans[3]; ?>">
						<div class="sh-iconbox sh-iconbox-style2 sh-iconbox-left">
							<div class="sh-iconbox-icon">
								<div class="sh-iconbox-icon-shape sh-iconbox-square">
									<i class="sh-iconbox-hover ti-alarm-clock"></i>
								</div>
							</div>
							<div class="sh-iconbox-aside">
								<div class="sh-iconbox-title">
									<h3>Yearly Subscription Plan</h3>
								</div>
								<div class="sh-iconbox-seperator"></div>
								<div class="sh-iconbox-content">
									<p><span style="font-weight: 400"><b><span class="plan-price">$<?php echo $plans[3]; ?></span></b> per month</span></p>
								</div>
								<div class="field-checkbox input-styled">
									<input type="checkbox" name="forex[]" value="4" data-price="$<?php echo $plans[4]; ?>" id="yearly_forex_plan">
									<label class="checkbox-label"><?php _e('Future and Forex', 'pippin_stripe'); ?></label>
								</div>
							</div>							
						</div>						
					</div>
				</div>
			</div>
			<h4 class="sh-heading-content size-default text-left">Personal Detail</h4>
			<div class="row">
				<div class="col-md-6">
					<div class="form-row">
						<label><?php _e('First Name', 'pippin_stripe'); ?></label>
						<input type="text" size="20" placeholder="First Name" class="fname" name="fname"/>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-row">
						<label><?php _e('Last Name', 'pippin_stripe'); ?></label>
						<input type="text" size="20" placeholder="Last Name" class="lname" name="lname"/>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-row">
						<label><?php _e('Username', 'pippin_stripe'); ?></label>
						<input type="text" size="20" placeholder="Username" class="uname" name="username"/>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-row">
						<label><?php _e('Password', 'pippin_stripe'); ?></label>
						<input type="password" size="10" placeholder="Password" class="password" name="password"/>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-row">
						<label><?php _e('Email', 'pippin_stripe'); ?></label>
						<input type="text" size="20"  placeholder="Email Address" class="email" name="email"/>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-row">
						<label><?php _e('Phone', 'pippin_stripe'); ?></label>
						<input id="phone" type="tel" class="phone" name="phone" />
					</div>
				</div>
			</div>
			<h4 class="sh-heading-content size-default text-left">Bllling Information</h4>
			<div class="row">
				<div class="col-md-6">
					<div class="form-row">
						<label><?php _e('Card Name', 'pippin_stripe'); ?></label>
						<input type="text" size="20" placeholder="Card Name" class="card-name" name="card-name"/>
					</div>
				</div>
				<div class="col-md-6">			
					<div class="form-row">
						<label><?php _e('Card Number', 'pippin_stripe'); ?></label>
						<input type="text" size="20" placeholder="Credit Card Number" autocomplete="off" class="card-number"/>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-row">
						<label><?php _e('CVC', 'pippin_stripe'); ?></label>
						<input type="text" size="4" placeholder="CVC" autocomplete="off" class="card-cvc"/>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-row">
						<div class="clearfix"><label><?php _e('Expiration (MM/YYYY)', 'pippin_stripe'); ?></label></div>
						<div class="row">
							<div class="col-md-6"><input type="text" size="2" placeholder="MM" class="card-expiry-month"/></div>
							<div class="col-md-6"><input type="text" size="4" placeholder="YYYY" class="card-expiry-year"/></div>
						</div>
					</div>
				</div>
			</div>			
			<?php /*if(isset($stripe_options['recurring'])) { ?>
			<div class="form-row">
				<label><?php _e('Payment Type:', 'pippin_stripe'); ?></label>
				<input type="radio" name="recurring" class="stripe-recurring" value="no" checked="checked"/><span><?php _e('One time payment', 'pippin_stripe'); ?></span>
				<input type="radio" name="recurring" class="stripe-recurring" value="yes"/><span><?php _e('Recurring monthly payment', 'pippin_stripe'); ?></span>
			</div>
			<div class="form-row" id="stripe-plans" style="display:none;">
				<label><?php _e('Choose Your Plan', 'pippin_stripe'); ?></label>
				<select name="plan_id" id="stripe_plan_id">
					<?php 						
						if($plans) {
							foreach($plans as $id => $plan) {
								echo '<option value="' . $id . '">' . $plan . '</option>';
							}
						}
					?>
				</select>
			</div>
			<?php }*/ ?>
			<input type="hidden" name="plan_id" id="stripe_plan_id" class="" value="1"/>
			<input type="hidden" name="action" value="stripe"/>
			<input type="hidden" name="redirect" value="<?php echo get_permalink(); ?>"/>
			<input type="hidden" name="amount" value="<?php echo base64_encode($amount); ?>"/>
			<input type="hidden" name="stripe_nonce" value="<?php echo wp_create_nonce('stripe-nonce'); ?>"/>
			<div class="form-row">
				<button type="submit" class="btn-submit" id="stripe-submit"><?php _e('Submit Payment', 'pippin_stripe'); ?></button>
			</div>
		</form>
		<div class="payment-errors"></div>
		<?php
	}
}
add_shortcode('payment_form', 'pippin_stripe_payment_form');