Stripe.setPublishableKey(stripe_vars.publishable_key);
function stripeResponseHandler(status, response) {
    if (response.error) {
    	//alert(response.error.message);
		// show errors returned by Stripe
        jQuery(".payment-errors").html(response.error.message);
		// re-enable the submit button
		jQuery('#stripe-submit').attr("disabled", false);
		//jQuery('#submit-signup-term').attr("disabled", false);
    } else {
        var form$ = jQuery("#stripe-payment-form");
        // token contains id, last4, and card type
        var token = response['id'];
        // insert the token into the form so it gets submitted to the server
        form$.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
        // and submit
        //jQuery('#signup_modal').modal();
        //alert('1');
       // var cvcvalue= jQuery('.card-cvc').val();
        var fname= jQuery('.fname').val();
                var lname = jQuery('.lname').val();
                var uname = jQuery('.uname').val();
                var password = jQuery('.password').val();
                var email = jQuery('.email').val();
                var phone = jQuery('.phone').val();
                var card_name = jQuery('.card-name').val();
                var card_number = jQuery('.card-number').val();
                var card_cvc = jQuery('.card-cvc').val();
                var card_month = jQuery('.card-expiry-month').val();
                var card_year = jQuery('.card-expiry-year').val();
                if((fname == '') || (lname == '') || (uname == '') || (password == '') ||(email == '') || (phone == '') ||(card_name == '') || (card_number == '') || (card_cvc == '') || (card_month == '') || (card_year == '')){
                	
                	return false;
                }
        //alert('cvc: '+cvcvalue);
        var user_id = jQuery('#user_id').val();
        console.log(user_id);
        if(user_id == ''){
        	jQuery("#signup_modal").modal('show');
        //jQuery(".modal-footer").removeClass("conditional_footer");
        // jQuery('#signup_modal .conditional_footer').css('display','block');
        jQuery("#signup_modal .modal-footer").css("cssText", "display: block;");
        jQuery('#btn_signup_accept').click(function(){

        	form$.get(0).submit();
        });
        
        }
        else{
        	form$.get(0).submit();
        }
        
    }
}


jQuery(document).ready(function($) {
    var qryVar = "";
    if(typeof window.location.href.split("?")[1] != "undefined"){
        if(window.location.href.split("?")[1].split("=")[0] == "userId" ){
          //  jQuery("#freePlan").hide();
            jQuery(".monthly").addClass("active");
            //jQuery("#billingInfo").slideDown();
            qryVar = "set";
        }
    }
    
	
	jQuery("#phone").intlTelInput({
	  	initialCountry: "us",
	  	allowExtensions: true,
		autoFormat: false,
		autoHideDialCode: false,
		autoPlaceholder: false,
		defaultCountry: "us",
		//ipinfoToken: "yolo",
		nationalMode: false,
		numberType: "MOBILE",
		preventInvalidNumbers: true,
              preferredCountries: ["us"],
	  	geoIpLookup: function(callback) {
		    jQuery.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
		    	var countryCode = (resp && resp.country) ? resp.country : "";	      
		    	callback(countryCode);	      
		    });
	  	},
	  utilsScript: "../wp-content/themes/jevelin-child/js/utils.js" // just for formatting/placeholders etc
	});

	$('#stripe-payment-form').validate({
	    onfocusout: function(element) {
	    
	    	this.element(element);
	    },    
	    rules: {     
			fname: {
				required: true,          
			},
			lname: {
				required: true,      
			},
			username: {
				required: true,      
			},
			email: {        
				email: true,
				required: true
			},
			password: {
				required: true,
				minlength : 6
			},
			phone: {
				required: true,
				minlength : 10
			},
			card_name: {
				required: true,
			},
			card_number: {
				required: true,
			},
			card_cvc: {
				required: true,
			},
			card_expiry_month: {
				required: true,
			},
			card_expiry_year: {
				required: true,
			},
            terms:{
                required: true
            }
			
	    },
	    messages: {
			fname: "Please enter a valid FirstName",
			lname: "Please enter a valid LastName",
			username: "Please enter a valid Username",
			email: "Please enter a valid E-mail Address",
			password: "Please enter a valid Password",
			phone: "Please enter a valid Phone Number",
			card_name: "Please enter a valid card name",
			card_number: "Please enter a valid card number",
			card_cvc: "Please enter a valid card security code",
			card_expiry_month: "Please enter a valid card expiry month",
			card_expiry_year: "Please enter a valid card expiry year",

                        terms: "Please check Terms and Condition"
	    },
	    errorElement: "span",
	    errorPlacement: function(error, element) {
	      	if (element.hasClass('error')) {
	            // Error message                
	      	}
	      	element.after(error);
	    }
  	});

	$('.card-number').payment('formatCardNumber');
	//$('.cc-exp').payment('formatCardExpiry');
	//$('.cc-cvc').payment('formatCardCVC');
	$.fn.toggleInputError = function(erred) {
		$(this).toggleClass('error', erred);
		return this;
	};

	$('#stripe-payment-form .card-number').focusout(function(){
		var cardType = $.payment.cardType($('.card-number').val());
		jQuery('.payment-errors').toggleInputError(!$.payment.validateCardNumber($('#stripe-payment-form .card-number').val(), cardType));
	}); 
	/*$('#stripe-payment-form .cc-exp').focusout(function(){
		var exp_date = $('.cc-exp').val().match(/([0-9]{2}) \/ ([0-9]{2})/);      
		var cardType = $.payment.cardType($('.cc-number').val());
		jQuery('.cc-exp-wrapper').toggleInputError(!$.payment.validateCardExpiry( $('.cc-exp').payment('cardExpiryVal') ));
	});
	$('#stripe-payment-form .card-cvc').focusout(function(){
		var cardType = $.payment.cardType($('.cc-number').val());
		jQuery('.cc-cvc-wrapper').toggleInputError(!$.payment.validateCardCVC($('#stripe-payment-form .cc-cvc').val(), cardType));
	});*/

	$( ".membership-plan" ).click(function() {			
		$(".membership-plan").removeClass('active');		
		$(this).addClass('active');
		/*var default_price = !$(".membership-plan").hasClass('active').data('price');
		!$(".membership-plan").hasClass('active').find(".plan-price").html(default_price);*/

		if($(this).find("input[type=checkbox]").prop("checked") == true) {
			var price = $(this).find("input[type=checkbox]").data('price')
			$(this).find(".plan-price").html(price);
			//var group = "input:checkbox[name='forex[]']";
			//$(group).prop("checked", false);			
			$(this).find("input[type=checkbox]").prop("checked",true);
			var plan_id = $(this).find("input[type=checkbox]").val();
		} else {
			var price = $(this).data('price');
			$(this).find(".plan-price").html(price);
			var plan_id = $(this).data('plan');	
		}	
		$("#stripe_plan_id").val(plan_id);
	});


	$("#stripe-payment-form").submit(function(event) {
		   // alert('1');
		    jQuery('#signup_modal .modal-footer').css('display','block !important');
            if($("#stripe_plan_id").val() != 3 ){
               // alert("here");
               // alert(qryVar);

               if(qryVar != "set"){
                   	var username = $(".uname").val();
		var email = $(".email").val();
		$.ajax({          
	        type : 'post',
	        data : { action : "check_user_exist", username : username, email : email, qryVar : qryVar },
	        success : function( response ) {
	          	if(response.status == true) {
	          		// disable the submit button to prevent repeated clicks

					$('#stripe-submit').attr("disabled", "disabled");
					//jQuery('#submit-signup-term').attr("disabled", "disabled");
					// send the card details to Stripe
					Stripe.createToken({
						name: $('.card-name').val(),
						number: $('.card-number').val(),
						cvc: $('.card-cvc').val(),
						exp_month: $('.card-expiry-month').val(),
						exp_year: $('.card-expiry-year').val()
					}, stripeResponseHandler);
	          	} else {
	          		$(".registeration-error").addClass('invalid');
	          		$(".registeration-error").html("<strong>Error:</strong> An account is already registered with your email address.");
	          		//$("<div class='registeration-error'><strong>Error:</strong> An account is already registered with your email address.</div>").insertBefore("#stripe-payment-form");
	          	}
        	}
        });
               }
               else{
                   // disable the submit button to prevent repeated clicks
					$('#stripe-submit').attr("disabled", "disabled");
					//jQuery('#submit-signup-term').attr("disabled", "disabled");
					// send the card details to Stripe
					Stripe.createToken({
						name: $('.card-name').val(),
						number: $('.card-number').val(),
						cvc: $('.card-cvc').val(),
						exp_month: $('.card-expiry-month').val(),
						exp_year: $('.card-expiry-year').val()
					}, stripeResponseHandler);
               }
	
		// prevent the form from submitting with the default action
		return false;
            }
	});

	$('.stripe-recurring').change(function() {
		if($(this).val() == 'yes') {
			$('#stripe-plans').slideDown();
		} else {
			$('#stripe-plans').slideUp();
		}
	});
        
        jQuery(".membership-plan").click(function(){
                               if(jQuery(this).attr("data-price") != "$0"){
                                   jQuery("#billingInfo").slideDown();
                               }
                               else{
                                  // jQuery("#billingInfo").slideUp();
                                  jQuery("#billingInfo").slideDown();
                               }
                            });
});