jQuery(document).ready(function(){
	$ = jQuery;
   var qryVar = "";
    if(typeof window.location.href.split("?")[1] != "undefined"){
        if(window.location.href.split("?")[1].split("=")[0] == "userId" ){
          //  jQuery("#freePlan").hide();
            jQuery(".monthly").addClass("active");
            //jQuery("#billingInfo").slideDown();
            qryVar = "set";
        }
    }
   
   $( ".membership-plan" ).click(function() {			
		$(".membership-plan").removeClass('active');		
		$(this).addClass('active');
		/*var default_price = !$(".membership-plan").hasClass('active').data('price');
		!$(".membership-plan").hasClass('active').find(".plan-price").html(default_price);*/

		// if($(this).find("input[type=checkbox]").prop("checked") == true) {
		// 	var price = $(this).find("input[type=checkbox]").data('price')
		// 	$(this).find(".plan-price").html(price);
		// 	$('#plan_amount').val(price);
		// 	//var group = "input:checkbox[name='forex[]']";
		// 	//$(group).prop("checked", false);			
		// 	$(this).find("input[type=checkbox]").prop("checked",true);
		// 	var plan_id = $(this).find("input[type=checkbox]").val();
		// } else {
		// 	var price = $(this).data('price');
		// 	$(this).find(".plan-price").html(price);
		// 	var plan_id = $(this).data('plan');	
		// 	$('#plan_amount').val(price);
		// }	

		// $("#transax_plan_id").val(plan_id);
		// $('#plan_amount').val(price);
	});
   jQuery("#phone").intlTelInput({
	  	initialCountry: "us",
	  	allowExtensions: true,
		autoFormat: false,
		autoHideDialCode: false,
		autoPlaceholder: false,
		defaultCountry: "us",
		//ipinfoToken: "yolo",
		nationalMode: false,
		numberType: "MOBILE",
		preventInvalidNumbers: true,
              preferredCountries: ["us"],
	  	geoIpLookup: function(callback) {
		    jQuery.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
		    	var countryCode = (resp && resp.country) ? resp.country : "";	      
		    	callback(countryCode);	      
		    });
	  	},
	  utilsScript: "../wp-content/themes/jevelin-child/js/utils.js" // just for formatting/placeholders etc
	});
   jQuery('#transax-payment-form').validate({
	    onfocusout: function(element) {
	        
	    	this.element(element);
	    },    
	    rules: {     
			fname: {
				required: true,          
			},
			lname: {
				required: true,      
			},
			username: {
				required: true,      
			},
			email: {        
				email: true,
				required: true
			},
			password: {
				required: true,
				minlength : 6
			},
			phone: {
				required: true,
				minlength : 10
			},
			card_name: {
				required: true,
			},
			card_number: {
				required: true,
			},
			card_cvc: {
				required: true,
			},
			card_expiry_month: {
				required: true,
			},
			card_expiry_year: {
				required: true,
			},
                        billing_address: {
				required: true,
			},
                        billing_city: {
				required: true,
			},
                        billing_state: {
				required: true,
			},
                        billing_zip: {
				required: true,
                                maxlength: 6,
                                minlength: 4
			}
            	
			
	    },
	    messages: {
			fname: "Please enter a valid FirstName",
			lname: "Please enter a valid LastName",
			username: "Please enter a valid Username",
			email: "Please enter a valid E-mail Address",
			password: {
                            required: "Please enter  a password",
                            minlength: "Your password must be at least 6 characters long"
                        },
                        phone: {
                            required: "Please enter a valid Phone Number",
                            minlength: "Your phone number be at least 10 digits long"
                        },
			billing_address : "Please enter a valid address",
                        billing_city : "Please enter a valid city",
                        billing_state : "Please enter a valid state",
                        billing_zip : "Please enter a valid zip",
			card_name: "Please enter a valid card name",
			card_number: "Please enter a valid card number",
			card_cvc: "Please enter a valid card security code",
			card_expiry_month: "Please enter a valid card expiry month",
			card_expiry_year: "Please enter a valid card expiry year"
                        
                        
	    },
	    errorElement: "span",
	    errorPlacement: function(error, element) {
	      	if (element.hasClass('error')) {
	            // Error message                
	      	}
	      	element.after(error);
	    },
            	    submitHandler: function(form) {
           
               if(qryVar != "set"){
                   //alert('if');
                   	var username = $(".uname").val();
					var email = $(".email").val();
					$.ajax({          
	        			type : 'post',
	        			data : { action : "check_user_exist", username : username, email : email, qryVar : qryVar },
	       				success : function( response ) {
	          			if(response.status == true) {
	          				// disable the submit button to prevent repeated clicks
                           // alert('suceess');	
							$('#transax-submit').attr("disabled", "disabled");
							
								validatecarddetails();
	          	} else {
	          		$(".registeration-error").addClass('invalid');
	          		$(".registeration-error").html("<strong>Error:</strong> An account is already registered with your email address.");
	          		//$("<div class='registeration-error'><strong>Error:</strong> An account is already registered with your email address.</div>").insertBefore("#stripe-payment-form");
	          	}
        	}
        });
               }
               else{
                  
                   // disable the submit button to prevent repeated clicks
					$('#transax-submit').attr("disabled", "disabled");
                                        validatecarddetails();
					
               }
	
		// prevent the form from submitting with the default action
		return false;
  }
            
  	});
      jQuery('#transax-payment-form-reactive').validate({
	    onfocusout: function(element) {
	        
	    	this.element(element);
	    },    
	    rules: {     
			
			card_name: {
				required: true,
			},
			card_number: {
				required: true,
			},
			card_cvc: {
				required: true,
			},
			card_expiry_month: {
				required: true,
			},
			card_expiry_year: {
				required: true,
			},
                        billing_address: {
				required: true,
			},
                        billing_city: {
				required: true,
			},
                        billing_state: {
				required: true,
			},
                        billing_zip: {
				required: true,
                                maxlength: 6,
                                minlength: 4
			}
            	
			
	    },
	    messages: {
			
			billing_address : "Please enter a valid address",
                        billing_city : "Please enter a valid city",
                        billing_state : "Please enter a valid state",
                        billing_zip : "Please enter a valid zip",
			card_name: "Please enter a valid card name",
			card_number: "Please enter a valid card number",
			card_cvc: "Please enter a valid card security code",
			card_expiry_month: "Please enter a valid card expiry month",
			card_expiry_year: "Please enter a valid card expiry year"
                        
                        
	    },
	    errorElement: "span",
	    errorPlacement: function(error, element) {
	      	if (element.hasClass('error')) {
	            // Error message                
	      	}
	      	element.after(error);
	    },
            submitHandler: function(form) {
           
              
                  
                   // disable the submit button to prevent repeated clicks
					$('#transax-submit').attr("disabled", "disabled");
                    validatecarddetailsrReactive();
				
					// prevent the form from submitting with the default action
					return false;
			  }
            
  	});
   jQuery('.card-number').payment('formatCardNumber');
   jQuery('.card-number').payment('formatCardNumber');
	//$('.cc-exp').payment('formatCardExpiry');
	//$('.cc-cvc').payment('formatCardCVC');
	$.fn.toggleInputError = function(erred) {
		jQuery(this).toggleClass('error', erred);
		return this;
	};

	jQuery('#transax-payment-form .card-number').focusout(function(){
		var cardType = $.payment.cardType($('.card-number').val());
		jQuery('.payment-errors').toggleInputError(!$.payment.validateCardNumber(jQuery('#transax-payment-form .card-number').val(), cardType));
         
	});
	jQuery('#transax-payment-form-reactive .card-number').focusout(function(){
		var cardType = $.payment.cardType($('.card-number').val());
		jQuery('.payment-errors').toggleInputError(!$.payment.validateCardNumber(jQuery('#transax-payment-form-reactive .card-number').val(), cardType));
         
	});
   /*$("#transax-payment-form1").submit(function(event) { 
      // jQuery('#signup_modal .modal-footer').css('display','block !important');
      //alert('submit');
       
            if($("#transax_plan_id").val() != 3 ){
               // alert("here");
               // alert(qryVar);

               if(qryVar != "set"){
                  
                   	var username = $(".uname").val();
					var email = $(".email").val();
					$.ajax({          
	        			type : 'post',
	        			data : { action : "check_user_exist", username : username, email : email, qryVar : qryVar },
	       				success : function( response ) {
	          			if(response.status == true) {
	          				// disable the submit button to prevent repeated clicks
                            	
							$('#transax-submit').attr("disabled", "disabled");
							//jQuery('#submit-signup-term').attr("disabled", "disabled");
							// send the card details to Stripe
							/*Stripe.createToken({
								name: $('.card-name').val(),
								number: $('.card-number').val(),
								cvc: $('.card-cvc').val(),
								exp_month: $('.card-expiry-month').val(),
								exp_year: $('.card-expiry-year').val()
								}, stripeResponseHandler);*/
							//	validatecarddetails();
	          /*	} else {
	          		$(".registeration-error").addClass('invalid');
	          		$(".registeration-error").html("<strong>Error:</strong> An account is already registered with your email address.");
	          		//$("<div class='registeration-error'><strong>Error:</strong> An account is already registered with your email address.</div>").insertBefore("#stripe-payment-form");
	          	}
        	}
        });
               }
               else{
                  
                   // disable the submit button to prevent repeated clicks
					$('#transax-submit').attr("disabled", "disabled");
                                        validatecarddetails();
					//jQuery('#submit-signup-term').attr("disabled", "disabled");
					// send the card details to Stripe
					/*Stripe.createToken({
						name: $('.card-name').val(),
						number: $('.card-number').val(),
						cvc: $('.card-cvc').val(),
						exp_month: $('.card-expiry-month').val(),
						exp_year: $('.card-expiry-year').val()
					}, stripeResponseHandler);*/
            /*   }
	
		// prevent the form from submitting with the default action
		return false;
            } 
            
   });*/
});

function validatecarddetails(){
 // alert('carddetails');
    jQuery('.payment-errors').html('');
	name = jQuery('.card-name').val();
	number = jQuery('.card-number').val();
	cvc = jQuery('.card-cvc').val();
	exp_month = jQuery('.card-expiry-month').val();
	exp_year = jQuery('.card-expiry-year').val();
	var isCardValid = $.payment.validateCardNumber(number);
    var isCvvValid = $.payment.validateCardCVC(cvc);
    

    var today;
	var exMonth=exp_month;
	var exYear=exp_year;
	today = new Date();

		if(isCardValid == false){
			jQuery('.payment-errors').html('Invalid card number');
			jQuery('#transax-submit').attr("disabled", false);
		}
		else if(isCvvValid == false){
			jQuery('.payment-errors').html('Invalid cvv number');
			jQuery('#transax-submit').attr("disabled", false);
		}
		else if (exMonth > 12) {
			jQuery('.payment-errors').html('Invalid expiry month');
			jQuery('#transax-submit').attr("disabled", false);
   		}	
		else if((exYear < today.getFullYear())){
				jQuery('.payment-errors').html('Invalid expiry year');
				jQuery('#transax-submit').attr("disabled", false);
		}
		else{
			jQuery('#transax-submit').attr("disabled", "disabled");
    		Processpayment();
		}

    
	
}

function validatecarddetailsrReactive(){
 // alert('carddetails');
    jQuery('.payment-errors').html('');
	name = jQuery('.card-name').val();
	number = jQuery('.card-number').val();
	cvc = jQuery('.card-cvc').val();
	exp_month = jQuery('.card-expiry-month').val();
	exp_year = jQuery('.card-expiry-year').val();
	var isCardValid = $.payment.validateCardNumber(number);
    var isCvvValid = $.payment.validateCardCVC(cvc);
    

    var today;
	var exMonth=exp_month;
	var exYear=exp_year;
	today = new Date();

		if(isCardValid == false){
			jQuery('.payment-errors').html('Invalid card number');
			jQuery('#transax-submit').attr("disabled", false);
		}
		else if(isCvvValid == false){
			jQuery('.payment-errors').html('Invalid cvv number');
			jQuery('#transax-submit').attr("disabled", false);
		}
		else if (exMonth > 12) {
			jQuery('.payment-errors').html('Invalid expiry month');
			jQuery('#transax-submit').attr("disabled", false);
   		}	
		else if((exYear < today.getFullYear())){
				jQuery('.payment-errors').html('Invalid expiry year');
				jQuery('#transax-submit').attr("disabled", false);
		}
		else{
			jQuery('#transax-submit').attr("disabled", "disabled");
    		ProcesspaymentReactive();
		}

    
	
}

function ProcesspaymentReactive(){
       // alert('processpayment');
	var user_id = jQuery('#user_id').val();
       // console.log(user_id);
        if(user_id == ''){
        	jQuery("#signup_modal").modal('show');
                
        //jQuery(".modal-footer").removeClass("conditional_footer");
        // jQuery('#signup_modal .conditional_footer').css('display','block');
        jQuery("#signup_modal .modal-footer").css("cssText", "display: block;");
        jQuery('#transax-submit').attr("disabled", false);
        jQuery('#btn_signup_accept').click(function(){
           // alert('click if');
           jQuery('#transax-submit').attr("disabled", true);
            $('#transax-payment-form-reactive')[0].submit();
           
        	
        });
    }
    else{
        
            $('#transax-payment-form-reactive')[0].submit();
        act = jQuery( '#transax-payment-form-reactive' ).attr( 'action' );
    
    }
}

function Processpayment(){
       // alert('processpayment');
	var user_id = jQuery('#user_id').val();
       // console.log(user_id);
        if(user_id == ''){
        	jQuery("#signup_modal").modal('show');
                
        //jQuery(".modal-footer").removeClass("conditional_footer");
        // jQuery('#signup_modal .conditional_footer').css('display','block');
        jQuery("#signup_modal .modal-footer").css("cssText", "display: block;");
        jQuery('#transax-submit').attr("disabled", false);
        jQuery('#btn_signup_accept').click(function(){
           // alert('click if');
           jQuery('#transax-submit').attr("disabled", true);
            $('#transax-payment-form')[0].submit();
           
        	
        });
    }
    else{
        
            $('#transax-payment-form')[0].submit();
        act = jQuery( '#transax-payment-form' ).attr( 'action' );
    
    }
}