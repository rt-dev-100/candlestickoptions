<?php
ini_set('display_errors', '1'); 

// Load nusoap code
require_once('nusoap.php'); 
require_once('../../../../../wp-load.php');

// Create client
$plan_id = $_POST['plan_id'];
$amount =str_replace("$","",$_POST['amount']);
$initial_amount =str_replace("$","",$_POST['initial_amount']);

$user_id = get_current_user_id(); 
$user_meta = get_user_meta($user_id);

$user = wp_get_current_user(); 
$to = $user->data->user_email;
$fname= $user->data->user_nicename;
$recuuring_data  = $user_meta['transax_recurring_id'];
$recurring_id    = $recuuring_data[0];
$old_amount  = $user_meta['amount'][0];

/**
 * [getTransactionId]
 * @param  [type] $params1 
 * @return [type] 
 */
function getTransactionId($params1){
	// Get TransactionId 
	$d=strtotime("-1 Day");
	$current_dt=date('Y-m-d H:i:s',$d);
	$searchStartDate = date('m/d/Y', strtotime($current_dt));
	$postData = [
		'transactionAPIParams'  => [
			'TransactionAPIKey' => TRANSACTIONAPIKEY,
			'GatewayUserName'   => GATEWAYUSERNAME,
			'GatewayPassword'   => GATEWAYPASSWORD,
			'AccountNumber'     => ACCOUNTNUMBER,
			'StartDate'         => $searchStartDate,
			'Email'             => $params1['EMail'],
			'CreditCardLast4'   => substr($params1['CCNumber'], -4),
		]
	];
	$postData = json_encode($postData);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,"https://secure.gwintegration.com/WebAPI/RoXWeb.aspx/TransactionAPI");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$server_output = curl_exec($ch);
	curl_close ($ch);
	$server_output =  json_decode($server_output);
	$server_output->d = array_reverse($server_output->d);
	$transactionIdCount = count($server_output->d);
	return $transactionId = $server_output->d[$transactionIdCount-1];
}

/**
 * [removeRecurring]
 * @return [String]
 */
function removeRecurring($userMeta, $userId, $refundType){
	$recuuring_data             = $userMeta['transax_recurring_id'];
	$recurring_id               = $recuuring_data[0];
	$plan_id                    = $userMeta['plan_name'][0];
	$soapclient1 = new nusoap_client('https://secure.transaxgateway.com/roxapi/recurring.asmx?WSDL', 'wsdl');
	$params4['GatewayUserName'] = GATEWAYUSERNAME;
	$params4['GatewayPassword'] = GATEWAYPASSWORD;
	$params4['MerchantID']      = MERCHANTID;
	$params4['RecurringID']     = $recurring_id;
	$proxy1                     = $soapclient1->getProxy();
	$result4                    = $proxy1->RecurringDelete(array('vRecurringParams' => $params4));
	$recurring_result           = $result4['RecurringDeleteResult'];

	if ($recurring_result['RESULT'] == 1) {
		update_userMeta($userId, 'transax_recurring_id', '');
		$d            = strtotime("now");
		$current_time = date('Y-m-d H:i:s', $d);
		$d2=strtotime("+1000 Years ");
		 $end_dt = date('Y-m-d H:i:s',$d2);
		update_userMeta($userId, 'current_period_end', $current_time);

		if($refundType == 'refund'){
			$action = "Refund";
			$removeRecurring = removeRecurringwithrefund($userMeta, $userId);
		} else {
			$action = "Deleted";
		}

		$curr_state = ['current_period_start' => $current_time, 'current_period_end' => $end_dt, 'plan_id' => $plan_id, 'transax_recurring_id' => $recurring_id, 'transactionId'=>$removeRecurring['TRANS_ID'], 'cartNumber'=>'' , 'amount' => $userMeta['amount'][0], 'action' => $action, 'state' => $refundType];

		if (isset($userMeta['subscribe_history']) && $userMeta['subscribe_history']) {
			$history = unserialize($userMeta['subscribe_history'][0]);
		} else { 
			trim($_POST['TransactionType']);    
			$history = [];
		}
		array_push($history, $curr_state);
		update_userMeta($userId, 'subscribe_history', $history);
	} 
	return true;
}

/**
 * [removeRecurringwithrefund]
 * @return [String]
 */
function removeRecurringwithrefund($userMeta, $userId){
	$recuuring_data             = $userMeta['transax_recurring_id'];
	$recurring_id               = $recuuring_data[0];
	$params4['GatewayUserName'] = GATEWAYUSERNAME;
	$params4['GatewayPassword'] = GATEWAYPASSWORD;
	$params4['MerchantID']      = MERCHANTID;
	$params4['RecurringID']     = $recurring_id;
	$params4['TransactionType'] = 'refund';  
	$params4['TransactionID']   = $userMeta['transactionId'][0];
	$params4['Amount']          = $userMeta['amount'][0];
	
	$soapclient1 = new nusoap_client('https://secure.transaxgateway.com/roxapi/rox.asmx?WSDL', 'wsdl');
	$proxy1      = $soapclient1->getProxy();
	$result4     = $proxy1->ProcessTransaction(array('objparameters' => $params4));
 
	$recurring_result = $result4['ProcessTransactionResult'];
	return $recurring_result;
}


/**
 * [dateDiff]
 * @return [days]
 */
function dateDiff ($d1, $d2) {
	return round(abs(strtotime($d1)-strtotime($d2))/86400);
} 

if(wp_verify_nonce(trim($_POST['transax_nonce']), 'transax_nonce')){

	try{

		/* Refund Code Start */
		$date=strtotime("now");
		$currentDate = date('Y-m-d',$date);
		$subscribeHistory = unserialize($user_meta['subscribe_history'][0]);
		$registredPeriodStartDate = $subscribeHistory[0]['current_period_start'];
		$registredPeriodStartDate = date('Y-m-d', strtotime($registredPeriodStartDate));

		$currentPeriodStart  = $user_meta['current_period_start'][0];
		$lastPeriodStartDate = $currentPeriodStart;
		$currentPeriodStart = date('Y-m-d', strtotime($currentPeriodStart));

		// Registered to Current Date diff [30 Day]
		$registredToCurrentdatediff = dateDiff($registredPeriodStartDate, $currentDate);

		// Registered to change period diff [30 Day]
		$registredToChangedatediff = dateDiff($currentPeriodStart, $registredPeriodStartDate);

		// Current date to period startdate
		$get7daysDifffromcurrent = dateDiff($currentDate, $currentPeriodStart);

		$soapclient1 = new nusoap_client('https://secure.transaxgateway.com/roxapi/recurring.asmx?WSDL','wsdl');
		$params1['GatewayUserName'] = GATEWAYUSERNAME;
		$params1['GatewayPassword'] = GATEWAYPASSWORD;
		$params1['MerchantID'] = MERCHANTID;
		$params1['InitialAmount'] = $initial_amount;
		$params1['RecurringAmount'] = $amount;
		$params1['Frequency'] = "months";
		$params1['Quantity'] = "1";
		$params1['NumberOfRetries'] = "2";
		$params1['PaymentType'] = trim($_POST['PaymentType']);
		$params1['CCNumber'] = str_replace(' ', '', trim($_POST['card_number']));
		$expiry_month = trim($_POST['card_expiry_month']);
		$expiry_year = trim($_POST['card_expiry_year']);
		$expiry_year =  substr($expiry_year, -2); 
		$params1['CCExpDate'] = $expiry_month.$expiry_year;
		$params1['CVV'] = trim($_POST['card_cvc']);
		$params1['EMail'] = $to;
		$params1['FirstName'] = $fname;
		$params1['LastName'] = " ";
		$params1['Address1'] = trim($_POST['billing_address']); //Los Angeles
		$params1['City'] = trim($_POST['billing_city']); //Los Angeles
		$params1['State'] = trim($_POST['billing_state']); //california
		$params1['Zip'] = trim($_POST['billing_zip']);
		$proxy1 = $soapclient1->getProxy(); 

		if($recurring_id) {
			if($registredToCurrentdatediff < 30){
				$params1['TransactionType'] = trim($_POST['TransactionType']);
				$params1['StartDate'] = date("Y/m/d");
				$result1 = $proxy1->RecurringAdd(Array('vRecurringParams' => $params1));
				$response = $result1['RecurringAddResult'];
				$recurringid = $response['RecurringID'];
				if($old_amount == 8){
					$planstart = $params1['StartDate'];
					removeRecurring($user_meta, $user_id, 'norefund'); 
				} else if($get7daysDifffromcurrent < 7){
					removeRecurring($user_meta, $user_id, 'refund'); 	
					$planstart = $currentPeriodStart;
				} else {
					removeRecurring($user_meta, $user_id, 'norefund'); 	
					$planstart = $currentPeriodStart;
				}
			} else if($registredToChangedatediff > 30) {
				$params1['TransactionType'] = trim($_POST['TransactionType']);
				$params1['StartDate'] = date("Y/m/d");
				$result1 = $proxy1->RecurringAdd(Array('vRecurringParams' => $params1));
				$response = $result1['RecurringAddResult'];
				$recurringid = $response['RecurringID'];
				$planstart = $currentPeriodStart;

				if($get7daysDifffromcurrent < 7) {
					removeRecurring($user_meta, $user_id, 'refund'); 
				} else {
					removeRecurring($user_meta, $user_id, 'norefund'); 
				}
			} else {
				$params1['TransactionType'] = "update"; 
				$params1['StartDate'] = date('Y/m/d', strtotime($registredPeriodStartDate.'+1 month'));
				$params1['RecurringID'] = $recurring_id;
				$result1 = $proxy1->RecurringUpdate(Array('vRecurringParams' => $params1));
				$response = $result1['RecurringUpdateResult'];
				$recurringid = $recurring_id;
				$planstart = $registredPeriodStartDate;
			}
			$stateAction = "planchange";

		} else {
			$params1['TransactionType'] = trim($_POST['TransactionType']);
			$params1['StartDate'] = date("Y/m/d");
			$result1 = $proxy1->RecurringAdd(Array('vRecurringParams' => $params1));
			$response = $result1['RecurringAddResult'];
			$recurringid = $response['RecurringID'];
			$planstart = $params1['StartDate'];
			$stateAction = "Re Active";
		}

		if($response['RESULT'] == '1') {

			$redirect = add_query_arg('payment', 'paid', $_POST['redirect']);
			$to = $user->data->user_email;
			$fname= $user->data->user_nicename;
			$subject = 'Greetings!..1234investors.com';
			$headers = "MIME-Version: 1.0" . "\r\n";   
			$headers .= "Content-Type: text/html; charset=UTF-8";
			$headers .= 'From:  1234ivestors.com <ds.sparkle018@gmail.com>' . "\r\n"; 
			$headers .= "Reply-To: 1234ivestors.com <ds.sparkle018@gmail.com>" . "\r\n"; 
			$body = '<html><body><span>Hello,'.$fname.', </span><br><br>';
			$body .= '<span>Thanks for registering with us..</span><br><br>';
			$body .= '<span>We received the payment and your subscription is started. Keep in touch.</span><br><br>';
			$body .= '<span>Thank you!</span>';
			$body .= '</body></html>';
			$mailResult = wp_mail($to, $subject, $body, $headers);
			 
			update_user_meta($user_id, 'transax_response', $response);
			//update_user_meta($user_id, 'stripe_customer_id', $stripe_customer_id);
			update_user_meta($user_id, 'transax_recurring_id', $recurringid);
			update_user_meta($user_id, 'plan_name', $plan_id);
			// $d=strtotime("now");
			$d = strtotime($planstart);
			$current_dt = date('Y-m-d H:i:s',$d);
			$d2 = strtotime("+1000 Years ");
			$end_dt = date('Y-m-d H:i:s',$d2);

			$server_output = getTransactionId($params1);
			$last4DigitCartNumber = substr($_POST['card_number'], -4);

			update_user_meta($user_id, 'current_period_start', $current_dt);
			update_user_meta($user_id, 'current_period_end',$end_dt);
			update_user_meta($user_id,'address',trim($_POST['billing_address']));
			update_user_meta($user_id,'city',trim($_POST['billing_city']));
			update_user_meta($user_id,'state',trim($_POST['billing_state']));
			update_user_meta($user_id,'zip_code',trim($_POST['billing_zip']));

			$curr_state=['current_period_start'=>$current_dt,'current_period_end'=>$end_dt,'plan_id'=>$plan_id,'transax_recurring_id'=>$recurringid, 'transactionId'=>$server_output->responsetransid, 'cartNumber'=>$last4DigitCartNumber, 'amount' => $amount, 'action'=>$stateAction,'state'=>'reactive'];

			if(isset($user_meta['subscribe_history']) && $user_meta['subscribe_history']) {
				$history= unserialize($user_meta['subscribe_history'][0]);
			} else {
				$history=[];
			}
		
			array_push($history, $curr_state);
			update_user_meta($user_id, 'subscribe_history', $history);
			update_user_meta($user_id, 'transactionId', $server_output->responsetransid);
			update_user_meta($user_id, 'amount', $amount);
			update_user_meta($user_id, 'cartNumber', $last4DigitCartNumber);

		} else {
			$redirect = add_query_arg('payment', 'failed', trim($_POST['redirect']));
		}
	}
	catch(Exception $e){
		$redirect = add_query_arg('payment', 'failed', trim($_POST['redirect']));
 	}
 	// die;
 	wp_redirect($redirect); exit;
} 