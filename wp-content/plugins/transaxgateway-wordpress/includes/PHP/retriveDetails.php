<?php
ini_set('display_errors', '1');
//load nusoap code
require_once 'nusoap.php';
require_once '../../../../../wp-load.php';

/**
 * [removeRecurring]
 * @return [String]
 */
function removeRecurring($userMeta, $userId, $refundType){
    $recuuring_data             = $userMeta['transax_recurring_id'];
    $recurring_id               = $recuuring_data[0];
    $plan_id                    = $userMeta['plan_name'][0];
    $soapclient1 = new nusoap_client('https://secure.transaxgateway.com/roxapi/recurring.asmx?WSDL', 'wsdl');
    $params4['GatewayUserName'] = GATEWAYUSERNAME;
    $params4['GatewayPassword'] = GATEWAYPASSWORD;
    $params4['MerchantID']      = MERCHANTID;
    $params4['RecurringID']     = $recurring_id;
    $proxy1                     = $soapclient1->getProxy();
    $result4                    = $proxy1->RecurringDelete(array('vRecurringParams' => $params4));

    $recurring_result           = $result4['RecurringDeleteResult'];
  
    if ($recurring_result['RESULT'] == 1) {
        update_userMeta($userId, 'transax_recurring_id', '');
        $d            = strtotime("now");
        $current_time = date('Y-m-d H:i:s', $d);
        $d2=strtotime("+1000 Years ");
         $end_dt = date('Y-m-d H:i:s',$d2);
        update_userMeta($userId, 'current_period_end', $current_time);

        if($refundType == 'refund'){
            $action = "Refund";
            $removeRecurring = removeRecurringwithrefund($userMeta, $userId);
        } else {
            $action = "Deleted";
        }

        $curr_state = ['current_period_start' => $current_time, 'current_period_end' => $end_dt, 'plan_id' => $plan_id, 'transax_recurring_id' => $recurring_id, 'transactionId'=>$removeRecurring['TRANS_ID'], 'cartNumber'=>'', 'amount' => $userMeta['amount'][0], 'action' => $action, 'state' => $refundType];

        if (isset($userMeta['subscribe_history']) && $userMeta['subscribe_history']) {
            $history = unserialize($userMeta['subscribe_history'][0]);
        } else { trim($_POST['TransactionType']);    
            $history = [];
        }
        array_push($history, $curr_state);
        update_userMeta($userId, 'subscribe_history', $history);
        echo 'success';
    } else {
        echo 'fail';
    }
}

/**
 * [removeRecurringwithrefund]
 * @return [String]
 */
function removeRecurringwithrefund($userMeta, $userId){
    $recuuring_data             = $userMeta['transax_recurring_id'];
    $recurring_id               = $recuuring_data[0];
    $params4['GatewayUserName'] = GATEWAYUSERNAME;
    $params4['GatewayPassword'] = GATEWAYPASSWORD;
    $params4['MerchantID']      = MERCHANTID;
    $params4['RecurringID']     = $recurring_id;
    $params4['TransactionType'] = 'refund';  
    $params4['TransactionID']   = $userMeta['transactionId'][0];
    $params4['Amount']          = $userMeta['amount'][0];
  
    $soapclient1 = new nusoap_client('https://secure.transaxgateway.com/roxapi/rox.asmx?WSDL', 'wsdl');
    $proxy1      = $soapclient1->getProxy();
    $result4     = $proxy1->ProcessTransaction(array('objparameters' => $params4));
   
    $recurring_result = $result4['ProcessTransactionResult'];
    
    return $recurring_result;
}

/**
 * [getTransactionId]
 * @param  [type] $params1 
 * @return [type] 
 */
function getTransactionId($params1){
    // Get TransactionId 
    $d=strtotime("-1 Day");
    $current_dt=date('Y-m-d H:i:s',$d);
    $searchStartDate = date('m/d/Y', strtotime($current_dt));
    $postData = [
        'transactionAPIParams'  => [
            'TransactionAPIKey' => TRANSACTIONAPIKEY,
            'GatewayUserName'   => GATEWAYUSERNAME,
            'GatewayPassword'   => GATEWAYPASSWORD,
            'AccountNumber'     => ACCOUNTNUMBER,
            'StartDate'         => $searchStartDate,
            'Email'             => $params1['EMail'],
            'CreditCardLast4'   => substr($params1['CCNumber'], -4),
        ]
    ];
    $postData = json_encode($postData);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,"https://secure.gwintegration.com/WebAPI/RoXWeb.aspx/TransactionAPI");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $server_output = curl_exec($ch);
    curl_close ($ch);
    $server_output =  json_decode($server_output);
    $server_output->d = array_reverse($server_output->d);
    // $transactionIdCount = count($server_output->d);
    // return $transactionId = $server_output->d[$transactionIdCount-1];   
    return $server_output->d;
}


$date        = strtotime("now");
$currentDate = date('Y-m-d H:i:s',$date);
$userId      = get_current_user_id();

if($userId) {

    $userMeta = get_user_meta($userId);
    $user = wp_get_current_user(); 
    $Email = $user->data->user_email;
    $card= $user->data->user_nicename;
 
    $params1['EMail'] = $Email;
    $params1['CCNumber'] = $userMeta['cartNumber'][0];
 
    $transactionHistory = getTransactionId($params1);


    $subscribeHistory = unserialize($userMeta['subscribe_history'][0]);

    $subscribeHistoryCount = count($subscribeHistory);
    $lastPlanDetails = $subscribeHistory[$subscribeHistoryCount-1];   
    
    foreach ($transactionHistory as $key => $value) {
        echo '<pre/>';
        print_r($value);
        print_r(get_bloginfo('admin_email'));
            exit;
        if($value['responsestatuscode'] == '1' && $value['responsestatusmsg'] == 'Approved') {
            if(!in_array($value->responsetransid, array_column($subscribeHistory, 'transactionId'))){

                $transactionDate = date('Y-m-d H:i:s', strtotime($value->transactiondate));
                $t = strtotime($transactionDate);
                $t2 = strtotime('+1000 Years', $t);
                $end_dt = date('Y-m-d H:i:s',$t2);

                $curr_state=[
                    'current_period_start'  => $transactionDate,
                    'current_period_end'    => $end_dt,
                    'plan_id'               => $lastPlanDetails['plan_id'],
                    'transax_recurring_id'  => $lastPlanDetails['transax_recurring_id'], 
                    'transactionId'         => $value->responsetransid, 
                    'cartNumber'            => $params1['CCNumber'], 
                    'amount'                => $value->amount, 
                    'action'                => 'Recurring',
                    'state'                 => 'active'
                ];

                array_push($history ,$curr_state);

                update_user_meta($userId,'subscribe_history',$history);
                update_user_meta($userId,'transactionId',$value->responsetransid);
                update_user_meta($userId,'amount',$value->amount);
                update_user_meta($userId,'cartNumber',$params1['CCNumber']);

            }
        } else {
            

            $adminemail      = get_bloginfo('admin_email'));
            $ccnumber        = $value['ccnumber'];
            $ccexpdate       = $value['ccexpdate'];
            $cardtype        = $value['cardtype'];
            $amount          = $value['amount'];
            $paymenttype     = $value['paymenttype'];
            $id              = $value['id'];
            $transactiondate = $value['transactiondate'];

            // User email send
            $to = $value['email'];
            $subject = 'Greetings!..1234investors.com';
            $headers = "MIME-Version: 1.0" . "\r\n";   
            $headers .= "Content-Type: text/html; charset=UTF-8";
            $headers .= 'From:  1234ivestors.com <ds.sparkle018@gmail.com>' . "\r\n"; 
            $headers .= "Reply-To: 1234ivestors.com <ds.sparkle018@gmail.com>" . "\r\n"; 
            $body = '<html><body><span>Hello,'.$value['firstname'].' '.$value['lastname'].', </span><br><br>';
            $body .= '<span>Currently Your recurring had been remove because creditcard transaction has been fail on this date '.$transactiondate.' and amount is '.$amount.'..</span><br><br>';
            $body .= '<span>Please update your credit card detail and continue your recurring.</span><br><br>';
            $body .= '<span>Thank you!</span>';

            $body .= '</body></html>';
            $mailResult = wp_mail($to, $subject, $body, $headers);


            // Admin Mail
            $subject1 = 'User recurrign fail!..1234investors.com ~ '.$value['firstname'].' '.$value['lastname'];
            $headers1 = "MIME-Version: 1.0" . "\r\n";   
            $headers1 .= "Content-Type: text/html; charset=UTF-8";
            $headers1 .= 'From:  1234ivestors.com <ds.sparkle018@gmail.com>' . "\r\n"; 
            $headers1 .= "Reply-To: 1234ivestors.com <ds.sparkle018@gmail.com>" . "\r\n"; 
            $body1 = '<html><body><span>Hello Admin,</span><br><br>';
            $body1 .= '<span>Currently '.$to.' recurring had been remove because creditcard transaction has been fail on this date '.$transactiondate.' and amount is '.$amount.'..</span><br><br>';
            $body1 .= '<span>Please contact this persion and update his recurring .</span><br><br>';
            $body1 .= '<span>Thank you!</span>';

            $body .= '</body></html>';
            $mailResult = wp_mail($adminemail, $subject1, $body1, $headers1);
            


            removeRecurring($userMeta, $userId, 'norefund');

        }
    }
    echo 1;
} else {
    // No User login
    echo 0;
}
