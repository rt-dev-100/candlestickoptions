<?php
ini_set('display_errors', '1');
//load nusoap code
require_once 'nusoap.php';
require_once '../../../../../wp-load.php';

/**
 * [removeRecurring]
 * @return [String]
 */
function removeRecurring($userMeta, $userId, $refundType){
    $recuuring_data             = $userMeta['transax_recurring_id'];
    $recurring_id               = $recuuring_data[0];
    $plan_id                    = $userMeta['plan_name'][0];
    $soapclient1 = new nusoap_client('https://secure.transaxgateway.com/roxapi/recurring.asmx?WSDL', 'wsdl');
    $params4['GatewayUserName'] = GATEWAYUSERNAME;
    $params4['GatewayPassword'] = GATEWAYPASSWORD;
    $params4['MerchantID']      = MERCHANTID;
    $params4['RecurringID']     = $recurring_id;
    $proxy1                     = $soapclient1->getProxy();
    $result4                    = $proxy1->RecurringDelete(array('vRecurringParams' => $params4));

    $recurring_result           = $result4['RecurringDeleteResult'];
  
    if ($recurring_result['RESULT'] == 1) {
        update_userMeta($userId, 'transax_recurring_id', '');
        $d            = strtotime("now");
        $current_time = date('Y-m-d H:i:s', $d);
        $d2=strtotime("+1000 Years ");
         $end_dt = date('Y-m-d H:i:s',$d2);
        update_userMeta($userId, 'current_period_end', $current_time);

        if($refundType == 'refund'){
            $action = "Refund";
            $removeRecurring = removeRecurringwithrefund($userMeta, $userId);
        } else {
            $action = "Deleted";
        }

        $curr_state = ['current_period_start' => $current_time, 'current_period_end' => $end_dt, 'plan_id' => $plan_id, 'transax_recurring_id' => $recurring_id, 'transactionId'=>$removeRecurring['TRANS_ID'], 'cartNumber'=>'', 'amount' => $userMeta['amount'][0], 'action' => $action, 'state' => $refundType];

        if (isset($userMeta['subscribe_history']) && $userMeta['subscribe_history']) {
            $history = unserialize($userMeta['subscribe_history'][0]);
        } else { trim($_POST['TransactionType']);    
            $history = [];
        }
        array_push($history, $curr_state);
        update_userMeta($userId, 'subscribe_history', $history);
        echo 'success';
    } else {
        echo 'fail';
    }
}

/**
 * [removeRecurringwithrefund]
 * @return [String]
 */
function removeRecurringwithrefund($userMeta, $userId){
    $recuuring_data             = $userMeta['transax_recurring_id'];
    $recurring_id               = $recuuring_data[0];
    $params4['GatewayUserName'] = GATEWAYUSERNAME;
    $params4['GatewayPassword'] = GATEWAYPASSWORD;
    $params4['MerchantID']      = MERCHANTID;
    $params4['RecurringID']     = $recurring_id;
    $params4['TransactionType'] = 'refund';  
    $params4['TransactionID']   = $userMeta['transactionId'][0];
    $params4['Amount']          = $userMeta['amount'][0];
  
    $soapclient1 = new nusoap_client('https://secure.transaxgateway.com/roxapi/rox.asmx?WSDL', 'wsdl');
    $proxy1      = $soapclient1->getProxy();
    $result4     = $proxy1->ProcessTransaction(array('objparameters' => $params4));
   
    $recurring_result = $result4['ProcessTransactionResult'];
    
    return $recurring_result;
}

/**
 * [dateDiff]
 * @return [days]
 */
function dateDiff ($d1, $d2) {
    return round(abs(strtotime($d1)-strtotime($d2))/86400);
} 

$date=strtotime("now");
$currentDate = date('Y-m-d',$date);
$userId   = get_current_user_id();
$userMeta = get_user_meta($userId);

$currentPeriodStart = $userMeta['current_period_start'][0];
$subscribeHistory = unserialize($userMeta['subscribe_history'][0]);

$lastPeriodStartDate = $currentPeriodStart;
$lastPeriodStartDate = date('Y-m-d', strtotime($lastPeriodStartDate));
$registredAction = $subscribeHistory[0]['action'];
$registredPeriodStartDate = $subscribeHistory[0]['current_period_start'];
$registredPeriodStartDate = date('Y-m-d', strtotime($registredPeriodStartDate));

$registredToCurrentdatediff = dateDiff($currentDate, $registredPeriodStartDate);
$registredToChangedatediff = dateDiff($registredPeriodStartDate, $lastPeriodStartDate);


/* Condition of first month of 30 days and other month under 7 days refund call */
if($registredToCurrentdatediff < 30 || $registredToChangedatediff < 30){
    removeRecurring($userMeta, $userId, 'norefund'); // Start Date Change only
} else {
    $get7daysDifffromcurrent = dateDiff($currentDate, $registredPeriodStartDate);
    if($get7daysDifffromcurrent < 7){
        removeRecurring($userMeta, $userId, 'refund');
    } else {
        removeRecurring($userMeta, $userId, 'norefund');
    }   
}