<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD 
name=Head1>
		<title>NELiX TransaX Rox</title>
		<link href="Styles.css" rel="stylesheet" type="text/css">
		<script type="text/JavaScript">
<!--
function MM_openBrWindow(theURL,winname,features) { //v2.0
  window.open(theURL,winname,features);
}
//-->
		</script>
</HEAD>
	<body>
		<form name="form1" action="GatewayDemoProcess.php" method="post">
			<div class="top_nav_layout">
				<div class="top_nav">
					<br>
					&nbsp;</div>
			</div>
			<div class="masthead">
				<div class="transax_logo"><a href="#"><img src="images/transax_logo.gif" alt="NELiX transaX Inc. - Your full service payment processing provider."
							border="0"></a></div>
				<div class="transax_logo_print">
					&nbsp;</div>
			</div>
			<br/>
			<table border="0" cellspacing="0" cellpadding="0" class="main_box">
				<tr>
					<td class="mb_topleft" width=20 height=20><img src="images/spacer.gif" width="20" alt="" height="20"></td>
					<td class="mb_top" height=20>&nbsp;</td>
					<td class="mb_topright" width=20 height=20><img src="images/spacer.gif" width="20" alt="" height="20"></td>
				</tr>
				<tr>
					<td class="mb_left" width=20>&nbsp;</td>
					<td class="mb_header"><h1>
							NELiX TransaX Gateway Web Service API Demo&nbsp;</h1>
					</td>
					<td class="mb_right" width=20>&nbsp;</td>
				</tr>
				<tr>
					<td class="mb_left" width=20>&nbsp;</td>
					<td class="mb_main">
						<p align="left">
							<asp:Label name="MSG" ForeColor="Red">
                            </asp:Label></p>
						<table name="table1" class="portalhtml" align="center" cellpadding="0" cellspacing="0">
        <TBODY class=inputd>
							<tr>
								<th colspan="4">
									Gateway API Demonstration</th>
							</tr>
							<tr>
								<td width=115><FONT class=inputd 
            size=2>TransactionType</FONT></td>
								<td colspan="1" width="225">
									<select CssClass="inputd" name="TransactionType">
										<option></option>
										<option>
                                    sale</option>
										<option>auth</option>
										<option>capture</option>
										<option>refund</option>
										<option>void</option>
										<option>update</option>
									</select >
								</td>
								<td colspan="2">
									&nbsp;</td>
							</tr>
						
							<tr>
								<td width=115>
									PaymentType</td>
								<td colspan="1">
                                <select CssClass="inputd" name="PaymentType">
										<option Selected="True">creditcard</option>
										<option>check</option>
									</select ></td>
								<td colspan="1">
									&nbsp;TransactionID</td>
								<td colspan="1">
									<Input Name="TransactionID" CssClass="inputd" size="20">
                                    </td>
							</tr>
							<tr>
								<td colspan="4">
									&nbsp;</td>
							</tr>
							<tr>
								<th colspan="4">
									Credit Card Info</th>
							</tr>
							<tr>
								<td width=115>
									CCNumber</td>
								<td colspan="1">
									<Input Name="CCNumber" CssClass="inputd" size="20">
                                    </td>
								<td colspan="1">
									CCExpDate (MMDD)</td>
								<td colspan="1">
									<Input Name="CCExpDate" CssClass="inputd" size="20">
                                    </td>
							</tr>
							<tr>
								<td width=115>
									CVV</td>
								<td colspan="1">
									<Input Name="CVV" CssClass="inputd" MaxLength="5" Width="50px" size="20">
                                    </td>
								<td colspan="1">
									&nbsp;</td>
								<td colspan="1">
									&nbsp;</td>
							</tr>
							<tr>
								<td colspan="4">
									&nbsp;</td>
							</tr>
							<tr>
								<th colspan="4">
									Check (ACH) Info</th>
							</tr>
							<tr>
								<td width=115>
									CheckName</td>
								<td colspan="3">
									<Input Name="CheckName" CssClass="inputd" Width="350px" size="20">
                                    </td>
							</tr>
							<tr>
								<td width=115>
									CheckABA</td>
								<td colspan="1">
									<Input Name="CheckABA" CssClass="inputd" size="20">
                                    </td>
								<td colspan="1">
									CheckAccount</td>
								<td colspan="1">
									<Input Name="CheckAccount" CssClass="inputd" size="20">
                                    </td>
							</tr>
							<tr>
								<td width=115>
									AccountHolderType</td>
								<td colspan="1">
                                <select CssClass="inputd" name="AccountHolderType">
										<option></option>
										<option>business</option>
										<option>personal</option>
									</select ></td>
								<td colspan="1">
									AccountType</td>
								<td colspan="1">
                                <select CssClass="inputd" name="AccountType">
										<option Selected="True">
                                </option>
										<option>checking</option>
										<option>savings</option>
									</select ></td>
							</tr>
							<tr>
								<td width=115>
									SecCode</td>
								<td colspan="1">
                                <select CssClass="inputd" name="SecCode">
										<option></option>
										<option>PPD</option>
										<option>WEB</option>
										<option>TEL</option>
										<option>CCD</option>
									</select ></td>
								<td colspan="1">
									&nbsp;</td>
								<td colspan="1">
									&nbsp;</td>
							</tr>
							<tr>
								<td colspan="4">
									&nbsp;</td>
							</tr>
							<tr>
								<th colspan="4">
									Transaction Information</th>
							</tr>
							<tr>
								<td width=115>
									Amount</td>
								<td colspan="1">
									<Input Name="Amount" CssClass="inputd" size="20">
                                    </td>
								<td colspan="1">
									&nbsp;</td>
								<td colspan="1">
									&nbsp;</td>
							</tr>
							<tr>
								<td width=115>
									OrderDescription</td>
								<td colspan="3">
									<Input Name="OrderDescription" CssClass="inputd" Width="350px" size="20">
                                    </td>
							</tr>
							<tr>
								<td width=115>
									Tax</td>
								<td colspan="1">
									<Input Name="Tax" CssClass="inputd" size="20">
                                    </td>
								<td colspan="1">
									Shipping</td>
								<td colspan="1">
									<Input Name="Shipping" CssClass="inputd" size="20">
                                    </td>
							</tr>
							<tr>
								<td width=115>
									OrderID</td>
								<td colspan="1">
									<Input Name="OrderID" CssClass="inputd" size="20">
                                    </td>
								<td colspan="1">
									PONumber</td>
								<td colspan="1">
									<Input Name="PONumber" CssClass="inputd" size="20">
                                    </td>
							</tr>
							<tr>
								<td colspan="4">
									&nbsp;</td>
							</tr>
							<tr>
								<th colspan="4">
									Billing Information</th>
							</tr>
							<tr>
								<td width=115>
									FirstName</td>
								<td colspan="1">
									<Input Name="FirstName" CssClass="inputd" size="20">
                                    </td>
								<td colspan="1">
									LastName</td>
								<td colspan="1">
									<Input Name="LastName" CssClass="inputd" size="20">
                                    </td>
							</tr>
							<tr>
								<td width=115>
									Company</td>
								<td colspan="3">
									<Input Name="Company" CssClass="inputd" Width="350px" size="20">
                                    </td>
							</tr>
							<tr>
								<td width=115>
									Address1</td>
								<td colspan="3">
									<Input Name="Address1" CssClass="inputd" Width="350px" size="20">
                                    </td>
							</tr>
							<tr>
								<td width=115>
									Address2</td>
								<td colspan="3">
									<Input Name="Address2" CssClass="inputd" Width="350px" size="20">
                                    </td>
							</tr>
							<tr>
								<td width=115>
									City</td>
								<td colspan="1">
									<Input Name="City" CssClass="inputd" size="20">
                                    </td>
								<td colspan="1">
									State</td>
								<td colspan="1">
									<Input Name="State" CssClass="inputd" size="20">
                                    </td>
							</tr>
							<tr>
								<td width=115>
									Zip</td>
								<td colspan="1">
									<Input Name="Zip" CssClass="inputd" size="20">
                                    </td>
								<td colspan="1">
									Country</td>
								<td colspan="1">
									<Input Name="Country" CssClass="inputd" size="20">
                                    </td>
							</tr>
							<tr>
								<td width=115>
									Phone</td>
								<td colspan="1">
									<Input Name="Phone" CssClass="inputd" size="20">
                                    </td>
								<td colspan="1">
									Fax</td>
								<td colspan="1">
									<Input Name="Fax" CssClass="inputd" size="20">
                                    </td>
							</tr>
							<tr>
								<td width=115>
									EMail</td>
								<td colspan="1">
									<Input Name="EMail" CssClass="inputd" size="20">
                                    </td>
								<td colspan="1">
									Website</td>
								<td colspan="1">
									<Input Name="Website" CssClass="inputd" size="20">
                                    </td>
							</tr>
							<tr>
								<td colspan="4">
									&nbsp;</td>
							</tr>
							<tr>
								<th colspan="4">
									Shipping Information</th>
							</tr>
							<tr>
								<td width=115>
									FirstName</td>
								<td colspan="1">
									<Input Name="ShippingFirstName" CssClass="inputd" size="20">
                                    </td>
								<td colspan="1">
									LastName</td>
								<td colspan="1">
									<Input Name="ShippingLastName" CssClass="inputd" size="20">
                                    </td>
							</tr>
							<tr>
								<td width=115>
									Company</td>
								<td colspan="3">
									<Input Name="ShippingCompany" CssClass="inputd" Width="350px" size="20">
                                    </td>
							</tr>
							<tr>
								<td width=115>
									Address1</td>
								<td colspan="3">
									<Input Name="ShippingAddress1" CssClass="inputd" Width="350px" size="20">
                                    </td>
							</tr>
							<tr>
								<td width=115>
									Address2</td>
								<td colspan="3">
									<Input Name="ShippingAddress2" CssClass="inputd" Width="350px" size="20">
                                    </td>
							</tr>
							<tr>
								<td width=115>
									City</td>
								<td colspan="1">
									<Input Name="ShippingCity" CssClass="inputd" size="20">
                                    </td>
								<td colspan="1">
									State</td>
								<td colspan="1">
									<Input Name="ShippingState" CssClass="inputd" size="20">
                                    </td>
							</tr>
							<tr>
								<td width=115>
									Zip</td>
								<td colspan="1">
									<Input Name="ShippingZip" CssClass="inputd" size="20">
                                    </td>
								<td colspan="1">
									Country</td>
								<td colspan="1">
									<Input Name="ShippingCountry" CssClass="inputd" size="20">
                                    </td>
							</tr>
							<tr>
								<td width=115>
									Email</td>
								<td colspan="1">
									<Input Name="ShippingEmail" CssClass="inputd" size="20">
                                    </td>
								<td colspan="1">
									&nbsp;</td>
								<td colspan="1">
									&nbsp;</td>
							</tr>
							<tr>
								<td width=115>
									Tracking_Number</td>
								<td colspan="1">
									<Input Name="Tracking_Number" CssClass="inputd" size="20">
                                    </td>
								<td colspan="1">
									Shipping_Carrier</td>
								<td colspan="1">
									<Input Name="Shipping_Carrier" CssClass="inputd" size="20">
                                    </td>
							</tr>
							<tr>
								<td colspan="4">
								</td>
							</tr>
							<tr>
								<th colspan="4">
									Optional Features</th>
							</tr>
							<tr>
								<td width=115>
									ProcessorID</td>
								<td colspan="1">
									<Input Name="ProcessorID" CssClass="inputd" size="20">
                                    </td>
								<td colspan="2">
									&nbsp;&nbsp;
								</td>
							</tr>
							<tr>
								<td width=115>
									Descriptor</td>
								<td colspan="1">
									<Input Name="Descriptor" CssClass="inputd" size="20">
                                    </td>
								<td colspan="1">
									DescriptorPhone</td>
								<td colspan="1">
									<Input Name="DescriptorPhone" CssClass="inputd" size="20">
                                    </td>
							</tr>
							<tr>
								<td width=115>
									Merchant_Defined_Field_1</td>
								<td colspan="1">
									<Input Name="Merchant_Defined_Field_1" CssClass="inputd" size="20">
                                    </td>
								<td colspan="1">
									Merchant_Defined_Field_2</td>
								<td colspan="1">
									<Input Name="Merchant_Defined_Field_2" CssClass="inputd" size="20">
                                    </td>
							</tr>
							<tr>
								<td width=115>
									Customer_Vault_Action</td>
								<td colspan="1">
									<select CssClass="inputd" name="Customer_Vault_Action">
										<option></option>
										<option>add_customer</option>
										<option>update_customer</option>
										<option>delete_customer</option>
									</select ></td>
								<td colspan="1">
									Customer_Vault_ID</td>
								<td colspan="1">
									<Input Name="Customer_Vault_ID" CssClass="inputd" size="20">
                                    </td>
							</tr>
							<tr>
								<td width=115>
									Payment_Plan_SKU</td>
								<td colspan="1">
									<Input Name="Payment_Plan_SKU" CssClass="inputd" size="20">
                                    </td>
								<td colspan="2">
									&nbsp;</td>
							</tr></TBODY>
						</table>
						<p></p>
						<table cellspacing="0" cellpadding="0" border="0" align="center">
							<tr>
								<td>
								    <input type="image" border="0" src="images/buttons/button_continue.gif" name="Submit"></td>
							</tr>
						</table>
					</td>
					<td class="mb_right" width=20>&nbsp;</td>
				</tr>
				<tr>
					<td class="mb_bottomleft" width=20>&nbsp;</td>
					<td class="mb_bottom">&nbsp;</td>
					<td class="mb_bottomright" width=20>&nbsp;</td>
				</tr>
			</table>
			<!--  END CONTENT -->
			<table border="0" cellspacing="0" cellpadding="0" class="main_box" align="left">
				<tr>
					<td class="mb_topleft"><img src="images/spacer.gif" width="20" height="20" alt=""></td>
					<td class="mb_top">&nbsp;</td>
					<td class="mb_topright"><img src="images/spacer.gif" width="20" height="20" alt=""></td>
				</tr>
				<tr>
					<td class="mb_left">&nbsp;</td>
					<td class="mb_copyright">Copyright �2007 NELiX TransaX. All rights reserved.&nbsp;<a href="#">Privacy 
							Policy</a>&nbsp;|&nbsp;<a href="#">Terms of Service</a><br>
						NELiX TransaX, LLC is a registered ISO/MSP of BancorpSouth Bank of Tulepo, MS.</td>
					<td class="mb_right">&nbsp;</td>
				</tr>
				<tr>
					<td class="mb_bottomleft">&nbsp;</td>
					<td class="mb_bottom">&nbsp;</td>
					<td class="mb_bottomright">&nbsp;</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>