<?php

ini_set('display_errors', '1'); 
//load nusoap code
require_once('nusoap.php'); 
require_once('../../../../../wp-load.php');
//create client

/**
 * [getTransactionId]
 * @param  [type] $params1 
 * @return [type] 
 */
function getTransactionId($params1){
     // Get TransactionId 
     $d=strtotime("-1 Day");
     $current_dt=date('Y-m-d H:i:s',$d);
     $searchStartDate = date('m/d/Y', strtotime($current_dt));
     $postData = [
          'transactionAPIParams'  => [
               'TransactionAPIKey' => TRANSACTIONAPIKEY,
               'GatewayUserName'   => GATEWAYUSERNAME,
               'GatewayPassword'   => GATEWAYPASSWORD,
               'AccountNumber'     => ACCOUNTNUMBER,
               'StartDate'         => $searchStartDate,
               'Email'             => $params1['EMail'],
               'CreditCardLast4'   => substr($params1['CCNumber'], -4),
          ]
     ];
     $postData = json_encode($postData);
     $ch = curl_init();
     curl_setopt($ch, CURLOPT_URL,"https://secure.gwintegration.com/WebAPI/RoXWeb.aspx/TransactionAPI");
     curl_setopt($ch, CURLOPT_POST, 1);
     curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
     curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
     $server_output = curl_exec($ch);
     curl_close ($ch);
     $server_output =  json_decode($server_output);
     $server_output->d = array_reverse($server_output->d);
     $transactionIdCount = count($server_output->d);
     return $transactionId = $server_output->d[$transactionIdCount-1];
     
}

$plan_id = $_POST['plan_id'];
$amount =str_replace("$","",$_POST['amount']);
$initial_amount =str_replace("$","",$_POST['initial_amount']);

if(wp_verify_nonce($_POST['transax_nonce'], 'transax_nonce')){

     try 
     {           
          $soapclient1 = new nusoap_client('https://secure.transaxgateway.com/roxapi/recurring.asmx?WSDL','wsdl');
          $params1['GatewayUserName'] = GATEWAYUSERNAME;
          $params1['GatewayPassword'] = GATEWAYPASSWORD;
          $params1['RecurringID'] = "";
          $params1['MerchantID'] = MERCHANTID;
          $params1['InitialAmount'] = $initial_amount;
          $params1['RecurringAmount'] = $amount;

          $params1['StartDate'] = date("Y/m/d");
          $params1['Frequency'] = "months";
          $params1['Quantity'] = "1";

          $params1['NumberOfRetries'] = "2";
          $params1['PaymentType'] = $_POST['PaymentType'];
          $params1['CCNumber'] = str_replace(' ', '', $_POST['card_number']);

          $expiry_month = $_POST['card_expiry_month'];
          $expiry_year = $_POST['card_expiry_year'];
          $expiry_year =  substr($expiry_year, -2); 
          $params1['CCExpDate'] = $expiry_month.$expiry_year;
          $params1['CVV'] = $_POST['card_cvc'];
          $params1['TransactionType'] = $_POST['TransactionType'];        

          $params1['EMail'] = $_POST['email'];
          $params1['FirstName'] = $_POST['fname'];
          $params1['LastName'] = $_POST['lname'];
          $params1['Address1'] = $_POST['billing_address']; //Los Angeles
          $params1['City'] = $_POST['billing_city']; //Los Angeles
          $params1['State'] = $_POST['billing_state']; //california
          $params1['Zip'] = $_POST['billing_zip'];
          $proxy1 = $soapclient1->getProxy(); 
          $result1 = $proxy1->RecurringAdd(Array('vRecurringParams' => $params1));
          $response = $result1['RecurringAddResult'];
          $recurringid = $response['RecurringID'];

         

          // $soapclientgetTrans = new nusoap_client('https://secure.transaxgateway.com/roxapi/recurring.asmx?op='.$recurringid,'wsdl');
          // $soapclientgetTransProxy = $soapclientgetTrans->getProxy(); 

          // $getTransclientdetail = $soapclientgetTransProxy->ProcessTransaction(Array('objparameters' => $params2));

          // print_r($getTransclientdetail);
          // exit;
          if($response['RESULT'] == '1' && $response['MSG'] == 'Success' && (!empty($response['RecurringID'])))
          { 
               $redirect = add_query_arg('payment', 'paid', $_POST['redirect']);
               if($_POST['user_id'] != "") {
                    $user_id = $_POST['user_id'];
               } else {

                    $user_id = wp_create_user( $_POST['username'], $_POST['password'], $_POST['email'] );
                    $user = new WP_User($user_id);
                    $user->set_role('subscriber');
                    $to = $_POST['email'];
                    $subject = 'Greetings!..1234investors.com';
                    $headers = "MIME-Version: 1.0" . "\r\n";   
                    $headers .= "Content-Type: text/html; charset=UTF-8";
                    $headers .= 'From:  1234ivestors.com <ds.sparkle018@gmail.com>' . "\r\n"; 
                    $headers .= "Reply-To: 1234ivestors.com <ds.sparkle018@gmail.com>" . "\r\n"; 
                    $body = '<html><body><span>Hello,'.$_POST['fname'].' '.$_POST['lname'].', </span><br><br>';
                    $body .= '<span>Thanks for registering with us..</span><br><br>';
                    $body .= '<span>We received the payment and your subscription is started. Keep in touch.</span><br><br>';
                    $body .= '<span>Thank you!</span>';

                    $body .= '</body></html>';
                    $mailResult = wp_mail($to, $subject, $body, $headers);
               }
               update_user_meta($user_id, 'first_name', $_POST['fname']);
               update_user_meta($user_id, 'last_name', $_POST['lname']);
               update_user_meta($user_id, 'phone', $_POST['phone']);
               update_user_meta($user_id, 'transax_response', $response);
               //update_user_meta($user_id, 'stripe_customer_id', $stripe_customer_id);
               update_user_meta($user_id, 'transax_recurring_id', $recurringid);
               update_user_meta($user_id, 'plan_name', $plan_id);
               $d=strtotime("now");
               $current_dt=date('Y-m-d H:i:s',$d);
               $d2=strtotime("+1000 Years ");
               $end_dt = date('Y-m-d H:i:s',$d2);
               update_user_meta($user_id, 'current_period_start', $current_dt);
               update_user_meta($user_id, 'current_period_end',$end_dt);
               update_user_meta($user_id,'address',$_POST['billing_address']);
               update_user_meta($user_id,'city',$_POST['billing_city']);
               update_user_meta($user_id,'state',$_POST['billing_state']);
               update_user_meta($user_id,'zip_code',$_POST['billing_zip']);


               $server_output = getTransactionId($params1);


               $last4DigitCartNumber = substr($_POST['card_number'], -4);
            
               $curr_state=[
                    'current_period_start'   =>$current_dt,
                    'current_period_end'     =>$end_dt,
                    'plan_id'                =>$plan_id,
                    'transax_recurring_id'   =>$recurringid,
                    'transactionId'          =>$server_output->responsetransid,
                    'cartNumber'             =>$last4DigitCartNumber, 
                    'amount'                 =>$amount,
                    'action'                 =>'registered',
                    'state'                  =>'new'
               ];

               $history=[];
               array_push($history, $curr_state);
               update_user_meta($user_id,'subscribe_history',$history);
               update_user_meta($user_id,'transactionId',$server_output->responsetransid);
               update_user_meta($user_id,'amount',$initial_amount);
               update_user_meta($user_id,'cartNumber',$last4DigitCartNumber);

          }else{
               $redirect = add_query_arg('payment', 'failed', $_POST['redirect']);
          }

     }
     catch(Exception $e){
          $redirect = add_query_arg('payment', 'failed', $_POST['redirect']);

     }
     wp_redirect($redirect); exit;
}