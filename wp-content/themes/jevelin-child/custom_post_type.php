<?php 

add_action( 'init', 'fnCreateCustomPostType' );
function fnCreateCustomPostType()
{
	// 
	register_post_type( 'example-trade', array(
		'labels'        => array(
			'name'               => _x( 'All Example Trade', 'post type general name', 'atlas' ),
			'singular_name'      => _x( 'Example Trade', 'post type singular name', 'atlas' ),
			'add_new'            => __( 'Add Example Trade', 'atlas' ),
			'add_new_item'       => __( 'Add New Example Trade', 'atlas' ),
			'edit_item'          => __( 'Edit Example Trade', 'atlas' ),
			'new_item'           => __( 'New Example Trade', 'atlas' ),
			'all_items'          => __( 'All Example Trade', 'atlas' ),
			'view_item'          => __( 'View Example Trade', 'atlas' ),
			'search_items'       => __( 'Search Example Trade', 'atlas' ),
			'not_found'          => __( 'No Example Trade found', 'atlas' ),
			'not_found_in_trash' => __( 'No Example Trade found in the Trash', 'atlas' ), 
			'parent_item_colon'  => '',
			'menu_name'          => 'Example Trade'
		 ),
			'description'   => 'Example Trade',
			'public'        => true,
			'menu_position' => 8,
			'supports'      => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
			'has_archive'   => true,
			'menu_icon'		=> 'dashicons-slides',
			'hierarchical' => false,
	   ) 
    );
	
	

	
}