<?php
/**
 * Post format - Standard
 */

	if( !is_single() ) :
		$show_link = 1;
	else :
		$show_link = 0;
	endif;
?>

	<article id="post-<?php the_ID(); ?>" <?php post_class('post-item post-item-single'); ?>>
		<div class="post-container">
		<div class="example-trade-inner">	
			<?php jevelin_popover( jevelin_post_option( get_the_ID(), 'post-popover' ) ); ?>

			<?php if( jevelin_post_option( get_the_ID(), 'hide-image', false ) == false ) : ?>
				<div class="post-meta-thumb example-trade-img">
					<?php //echo the_post_thumbnail( 'full' ); ?>
					<?php echo jevelin_blog_overlay( jevelin_get_thumb( get_the_ID() ), $show_link, 1 ); ?>
				</div>
			<?php endif; ?>

			<div class="example-trade-desc">
				
					<?php if( !is_single() ) : ?>
						<h1 class="example-trade-title"><a href="<?php echo esc_url( get_permalink() ); ?>" class="post-title"><?php the_title(); ?> </a></h1>
					<?php else : ?>
						<h1><a href="<?php echo esc_url( get_permalink() ); ?>" class="post-title"><?php the_title(); ?>
						</a></h1>
					<?php endif; ?>
				

				<!-- <div class="post-meta-data sh-columns">
					<div class="post-meta post-meta-one">
						<?php jevelin_meta_one(); ?>
					</div>
					<div class="post-meta post-meta-two">
						<?php jevelin_meta_two(); ?>
					</div>
				</div> -->

				<div class="post-content example-trade-content">
					<?php
						// if( !is_single() ) :
						// 	the_excerpt();
						// else :
							the_content();
						// endif;
					?>
					<!-- <a class="read-more" href="<?php echo get_permalink(); ?>">Continue Reading...</a> -->
				</div>
			</div>
		</div>
		</div>
	</article>