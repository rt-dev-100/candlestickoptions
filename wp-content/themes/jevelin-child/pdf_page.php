<?php
/*
Template Name: PDF PAGE
*/
 
if(is_page('pl')){
	if(!is_user_logged_in()){
		$url = home_url();
		wp_redirect($url);
		exit();
	}
}
if( jevelin_post_option( get_the_ID(), 'page_layout' ) == 'sidebar-right' || jevelin_post_option( get_the_ID(), 'page_layout' ) == 'sidebar-left' ) :
	$layout_sidebar = esc_attr( jevelin_post_option( get_the_ID(), 'page_layout' ) );
endif;

$class = '';
if( function_exists('fw_ext_page_builder_is_builder_post') && !fw_ext_page_builder_is_builder_post( get_queried_object_id() ) ) {
	$class = ' page-default-content';
}
 
get_header(); ?>
     

	<div id="content" class="abcd page-content <?php if( isset($layout_sidebar) && $layout_sidebar ) : ?>content-with-<?php echo esc_attr( $layout_sidebar ); endif; ?><?php echo esc_attr( $class ); ?>">

		<?php
			/*while ( have_posts() ) : the_post();
				the_content();
			endwhile;*/
			if(is_page('pl')){
				$url = 'http://gsx2json.com/api?id=1UOY7JqKxxMxU0jdN3uKj7lc0rko2d7ZI1ZnpA3eogis&sheet=1';
			}
			else if(is_page('performance')){
				$url = 'http://gsx2json.com/api?id=1HZPhHmjR6IZTiRhoxUAPyWIeTzmpdQLVF7Vwyoq96qQ&sheet=1';
			}
			

			    $ch=curl_init($url);
			   // $data_string = json_encode($fields);
			   
			    
			    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			   // curl_setopt($ch, CURLOPT_POSTFIELDS, array('params'=>$data_string));
			 
			    
			    curl_setopt($ch, CURLOPT_HEADER, false);
			    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
			    $response = curl_exec($ch);
			    $status = curl_getinfo($ch);
			    //$result = curl_exec($ch);
			    curl_close($ch);

			   
			    $json_res = json_decode($response);
			    $header_row = $json_res->rows[0];
    
				include('mpdf/mpdf.php');

		    	$html = '';
		    	if(is_page('pl')){
                    $html .= '<h1 class="heading_title">Performance</h1>';
		    	}
		    	else if(is_page('performance')){
		    		$html .= '<h1 class="heading_title">Sample Performance</h1>';
		    	}

		    	
			    $html .= '<div class="container">';
			    
			    $html .= '<table id="customers">';
			    $html .= '<thead>';
			    $html .=  '<tr>';

			    foreach($header_row as $key => $hr){
			    	if (substr($key, 0, 1) === '_') { 
		               $html .= '<th>'.' '.'</th>'; 
			    	 }
			    	 else{
			    	 	$header_arr = array('completedtrade' => 'Completed Trade' , 'symbol' => 'Symbol' , 'totalcost' => 'Total Cost' , 'rateofreturn' => 'Rate of Return' , 'grossprofitloss' => 'Gross Profilt Loss' ,'actualtrade' => 'Actual Trade' , 'completedtradeid' => 'Completed Trade Id','expirationdate'=>'Expiration Date' , 'daysleft' => 'Days Left','netdebit' => 'Net Debit','costcontract' => 'Cost/Contract','pnlcontract'=>'PNL/Contract' ,'grosspnl' => 'Gross PNL','roi'=>'ROI','pnl'=>'PNL');
			    	 	if(array_key_exists($key,$header_arr)){
			    	 		$html .= '<th>'.ucfirst($header_arr[$key]).'</th>';
			    	 	}
			    	 	else{
			    	 		$html .= '<th>'.ucfirst($key).'</th>';
			    	 	}
			    	 	
			    	 }
				       
			    }
	    
			    $html .= '</tr>';
			    $html .= '</thead>';
			    $html .= '<tbody>';
			    foreach($json_res->rows as $key => $row){ 
			      $html .= '<tr>';
			        foreach ($row as $key => $value) {
			           $html .= '<td>'.$value.'</td>';  
			        }

			         $html .= '</tr>';
			     
			    }
			    $html .= '</tbody></table></div>';
			     

		     
			    // create an object of the class mpdf
			    $mpdf=new mPDF('c', 'A4-L', '', '', 16, 16, 16, 16, 9, 9, 'L'); 
			     
			    // write the html to the file

			    //$mpdf->SetHTMLHeader('<h1 class="heading_title">Treading Sheet</h1>');
			    $mpdf->SetHTMLFooter('<h6 class="footer_text">1234investors.com</h6>');
			    $mpdf->SetDisplayMode(110,'single');
			    $stylesheet = file_get_contents('./wp-content/themes/jevelin-child/css/pdf.css');
			    $mpdf->WriteHTML($stylesheet,1);
				$mpdf->WriteHTML($html,2);
			    //$mpdf->WriteHTML($html);
			     
			    // generate the output
			    //$mpdf->Output();
			    if(is_page('pl')){
					$mpdf->Output('./wp-content/themes/jevelin-child/subscribers.pdf', 'F' ); ?>
					<div class="content" style='text-align: center;'>
						<iframe seamless class="non_subscriberframe"  src="<?php echo site_url(); ?>/wp-content/themes/jevelin-child/subscribers.pdf" height="800px" width="70%"></iframe>
					</div>	
				<?php }
				else if(is_page('performance')){
					$mpdf->Output('./wp-content/themes/jevelin-child/non-subscribers.pdf', 'F' ); ?>
					<div class="content" style='text-align: center;'>
						<iframe  seamless class="non_subscriberframe"  src="<?php echo site_url(); ?>/wp-content/themes/jevelin-child/non-subscribers.pdf" height="800px" width="70%"></iframe>
					</div>	
					
				<?php }
		    	 ?>
		         <style type="text/css">
						
						.non_subscriberframe{
							display: initial !important;

						    overflow-y: scroll !important;
						}
						.content {
						    position: relative;
						    overflow: hidden;
						}
				</style>

				<?php /* Clear unclosed floats */ ?>

				<div class="sh-clear"></div>

				<?php 
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

					if ( is_singular() ) :
						wp_enqueue_script( 'comment-reply' );
					endif;
				?>

	</div>

	<?php if( isset($layout_sidebar) && $layout_sidebar ) : ?>
		<div id="sidebar" class="<?php echo esc_attr( $layout_sidebar ); ?>">
			<?php get_sidebar(); ?>
		</div>
	<?php endif; ?>


<?php get_footer(); ?>

