<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<p><?php
	/* translators: 1: user display name 2: logout url */
	/*printf(
		__( 'Hello %1$s (not %1$s? <a href="%2$s">Log out</a>)', 'woocommerce' ),
		'<strong>' . esc_html( $current_user->display_name ) . '</strong>',
		esc_url( wc_logout_url( wc_get_page_permalink( 'myaccount' ) ) )
	);*/
?></p>

<p><?php
	/*printf(
		__( 'From your account dashboard you can view your <a href="%1$s">recent orders</a>, manage your <a href="%2$s">shipping and billing addresses</a> and <a href="%3$s">edit your password and account details</a>.', 'woocommerce' ),
		esc_url( wc_get_endpoint_url( 'orders' ) ),
		esc_url( wc_get_endpoint_url( 'edit-address' ) ),
		esc_url( wc_get_endpoint_url( 'edit-account' ) )
	);*/
?></p>

<?php
	/**
	 * My Account dashboard.
	 *
	 * @since 2.6.0
	 */
	//do_action( 'woocommerce_account_dashboard' );

	/**
	 * Deprecated woocommerce_before_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	//do_action( 'woocommerce_before_my_account' );

	/**
	 * Deprecated woocommerce_after_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	//do_action( 'woocommerce_after_my_account' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */

if(is_user_logged_in()){

	$current_user_id = get_current_user_id();
	//echo $current_user_id;
	$user_meta = get_user_meta($current_user_id);
	
	$plan_name= $user_meta['plan_name'];
	$current_period_start = $user_meta['current_period_start'];
	$current_period_end = $user_meta['current_period_end'];
	$name = $user_meta['first_name'];
    $start_date = date('d F, Y',$current_period_start[0]);
   
    $end_date = date('d F Y',$current_period_end[0]);
    $subscription_id_data = $user_meta['stripe_subscription_id'];
    $subscription_id=$subscription_id_data[0];
    //$subscription_id ='';
	echo '<legend id="plan_header">Hi..'.ucfirst($name[0]).' Your Subscription Plan detail is here.</legend>';
	//echo '<p>Your current subscribed plan is '.$plan_name[0].'</p>';
	//echo '<p>Your plan validity '.$start_date. ' to '.$end_date.'</p>';
	if($plan_name[0] == 'per month'){
		$fee = '$199';
		$plan_name[0] = '$199 per month';
	}
	else{
		$fee = '$8';
		$plan_name[0] = '$8 of one month';
	}
	
	if(($subscription_id != '')&&($plan_name[0] == '$199 per month')){
		$memebership_status = 'Active';
	}
	else if(($plan_name[0] == '$8 of one month')){
		$memebership_status = 'Active';
	}
	else{
	   $memebership_status = 'InActive';	
	}
?>  
<span id="cancel_memebership_stripe_status"></span>  
<table>
	<th colspan="2">
		Plan Details
	</th>
	
	<tr>
		<td style="width: 25%;">Current Plan </td>
        <td style="width: 100%;"> <?php echo $plan_name[0]; ?> </td> 
	</tr>
	<tr>
		<td>Plan Fees </td>
		<td><?php echo $fee; ?></td>
	</tr>
	<tr>
		<td>Membership Start Date</td>
		<td><?php echo $start_date; ?></td>
	</tr>
	<tr>
		<td>Membership Status</td>
		<td><?php echo $memebership_status; ?></td>
	</tr>

</table>
<div class="row" style="padding-top: 10px;">

  <?php
    if(($memebership_status == 'Active')&& ($plan_name[0] == '$199 per month')){
    ?>	
       <legend style="margin-left: 15px;">You can cancel your subscription for next billing cycle and renew it whenever you want.</legend>
       <button type="button" name="cancel_membership" class="btn btn-primary" style="margin-left: 20px;" id="cancel_member" >Cancel Membership</button>
   <?php }
   ?>	
  	
</div>
<style type="text/css">
	#plan_header{
		padding-top: 0px;
	}
	table th{
		background-color: #008125!important;
        color: #fff !important;
	}
</style>


<?php

}