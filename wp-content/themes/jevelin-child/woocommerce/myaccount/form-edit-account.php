<?php
/**
 * Edit account form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_edit_account_form' ); ?>

<form class="woocommerce-EditAccountForm edit-account" action="" method="post">

	<?php do_action( 'woocommerce_edit_account_form_start' );
	
	$notify_for_new_question = get_user_meta( $user->ID, 'notify_for_new_question', true );
	
	$notify_for_my_question = get_user_meta( $user->ID, 'notify_for_my_question', true );

	$notify_for_summary_question = get_user_meta( $user->ID, 'notify_for_summary_question', true );

	$new_answer_notify_follower = get_user_meta( $user->ID, 'new_answer_notify_follower', true );

	$do_not_sent_any_notifications = get_user_meta( $user->ID, 'do_not_sent_any_notifications', true );
    
    $phone_number = get_user_meta($user->ID,'phone',true);
    
	$check = 'checked="checked"';
	$check2 = 'checked="checked"';
	$check3 = 'checked="checked"';
	$check4 = 'checked="checked"';
	$check5 = 'checked="checked"';

	if( empty($notify_for_new_question) || ($notify_for_new_question=="No")){
			$check = '';
			$check_no = 'checked="checked"';
		}
	if( empty($notify_for_my_question) || ($notify_for_my_question=="No")){
			$check2 = '';
			$check2_no = 'checked="checked"';
		}
	if( empty($notify_for_summary_question) || ($notify_for_summary_question=="No")){
			$check3 = '';
			$check3_no = 'checked="checked"';
		} 
	if( empty($new_answer_notify_follower) || ($new_answer_notify_follower=="No")){
			$check4 = '';
			$check4_no = 'checked="checked"';
		} 
	if( empty($do_not_sent_any_notifications) || ($do_not_sent_any_notifications=="No")){
			$check5 = '';
			$check5_no = 'checked="checked"';
		}
	?>
	
	

	<p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
		<label for="account_first_name"><?php _e( 'First name', 'woocommerce' ); ?> <span class="required">*</span></label>
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_first_name" id="account_first_name" value="<?php echo esc_attr( $user->first_name ); ?>" />
	</p>
	<p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
		<label for="account_last_name"><?php _e( 'Last name', 'woocommerce' ); ?> <span class="required">*</span></label>
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_last_name" id="account_last_name" value="<?php echo esc_attr( $user->last_name ); ?>" />
	</p>
	<div class="clear"></div>
    <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
		<label for="account_phone_number"><?php _e( 'Phone Number', 'woocommerce' ); ?> <span class="required">*</span></label>
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_phone_number" id="account_phone_number" value="<?php echo esc_attr( $phone_number ); ?>" />
	</p>
	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="account_email"><?php _e( 'Email address', 'woocommerce' ); ?> <span class="required">*</span></label>
		<input type="email" class="woocommerce-Input woocommerce-Input--email input-text" name="account_email" id="account_email" value="<?php echo esc_attr( $user->user_email ); ?>" />
	</p>
     
	<fieldset>
		<legend><?php _e( 'Password change', 'woocommerce' ); ?></legend>

		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="password_current"><?php _e( 'Current password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>
			<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_current" id="password_current" />
		</p>
		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="password_1"><?php _e( 'New password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>
			<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_1" id="password_1" />
		</p>
		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="password_2"><?php _e( 'Confirm new password', 'woocommerce' ); ?></label>
			<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_2" id="password_2" />
		</p>

		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide" style="display: none;">
			<label for="account_last_name"><?php _e( 'Notify Me When A New Question Is Asked', 'woocommerce' ); ?> <span class="required">*</span></label>
			<input type="radio" name="notify_for_new_question" value="Yes" <?php echo $check; ?>>Yes
			<input type="radio" name="notify_for_new_question" value="No" <?php echo $check_no; ?>> No
		</p>
		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide" style="display: none;">
			<label for="account_last_name1"><?php _e( 'Notify Me When Someone Answers My Question', 'woocommerce' ); ?> <span class="required">*</span></label>
			<input type="radio" name="notify_for_my_question" value="Yes" <?php echo $check2; ?>>Yes
			<input type="radio" name="notify_for_my_question" value="No" <?php echo $check2_no; ?>> No
		</p>
		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide" style="display: none;">
			<label for="account_last_name1"><?php _e( 'Notify me when someone answers a question I am following', 'woocommerce' ); ?> <span class="required">*</span></label>
			<input type="radio" name="new_answer_notify_follower" value="Yes" <?php echo $check4; ?>>Yes
			<input type="radio" name="new_answer_notify_follower" value="No" <?php echo $check4_no; ?>> No
		</p>
		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide" style="display: none;">
			<label for="account_last_name2"><?php _e( 'Send me a daily summary of all questions asked', 'woocommerce' ); ?> <span class="required">*</span></label>
			<input type="radio" name="notify_for_summary_question" value="Yes" <?php echo $check3; ?>>Yes
			<input type="radio" name="notify_for_summary_question" value="No" <?php echo $check3_no; ?>> No
		</p>
		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide" style="display: none;">
			<label for="account_last_name3"><?php _e( 'I do not want to be sent any notifications', 'woocommerce' ); ?> <span class="required">*</span></label>
			<input type="radio" name="do_not_sent_any_notifications" value="Yes" <?php echo $check5; ?>>Yes
			<input type="radio" name="do_not_sent_any_notifications" value="No" <?php echo $check5_no; ?>> No
		</p>
	</fieldset>
	<div class="clear"></div>



	<?php do_action( 'woocommerce_edit_account_form' ); ?>

	<p>
		<?php wp_nonce_field( 'save_account_details' ); ?>
		<input type="submit" class="woocommerce-Button button" name="save_account_details" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>" />
		<input type="hidden" name="action" value="save_account_details" />
	</p>

	

	<?php do_action( 'woocommerce_edit_account_form_end' ); ?>
</form>

<?php do_action( 'woocommerce_after_edit_account_form' ); ?>
