<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<?php 
if(isset($_GET['payment']) && $_GET['payment'] == 'paid') { ?> 
<div class="alert alert-success"><strong>Success..!</strong>You have successfully subscribed to 1234investors site.
You will get SMS related to trading information soon.</div>
<?php  } ?> 
<?php
	/**
	 * My Account dashboard.
	 *
	 * @since 2.6.0
	 */
	//do_action( 'woocommerce_account_dashboard' );

	/**
	 * Deprecated woocommerce_before_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	//do_action( 'woocommerce_before_my_account' );

	/**
	 * Deprecated woocommerce_after_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	//do_action( 'woocommerce_after_my_account' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */

if(is_user_logged_in()){

	$current_user_id = get_current_user_id();
	// echo $current_user_id;
	$user_meta = get_user_meta($current_user_id);
	
	$plan_name= $user_meta['plan_name'];
	$current_period_start = $user_meta['current_period_start'];
	$current_period_end = $user_meta['current_period_end'];
	$name = $user_meta['first_name'];
    $start_date = date('d F, Y',strtotime($current_period_start[0]));
   
    $end_date = date('d F Y',strtotime($current_period_end[0]));
    $subscription_id_data = $user_meta['stripe_subscription_id'];
    $subscription_id=$subscription_id_data[0];
    //$subscription_id ='';
    // echo "<pre>";
	// print_r(unserialize($user_meta['subscribe_history'][0]));
    $recurring_id = $user_meta['transax_recurring_id'];
	echo '<legend id="plan_header">Hi..'.ucfirst($name[0]).' Your Subscription Plan detail is here.</legend>';
	//echo '<p>Your current subscribed plan is '.$plan_name[0].'</p>';
	//echo '<p>Your plan validity '.$start_date. ' to '.$end_date.'</p>';
	
	// echo "</pre>";
	// echo "</pre>";
	// if(($plan_name[0] == 'per month')|| ($plan_name[0] == 'subscription_plan')){
	// 	$fee = '$199';
	// 	$plan_name[0] = '$199 per month';
	// }
	// else{
	// 	$fee = '$8';
	// 	$plan_name[0] = '$8 of one month';
	// }
	$plan_id= $plan_name[0];
	if(($recurring_id[0] != '') &&  ($plan_id=='1' || $plan_id=='2' || $plan_id=='3')) {
		$memebership_status = 'Active';
	}
	// else if(($plan_name[0] == '$8 of one month')){
	// 	$memebership_status = 'Active';
	// }
	else{
	   $memebership_status = 'InActive';	
	}
	
	    switch($plan_id) {
                        case '1':
                           	$plan_name_text= "$249 of one month";
                            // $memebership_status = 'Active';
                            $fee = '$249';

                        break;
                        case '2':
                           	$plan_name_text= "$299 of one month";
                         	// $memebership_status = 'Active';
                         	$fee = '$299';

                        break;
                        case '3':
                           	$plan_name_text= "$399 of one month";
                      		// $memebership_status = 'Active';
                      		$fee = '$399';

                        break;
                        default:
                           	$plan_name_text= "No Plan selected";
                      	    // $memebership_status = 'InActive';	
                      	    $fee = '0';
                        break;
                           
                    }
?>  


<table>
	<th colspan="2">
		<div class="plantitle">Plan Details</div>
		<a class="paymentbutton" data-toggle="modal" data-target="#myModal">Payment History</a>
	</th>
	<tr>
		<td style="width: 25%;">Current Plan </td>
        <td style="width: 100%;"> <?php echo $plan_name_text; ?>   
        	<?php if ($memebership_status == 'Active') { ?> <a href="javascript://" class="" style="margin-left: 20px;" id="change_plan" ><i class="icon-note"></i> Change Plan</a>
		<?php } ?>
        </td> 
	</tr>
	<tr>
		<td>Plan Fees </td>
		<td><?php echo $fee; ?></td>
	</tr>
	<tr>
		<td>Membership Start Date</td>
		<td><?php echo $start_date; ?></td>
	</tr>
	<tr>
		<td>Membership Status</td>
		<td>
			<?php echo $memebership_status; ?> 
	  <?php
	    if(($memebership_status == 'Active') && ($plan_id=='1' || $plan_id=='2' || $plan_id=='3')){
	    ?>	
	     <!--   <legend style="margin-left: 15px;">You can cancel your subscription for next billing cycle and renew it whenever you want.</legend> -->
	       <a href="javascript://"   class="text-danger" style="margin-left: 20px;" id="cancel_member" ><i class="icon-close"></i> Cancel Membership</button>
	   <?php }else{
	   	?>
	   	<a href="javascript://"   class="text-active" style="margin-left: 20px;" id="reactive" ><i class="icon-check"></i> Re-enroll</button>
	   <?php }
	   ?>	
	</td>
	</tr>

</table>


  <?php/*
    if(($memebership_status == 'Active') && ($plan_id=='1' || $plan_id=='2' || $plan_id=='3')){
    ?>	
       <legend style="margin-left: 15px;">You can cancel your subscription for next billing cycle and renew it whenever you want.</legend>
       <button type="button" name="cancel_membership" class="btn btn-danger" style="margin-left: 20px;" id="cancel_member" >Cancel Membership</button>
   <?php } */
   ?>	
  	

<style type="text/css">
	#plan_header{
		padding-top: 0px;
	}
	table th{
		background-color: #008125!important;
        color: #fff !important;
	}
	.mrgT10{
		margin-top: 10px;
	}
</style>
<script type="text/javascript">
jQuery(document).on('click',"#change_plan,#reactive",function () {
	$('#change_plan_div').show('slow');
	 $('html,body').animate({
            scrollTop: $("#change_plan_div").offset().top-100
        }, 'slow');
	 $('#user_id').val('<?php echo $current_user_id;?>');
});
jQuery('#myModal').on('shown.bs.modal', function () {
	 jQuery('#myInput').focus()
})
</script>
<div class="row clearfix mrgT10" id="change_plan_div" style="display: none">
	
		<div class="col-md-4" >
			<div class="membership-plan active monthly" id="Plan-1">
				<div class="sh-iconbox sh-iconbox-style2 sh-iconbox-left">
					<div class="sh-iconbox-icon">
						<div class="sh-iconbox-icon-shape sh-iconbox-square sh-iconbox-hover">
																<i class="sh-iconbox-hover ti-package"></i>
							<!-- <img src="https://1234investors.com/wp-content/uploads/2017/09/free2.png" style="height: 35px"> -->
						</div>
					</div>
					<div class="sh-iconbox-aside">
						<div class="sh-iconbox-title">
							<h3>Silver</h3>
						</div>
						<div class="sh-iconbox-seperator"></div> 
						<div class="sh-iconbox-content">
							<p><span style="font-weight: 400"><b><span class="plan-price">$ 249 </span></b>per month </span></p>
						</div>
					</div>							
				</div>						
			</div>
		</div>
		<div class="col-md-4">
			<div class="membership-plan monthly " id="Plan-2">
				<div class="sh-iconbox sh-iconbox-style2 sh-iconbox-left">
					<div class="sh-iconbox-icon">
						<div class="sh-iconbox-icon-shape sh-iconbox-square">
							<i class="sh-iconbox-hover ti-package"></i>

						</div>
					</div>
					<div class="sh-iconbox-aside">
						<div class="sh-iconbox-title">
							<h3>Gold</h3>
					</div>
						<div class="sh-iconbox-seperator"></div> 
						<div class="sh-iconbox-content">
							<p><span style="font-weight: 400"><b><span class="plan-price">$299</span></b> per month</span></p>
						</div>
					</div>							
				</div>						
			</div>
		</div>
		<div class="col-md-4">
			<div class="membership-plan monthly" id="Plan-3">
				<div class="sh-iconbox sh-iconbox-style2 sh-iconbox-left">
					<div class="sh-iconbox-icon">
						<div class="sh-iconbox-icon-shape sh-iconbox-square">
							<i class="sh-iconbox-hover ti-package"></i>
							
						</div>
					</div>
					<div class="sh-iconbox-aside">
						<div class="sh-iconbox-title">
							<h3>Platinium</h3>
					</div>
						<div class="sh-iconbox-seperator"></div> 
						<div class="sh-iconbox-content">
							<p><span style="font-weight: 400"><b><span class="plan-price">$399</span></b> per month</span></p>
						</div>
					</div>							
				</div>						
			</div>
		</div>
	</div>
<?php

}
function diplaystatus($statusName){
	switch ($statusName) {
		case 'registered':
			return 'Registered';
			break;
		case 'planchange':
			return 'Plan Changed';
			break;
		case 'delete':
			return 'Deleted';
			break;
		default:
			return $statusName;
			break;
	}
}
?>
<style>
.modal-backdrop.in{
	opacity: 0;
	display: none;
}
.modal-open .modal {
	z-index: 9;
}
.subscribeHistory-table table td {
    padding: 5px 20px!important;
    font-size: 14px;
}
</style>
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog payment-history" style="width: 850px;">
      	<div class="modal-content">
        <div class="modal-header">
          	<button type="button" class="close" data-dismiss="modal">&times;</button>
          	<h4 class="modal-title text-success">Payment History</h4>
      	</div>
      	<div class="modal-body">
      		<div class="subscribeHistory-table">
          	<?php $subscribeHistory = unserialize($user_meta['subscribe_history'][0]);  ?>
      		<table>
      			<tr>
	      			<td><strong>No.</strong></td>
	      			<td style="width: 153px;"><strong>Period Date</strong></td>
	      			<td style="width: 95px;"><strong>Plan No</strong></td>
	      			<td style="width: 127px;"><strong>Recurring Id</strong></td>
	      			<td style="width: 141px;"><strong>Transaction Id</strong></td>
	      			<td ><strong>Amount</strong></td>
	      			<td><strong>Action</strong></td>
      			</tr>
      			<tbody>
      				<?php if(!empty($subscribeHistory)) {
      				$subscribeHistory = array_reverse($subscribeHistory);
      				foreach ($subscribeHistory as $key => $value) { ?>
      				<tr>
      					<td><?php echo $key+1; ?>
      					</td>
      					<td>
      						<?php echo array_key_exists('current_period_start', $value)?$value['current_period_start']!=''?date('d M, Y', strtotime($value['current_period_start'])):'-':'-'; ?>
      					</td>
      					<td><?php echo array_key_exists('plan_id', $value)?$value['plan_id']!=''?'Plan-'.$value['plan_id']:'-':'-'; ?>
      					</td>
      					<td><?php echo array_key_exists('transax_recurring_id', $value)?$value['transax_recurring_id']!=''?$value['transax_recurring_id']:'-':'-';?>
      					</td>
      					<td> <?php echo array_key_exists('transactionId', $value)?$value['transactionId']!=''?$value['transactionId']:'-':'-';?> 
      					</td>
      					<td><?php echo array_key_exists('amount', $value)?$value['amount']!=''?'$'.number_format($value['amount'], 2):'-':'-';?> 
      					</td>
      					<td><?php echo array_key_exists('action', $value)?$value['action']!=''?diplaystatus($value['action']):'-':'-';?>
      					</td>
      				</tr>
      				<?php }  
      						} else { ?>
      					<tr>
      						<td colspan="7">
      							No Records Found.
      						</td>
      					</tr>
      				<?php } ?>
      			</tbody>
      		</table>
      		</div>
      	</div>
     	</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	jQuery.ajax({
        type: "get",
        url: "<?php echo site_url(); ?>/wp-content/plugins/transaxgateway-wordpress/includes/PHP/retriveDetails.php", 
        cache: false,
        contentType: false,
        processData: false,
        success: function(data){
            console.log(data);
        }
    });
});
</script>