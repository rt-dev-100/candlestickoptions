<?php

add_shortcode('login_form', 'login_form');
function login_form() {
	$args = array(
		'redirect' => home_url()
	);
    return wp_login_form($args);
}

add_shortcode('login_form_msg', 'login_form_msg');
function login_form_msg() {
        if($_REQUEST['msg'] == 'reg'){
            echo "<p style='text-align:center'>Your account has been successfully registered!</p>";
        }
}

function get_stripe_plans() {

	global $stripe_options;
	// load the stripe libraries
	require_once(STRIPE_BASE_DIR . '/lib/Stripe.php');
	
	// check if we are using test mode
	if(isset($stripe_options['test_mode']) && $stripe_options['test_mode']) {
		$secret_key = $stripe_options['test_secret_key'];
	} else {
		$secret_key = $stripe_options['live_secret_key'];
	}

	Stripe::setApiKey($secret_key);
	// retrieve all plans from stripe
	$plans_data = Stripe_Plan::all();
	// setup a blank array
	$plans = array();
	if($plans_data) {
		foreach($plans_data['data'] as $plan) {
			// store the plan ID as the array key and the plan name as the value
			$plans[$plan['id']] = $plan['name'];			
		}
	}
	
	return $plans;
}

add_shortcode('stripe-plan-update', 'fnStripePlanUpdateForm');
function fnStripePlanUpdateForm() {
	
	//$plans = get_stripe_plans();
	$data = '';

	$data .= '<div class="remodal remodal-is-initialized remodal-is-closed" data-remodal-id="StripePlanUpdate" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
				<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
				<div>
					<h2 id="modal1Title">Subscription Plan Update</h2>
					<form action="'.get_post_type_archive_link( 'example-trade' ).'" method="POST" id="stripe-plan-update-form">
					<div class="form-row" id="stripe-plans">
						<label><?php _e("Choose Your Plan", "pippin_stripe"); ?></label>
						<select name="plan_id" id="stripe_plan_id">';												
								//if($plans) {
									//foreach($plans as $id => $plan) {
										//if($plan == 'per month'){
										//	$plan = '$199 per month';
									//	}
										$data .='<option value="' . '1' . '">' . 'subscription_plan' . '</option>';
									//}
								//}						
						$data .='</select>
					</div>
					<input type="hidden" name="action" value="stripe-plan-update"/>
					<input type="hidden" name="stripe_nonce" value="'.wp_create_nonce("stripe-nonce").'"/>
					<input type="hidden" name="redirect" value="'.get_post_type_archive_link( 'example-trade' ).'"/>
					<div class="form-row">
						<button type="submit" class="btn-submit" id="stripe-submit">Update</button>
					</div>
					</form>
				</div>
			</div>';

	return $data;
}


