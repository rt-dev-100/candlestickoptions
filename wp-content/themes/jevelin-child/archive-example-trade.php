<?php

if( jevelin_post_option( get_the_ID(), 'page_layout' ) == 'sidebar-right' || jevelin_post_option( get_the_ID(), 'page_layout' ) == 'sidebar-left' ) :
	$layout_sidebar = esc_attr( jevelin_post_option( get_the_ID(), 'page_layout' ) );
endif;

$class = '';
if( function_exists('fw_ext_page_builder_is_builder_post') && !fw_ext_page_builder_is_builder_post( get_queried_object_id() ) ) {
	$class = ' page-default-content';
}

get_header();
?>

		<div id="content" class="page-content example-trade-listing <?php if( isset($layout_sidebar) && $layout_sidebar ) : ?>content-with-<?php echo esc_attr( $layout_sidebar ); endif; ?><?php echo esc_attr( $class ); ?>">
			<?php
				$count_posts = wp_count_posts('example-trade');
				$published_posts = $count_posts->publish;
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;	
				$user_id = get_current_user_id();
				$stripe_subscription_id = get_user_meta($user_id, 'stripe_subscription_id', true);
				$subscription_end = get_user_meta($user_id, 'current_period_end', true);
				//$current_date = strtotime(date('Y/m/d h:i:s'));
                                $current_date = (date('Y/m/d h:i:s'));
                                $d=strtotime("now");
				$current_dt=date('Y-m-d H:i:s',$d);
                                
				if( isset($_GET['plan_update']) && $_GET['plan_update'] == 'true' ) {
					echo '<p class="plan-update-success">' . __('Thank you for updating your subscription plan.', 'pippin_stripe') . '</p>';
				} elseif  (isset($_GET['plan_update']) && $_GET['plan_update'] == 'false' ) {
					echo '<p class="plan-update-error">' . __('Sorry your subscription plan not updated. Please try again.', 'pippin_stripe') . '</p>';
				}

				if( is_super_admin() ) {
					$posts_per_page = get_option( 'posts_per_page' );
					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				} else if( is_user_logged_in() &&  $subscription_end >= $current_date ) {
					$posts_per_page = get_option( 'posts_per_page' );
					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;	
				} else {
					$posts_per_page = 5;
					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;	
				}
				$loop = new WP_Query( array('post_type' => 'example-trade','orderby'=>'menu_order', 'post_status' => 'publish', 'showposts'   => $posts_per_page, 'paged'=>$paged) );
				
				if ( have_posts() ) {
					while ( $loop->have_posts() ) : $loop->the_post();


							get_template_part( 'content-format-example-trades' );
							
		
					endwhile;
				} else {
					get_template_part( 'content', 'none' );
				}
                        //echo $subscription_end.' '.' '.$current_dt;        
			if( is_super_admin() ) {
				jevelin_pagination();
			}else if( is_user_logged_in() && $subscription_end >= $current_date ) {
				jevelin_pagination();
			} else if( is_user_logged_in() && !is_super_admin() && $subscription_end <= $current_dt) {
                            //echo $subscription_end.' '.$current_date;
				echo '<div class="load-more"><a data-toggle="StripePlanUpdate" data-target="#StripePlanUpdate" href="#StripePlanUpdate" class="btn-submit">Update Plan</a></div>';
				echo do_shortcode('[stripe-plan-update]');
			} else if( !is_user_logged_in() ) {
			?>
				<div class="load-more">
					<h3>And There Is Much More!</h3>				
					<a href="<?php echo site_url(); ?>/signup" class="btn-submit">Click here to Subscribe Now</a>				
				</div>
			<?php 
				
			} ?>
		</div>

	</div>
	<?php if( isset($layout_sidebar) && $layout_sidebar ) : ?>
		<div id="sidebar" class="<?php echo esc_attr( $layout_sidebar ); ?>">
			<?php get_sidebar(); ?>
		</div>
	<?php endif; ?>

<?php get_footer(); ?>


