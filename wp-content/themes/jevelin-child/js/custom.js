
jQuery( document ).ready(function() {	
	jQuery("#preview_model").click(function() {
		var post_title = jQuery('#title').val();	
		var content = jQuery('#content').val();
		jQuery(".post-title").html(post_title);
		jQuery(".post-content").html(content);

	});
	
	jQuery('[data-remodal-id=modal]').remodal();
	return jQuery('.icon').click(function () {
        jQuery(this).addClass('current').siblings('.icon').removeClass('current');
        var active = jQuery(this).attr('id');
        //jQuery('#device > .screen').removeClass('desktop').removeClass('tablet').removeClass('mobile').addClass(jQuery(this).attr('id') + ' show');
        
        jQuery('#device .screen').css('display','none');
        jQuery('#device .screen.' + active ).css('display','block');
        return jQuery('#device').removeClass('desktop').removeClass('tablet').removeClass('mobile').addClass(jQuery(this).attr('id'));
    });
	
	jQuery('[data-remodal-id=StripePlanUpdate]').remodal();
	
});