<?php

/**

 * Theme functions file

 */



/**

 * Enqueue parent theme styles first

 * Replaces previous method using @import

 * <http://codex.wordpress.org/Child_Themes>

 */



add_action( 'wp_enqueue_scripts', 'jevelin_child_enqueue', 99 );

function jevelin_child_enqueue() {
    // Stylesheet
	wp_enqueue_style( 'jevelin-child-style', get_stylesheet_directory_uri() . '/style.css' );
    wp_enqueue_style( 'prism', get_stylesheet_directory_uri() . '/css/prism.css' );
    wp_enqueue_style( 'intlTelInput', get_stylesheet_directory_uri() . '/css/intlTelInput.css' );

    // Scripts
    wp_enqueue_script( 'jevelin-child-scripts', get_stylesheet_directory_uri() . '/js/scripts.js' );
    wp_enqueue_script( 'validate-min-js', get_stylesheet_directory_uri() . '/js/jquery.validate.min.js' );
    wp_enqueue_script( 'payment', get_stylesheet_directory_uri() . '/js/payment.js' );
    wp_enqueue_script( 'intlTelInput', get_stylesheet_directory_uri() . '/js/intlTelInput.js' );
    wp_enqueue_script( 'prism', get_stylesheet_directory_uri() . '/js/prism.js' );

    wp_enqueue_style( 'remodal-css', get_stylesheet_directory_uri() . '/css/remodal.css', array(), '3.3.4' );
    wp_enqueue_script( 'remodal-js', get_stylesheet_directory_uri() . '/js/remodal.min.js', array( 'jquery' ), '1.0', true );
    wp_enqueue_script( 'custom', get_stylesheet_directory_uri() . '/js/custom.js', array( 'jquery' ), '1.0', true );
}

if( is_admin() ) {
    wp_enqueue_style( 'remodal-css', get_stylesheet_directory_uri() . '/css/remodal.css', array(), '3.3.4' );
    wp_enqueue_script( 'remodal-js', get_stylesheet_directory_uri() . '/js/remodal.min.js', array( 'jquery' ), '1.0', true );
    wp_enqueue_script( 'custom', get_stylesheet_directory_uri() . '/js/custom.js', array( 'jquery' ), '1.0', true );
}

/**

 * Add your custom functions below

 */

include('custom_post_type.php');
include('shortcode/shortcode.php');

add_action( 'template_redirect', 'redirect_to_specific_page' );
function redirect_to_specific_page() {
    if ( is_user_logged_in() ) {
        if (is_page('choose-plan') || is_page('signup') || is_page('sign-in')  ) {

            wp_redirect( site_url('account') );
        }
        global $wp;

        if ( isset( $wp->query_vars['customer-logout'] ) ) {
         wp_redirect( str_replace( '&amp;', '&', wp_logout_url( site_url() ) ));
         exit;
     }
 }

}

// Add a Login hyperlink to the secondary navigation menu if the user is logged-out

add_filter( 'wp_get_nav_menu_items', 'wpa_remove_menu_item', 10, 3 );
function wpa_remove_menu_item( $items, $menu, $args ) {



   if( is_admin() || ! is_user_logged_in() ) {
       foreach ( $items as $key => $item ) {
        if ( 'Account' == $item->title ) unset( $items[$key] );
    }
    return $items;
}


foreach ( $items as $key => $item ) {
    if ( 'Sign In' == $item->title ) unset( $items[$key] );
    if ( 'Sign Up' == $item->title ) unset( $items[$key] );
    if ( 'Sign Out' == $item->title ){$items[$key]->url=htmlspecialchars_decode(wp_logout_url(site_url()));}
}


return $items;
}

// Remove the Logout hyperlink from the secondary navigation menu when the user is logged-in

add_filter( 'wp_get_nav_menu_items', 'wpa_add_menu_item', 10, 3 );
function wpa_add_menu_item( $items, $menu, $args ) {
    if( is_user_logged_in() ) return $items;

    foreach ( $items as $key => $item ) {

        if ( 'Sign Out' == $item->title ) unset( $items[$key] );
    }

    return $items;
}

if ( isset($_POST["username"]) && isset($_POST["email"]) && $_POST["action"] == 'check_user_exist' ) {

    if($_POST['qryVar'] != "set"){
        $email_exists = email_exists($_POST['email']);
        $username_Exists = username_exists($_POST['username']);

        if ( $email_exists ||  $username_Exists ) {

            wp_send_json( array(
                'status' => false,
                'message'  => 'Sorry, that Username or Email already exists!',
            ) );


        } else {

            wp_send_json( array(
                'status' => true,
                'message'  => 'Sorry, that Username or Email not exist!',
            ) );

        }
    }

    exit;
}


if( is_user_logged_in() && !is_admin() ) {/*
    ?><style>#button-78f8d699989b8cf6bd077743dd4cb4b5 {display: none;}</style><?php */
}

require __DIR__ . '/twilio-php-master/Twilio/autoload.php';
// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;


/*add_action( 'transition_post_status', 'send_notification_post', 10, 3 );

function send_notification_post($new_status, $old_status, $post) {

    if($post->post_type == 'example-trade') {
        if ( $new_status === 'publish' && $old_status !== 'publish' && $old_status !== 'trash') {
            $user = new WP_User_Query(
                array(
                    'role'   => 'subscriber',
                    'fields' => array( 'ID', 'user_email' )
                )
            );
            $subject = 'New Post Created With Title: '.$post->post_title;
            $body_post = $post->post_content;
            $user_results = $user->get_results();

            foreach ($user_results as $user_result ) {

                $phone = get_user_meta($user_result->ID, 'phone', true);
                $subscription_end_date = get_user_meta($user_result->ID, 'current_period_end', true);
                
                if( strtotime($subscription_end_date) >= strtotime(date('Y/m/d')) ) {
                    wp_mail( $user_result->user_email, $subject, $body_post);
                    if($phone != '') {
                        // Your Account SID and Auth Token from twilio.com/console
                        $sid = 'AC03a64c97e3cdab1f9f377de09ec104af';
                        $token = 'e9fd37aa6b6c3c05eddc79a56138b7d6';
                        $client = new Client($sid, $token);
                        // Use the client to do fun stuff like send text messages!
                        $client->messages->create(
                            // the number you'd like to send the message to
                            $phone,

                            array(
                                // A Twilio phone number you purchased at twilio.com/console
                                'from' => '+14152345853',
                                // the body of the text message you'd like to send
                                'body' => $body_post
                            )
                        );
                    } 
                }               
            }
        }
    }
}*/

//add_action( 'post_submitbox_misc_actions', 'custom_button' );

function custom_button() {
    global $post;
    
    if($post->post_type == 'example-trade') {

        $html = '';
        $html .= '<div class="misc-pub-section"><a id="preview_model" class="button-primary popup-with-move-anim" data-toggle="modal" data-target="#myModal" href="#modal" >Customize Me!</a></div>';
        $html .= '<div class="preview remodal remodal-is-initialized remodal-is-closed" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
        <div id="demowrap" class="cd-modal-content">
        <div class="top-bar ">
        <div data-remodal-action="close" class=" backtheme" aria-label="Close"><img src="'.get_template_directory_uri().'/img/back.png" /></div>
        <div class="devicebtnwrap row">
        <div id="mobile" class="icon"><img src="'.get_template_directory_uri().'/img/device1.png" /></div>
        <div id="tablet" class="icon"><img src="'.get_template_directory_uri().'/img/device2.png" /></div>
        <div id="desktop" class="icon current"><img src="'.get_template_directory_uri().'/img/device3.png" /></div>
        </div>                        
        </div>
        <div id="device" class="device desktop">
        <div class="screen desktop">
        <iframe id="iframe" class="demoframe" src="'.site_url().'/?page_id='.get_the_ID().'&preview=true" seamless style="overflow:auto;-webkit-overflow-scrolling:touch"></iframe>
        </div>
        <div class="screen tablet">                            
        <h1 class="post-title"></h1>
        <div class="post-img"><img class="post-thumbnail" src="'.get_the_post_thumbnail(get_the_ID(),"full").'</div>
        <p class="post-content"></p>
        <a href="'.get_permalink(get_the_ID()).'" target="_blank">Checkout</a>
        </div>
        <div class="screen mobile">
        <h1 class="post-title"></h1>
        <p class="post-content"></p>
        <a href="#">Read More..</a>
        </div>
        </div>
        </div>
        </div>';

        echo $html;
    }
}

if(isset($_POST['action']) && $_POST['action'] == 'stripe-plan-update' && wp_verify_nonce($_POST['stripe_nonce'], 'stripe-nonce')) {
    if(isset($_POST['plan_id']) ) {

       /* global $stripe_options;
        require_once __DIR__ . '/stripe-php/init.php';

        $plan_id = strip_tags(trim($_POST['plan_id']));
        $user_id = get_current_user_id();       
                
        // check if we are using test mode
        if(isset($stripe_options['test_mode']) && $stripe_options['test_mode']) {
            $secret_key = $stripe_options['test_secret_key'];
        } else {
            $secret_key = $stripe_options['live_secret_key'];
        }
        $user_id = get_current_user_id();
        $subscription_id = get_user_meta($user_id, 'stripe_subscription_id', true);*/

        $user_id = get_current_user_id();
        $transax_recurring_id = get_user_meta($user_id, 'transax_recurring_id', true);
        /* \Stripe\Stripe::setApiKey($secret_key);*/
        
        if($transax_recurring_id == ''){
            $redirect =  add_query_arg('userId', $user_id, "https://1234investors.com/choose-plan");
            wp_redirect($redirect);
            die(); 
        }
        
        try {
            if($subscription_id != '') {

                $subscription = \Stripe\Subscription::retrieve($subscription_id);           
                $itemID = $subscription->items->data[0]->id;

                $response = \Stripe\Subscription::update($subscription_id, array(
                  "items" => array(
                    array(
                      "id" => $itemID,
                      "plan" => $plan_id,
                  ),
                ),
              ));

                $redirect = add_query_arg('plan_update', 'true', $_POST['redirect']);
                $plan_name = $response->plan->name;
                $current_period_start = $response->current_period_start;
                $current_period_end = $response->current_period_end;
                update_user_meta($user_id, 'stripe_response', $response);
                update_user_meta($user_id, 'plan_name', $plan_name);
                update_user_meta($user_id, 'current_period_start', $current_period_start);
                update_user_meta($user_id, 'current_period_end', $current_period_end);
                
            }
        } catch (Exception $e) {
            // redirect on failure
            $redirect = add_query_arg('plan_update', 'false', $_POST['redirect']);
        }
        wp_redirect($redirect); exit;
        
    }
}

function my_wp_nav_menu_args( $args = '' ) {

    if( is_user_logged_in() ) { 
        $args['menu'] = 'logout-menu';
    } else { 
        $args['menu'] = 'Menu 1';
    } 
    return $args;
}
add_filter( 'wp_nav_menu_args', 'my_wp_nav_menu_args' );

/*if(isset($_POST['action'])){
	
	if(isset($_POST['plan_id']) ) {
		$AccountSid = "ACb571271fc8117397fe7567832413099c";
    $AuthToken = "5913b9b1588377aca6e3dd5f434ac274";

    // Step 3: instantiate a new Twilio Rest Client
    $client = new Client($AccountSid, $AuthToken);
	 $client->messages->create(
                            // the number you'd like to send the message to
                            '+919913697309',

                            array(
                                // A Twilio phone number you purchased at twilio.com/console
                                'from' => '+18043125563',
                                // the body of the text message you'd like to send
                                'body' => 'trial message'
                            )
                        );

	

        // Display a confirmation message on the screen
       // echo "Sent message to ";
		
   // }
	}
}*/
/*add_action('publish_post', 'wpse120996_add_custom_field_automatically');
function wpse120996_add_custom_field_automatically($post_id) {*/
   add_action( 'transition_post_status', 'send_notification_post', 10, 3 );

   function send_notification_post($new_status, $old_status, $post) { 

    require_once 'googleapi.php';
    $key = 'AIzaSyDiqBezYfLX4QTUW5w-R1gOrdwXdVt6Fso';
    $googer = new GoogleURLAPI($key);

    if(($post->post_type == 'example-trade') || ($post->post_type == 'post')){
       if ( $new_status === 'publish' && $old_status !== 'publish' && $old_status !== 'trash') { 
          $url = get_permalink($post->ID);
          $body_post = $googer->shorten($url);

          $users = get_users( array( 'role'   => 'subscriber','fields' => array( 'ID' ) ) );
          foreach($users as $user_id){

              $data=get_user_meta( $user_id->ID);

              $phone = $data['phone'];
              $subscriber_end = $data['current_period_end'];
              $d=strtotime("now");
              if($subscriber_end[0] >= date('Y-m-d H:i:s',$d)){

                // echo '1 '.$subscriber_end[0];
                // echo '2'.date('Y-m-d H:i:s',$d);


                  if(!empty($phone[0]))
                  {
			 $AccountSid = "ACb571271fc8117397fe7567832413099c"; //ACf7d74450e825b3c738425fa876d1c682
    		 $AuthToken = "5913b9b1588377aca6e3dd5f434ac274"; //1a524f648d8052033deb39a7e9ca82fe
           //  $AccountSid = "ACf7d74450e825b3c738425fa876d1c682"; //ACf7d74450e825b3c738425fa876d1c682
           //  $AuthToken = "1a524f648d8052033deb39a7e9ca82fe"; //1a524f648d8052033deb39a7e9ca82fe
             $client = new Client($AccountSid, $AuthToken);
                        // Use the client to do fun stuff like send text messages!
             try {	
                 $client->messages->create(
                            // the number you'd like to send the message to
                    $phone[0],

                    array(
                                // A Twilio phone number you purchased at twilio.com/console
                                'from' => '+18043125563', //19104057872   //+18043125563
                                // the body of the text message you'd like to send
                                'body' => 'Greetings! New blog/post is published. Click this link: '.$body_post.' to open it.',
                                //'mediaUrl' => '"'.$url.'"',
                            )
                );
             } catch (Exception $e) {
                print_r($e->getMessage());
            }



        }
    }
}


}
}

}

add_action('wp_head', 'custom_css');
function custom_css(){
    ?>
    <style type="text/css">
    .signup_term_dialog{
        width: 70%;
    }
    .row.signup_modal_term_content {
        padding: 0px 10px;
        text-align: justify;
        font-size: 10px;
        max-height: 550px;
        overflow-x: auto;
    }
    .btn-success{
        background-color:#008125;
    }
    .text-success{
        color:#008125;
    }
    /*#stripe-payment-form .checkRow{
        display: none;
        }*/
        .box{
           display: none;
       }
       .widget_nav_menu .my_class{
        display: none;
    }
    #forget_pass_text{
        color: green;
    }
    #forget_pass{
        font-weight: bold;
        text-decoration: underline;
    }
    #error_msg{
        color: red;
    }
    #refund_policy_modal .modal-title{
        color: #008125;
    }
    div#refund_policy_modal .modal-dialog {
        width: 100% !important;
        max-width: 900px !important;
        margin: 0 auto !important;
        height: 100%;
    }
    div#refund_policy_modal {
        overflow: hidden;
        height: 100%;
        padding: 25px 15px;
    }
    div#refund_policy_modal .modal-header {
        position: sticky;
        top: 0;
        z-index: 111111 !IMPORTANT;
        background: white;
    }
    div#refund_policy_modal .modal-header .close {
        margin-top: -14px;
    }
    div#refund_policy_modal .modal-content {
        height: 100%;
        overflow: auto;
    }
    div#refund_policy_modal .modal-body {

        padding-top: 0;
    }
    @media screen and (max-width: 767px) {
        div#refund_policy_modal {
            z-index: 11111;
            margin: 0 15px;
            padding: 15px 0 !important;
        }

    }

    #transax-payment-form .col-md-12.plan_class .col-md-5 {
        float: none;
        display: inline-block;
    }
    #transax-payment-form .col-md-12.plan_class {
        text-align: center;
    }
</style>
<?php

if(is_page('choose-plan')){
    ?>
    <style>
    .sh-footer-columns, .blog-style-largedate, .container .fw-row, .contact-form .wrap-forms .fw-row {
        position: relative;
        margin: 0 15px;
    }
</style>
<?php }

}

add_action('wp_footer', 'custom_term_condition_modal');
function custom_term_condition_modal(){ ?>

    <script type="text/javascript">
      jQuery(document).ready(function(){
        $(".edu-box").css("cursor","pointer"); 
        $(".edu-box").click(function() { 
           window.location = $(this).find("a").attr("href");   
           return false;
       });
    });
    jQuery(document).ready(function(){

        jQuery.ajax({
            type: "get",
            url: "<?php echo site_url(); ?>/wp-content/plugins/transaxgateway-wordpress/includes/PHP/retriveDetails.php", 
            cache: false,
            contentType: false,
            processData: false,
            success: function(data){
                console.log(data);
            }
        });

        jQuery("#read1link").click(function(){
            jQuery(".box").slideDown("slow");
            jQuery('#read1link').css('display','none');
        });

        jQuery('#read2link').click(function(){
        	jQuery(".box").slideUp("fast");
        	jQuery('#read1link').css('display','block');
        	
        });

        jQuery('#cancel_member').click(function(){

            if(!confirm('Are you sure you want to cancel your Membership?')){
                return false;
            }

            formData = '1';
      /* jQuery.ajax({
                    type: "post",
                    url: "<?php echo admin_url('admin-ajax.php').'?action=cancel_membership'; ?>", 
                    data : formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(data){
                        
                         // window.location.reload();
                       console.log(data);
                      // alert(data);
                      if(data !== 'fail'){
                        window.location.href= "https://1234investors.com/example-trade/";
                      }
                       
                       
                    }
                });*/
       // console.log("<?php echo site_url(); ?>/wp-content/plugins/transaxgateway-wordpress/includes/PHP/deleterecurence.php"); 
       jQuery.ajax({
        type: "post",
        url: "<?php echo site_url(); ?>/wp-content/plugins/transaxgateway-wordpress/includes/PHP/deleterecurence.php", 
        data : formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function(data){
                // if(data == 'refund'){
                //     $('input[name=TransactionType]').val(data);
                //     $('.sign_upform_div').show();
                // } else 
                
                if(data.indexOf("success") > -1){
                    window.location.href= "<?php echo site_url();?>/account";
                } else if(data !== 'fail'){
                    window.location.href= "<?php echo site_url();?>/account?status=fail";
                }
                
                // if(data.indexOf("success") > -1){
                // }
                // if(data !== 'fail'){
                //   
                // }
            }
        });


   });
    });
</script>
<?php 
if(!is_page('about-us')){ ?>
    <style>

    #addressdiv{
        display: none;
    }    
</style>

<?php 
}

?>
<?php if(is_page( 'signup' ) ||(is_page('choose-plan')) || (is_page('transaxgateway'))){
    ?>
    <div class="modal fade" id="signup_modal" role="dialog">
        <div class="modal-dialog signup_term_dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title text-success">Terms and Conditions</h4>
          </div>
          <div class="modal-body">
              <div class="row signup_modal_term_content" style="font-size:14px">
                  <span>
                    <p>Terms and Conditions of 1234investors.com</p>
                    <p>Last revised: January 25, 2018</p>
                    <p>Please read this agreement carefully before using <strong>1234INVESTORS.COM</strong> services.</p>
                    <p>This Agreement is made between 1234investors.com and you, the user and/or member of the Services ("you").</p>
                    <p>1234investors.com ("we","us" or "1234investors.com") provides the website known as 1234investors.com.com and other information, including email newsletters, subject to<br>your compliance with the terms and conditions set forth in this Agreement. Collectively, your use of the website and other information, including video updates and email newsletters, shall be referred to as the "Services".</p>
                    <p>By using the Services, you agree to be bound by these terms and conditions. If you do not agree to these terms and conditions, you may not use the Services. You must be at<br>least 18 years of age to use the Services. If you are not at least 18 years old, you may<br>not access or use the Services.</p><p>We reserve the right at any time to:</p><p>Change the terms and conditions of this Agreement;<br>Change the Services, including eliminating or discontinuing any content on or feature of any of the Services; or<br>Change any fees or charges for use of the Services.<br>Any changes we make will be effective seven (7) days after notice of any change is provided to you, which may be done by any means including, without limitation, posting on the Services or via electronic mail. Your use of the Services after such notice will be deemed acceptance of such changes. Be sure to review this Agreement periodically to ensure familiarity with the most current version. Upon our request, you agree to sign a non electronic version of this Agreement.</p><p>The Services provided are for general informational purposes only. None of the Services is intended as investment, tax, accounting or legal advice, as an offer or solicitation of an offer to sell or buy, or as an endorsement, recommendation or sponsorship of any company, security, or fund. The Services should not be relied upon for purposes of transacting securities or other investments. We cannot and do not assess or guarantee the suitability or profitability of any particular investment, or the potential value of any investment or informational source. You bear responsibility for your own investment research and decisions, and should seek the advice of a qualified securities professional before making any investment.</p><p>Refund Policy. Cancellation will take effect no later than two (2) business days after we receive your notification.</p><ul><li>The first <a>30-day (trial)</a> period of any subscription is always $8 unless otherwise specified. Your credit card will be charged immediately, and if you do not cancel during the 30-day trial period the full subscription cost will be charged on the 31st day after the trial; however, only one $8 trial period is offered per person or address.</li><li>Your credit card will not be charged until the 31st day of subscription service, and ONLY if you do not cancel during the 30-day trial period</li><li>If you do not cancel within seven calendar days after the 30 day period of the<br>subscription has started then you will not be granted a refund.</li><li>If you cancel within seven calendar days after the 30 day period of the subscription then the full amount of the monthly subscription will be refunded to you.</li><li>Example 1 | Preeti subscribed to the trial for $8 and doesn’t use the subscription at all but she forgets to cancel. The subscription begins on day 31 and her credit card is charged $199. <a>Six</a> calendar days go by and she sees the charge on her online banking portal, she cancels his subscription on the seventh calendar day and <a>is INDEED entitled to a refund of $199</a>.</li><li>Example 2 | Kumar subscribed to the trial for $8 and also doesn’t use the subscription at all and also forgets to cancel. The subscription begins on day 31 and his credit card is charged $199. <a>Seven</a> calendar days go by and he sees the charge on his online banking portal on the eighth calendar day of his subscription, he cancels his subscription however he <a>is NOT entitled to a refund of $199</a> because he did not cancel within 7 calendar days of the subscription start date. His subscription will remain active until the 31st day of his subscription and <a>will NOT renew</a> and he <a>will NOT be charged again</a>.</li></ul><p>In all cases, please proceed to: <a href="https://1234investors.com/my-account/">www.1234investors.com.com/cancel</a> to initiate cancellation.</p><p>Termination. This Agreement shall remain effective until terminated in accordance with its terms. 1234investors.com may terminate this Agreement, and/or your access to and use of the Services or any portion thereof, immediately, in the event we determine, in our sole discretion, that you have breached this Agreement. If the subscription is terminated "for cause", you will not receive a refund of any consideration paid for the subscription. In addition, we reserve the right, upon 10 business days notice and the reimbursement of any whole months remaining on your subscription, to terminate this Agreement without cause. In the case of a lifetime subscription, any refund amount will be solely at the discretion of 1234investors.com. A lifetime subscription can be transferred upon the death or incapacity of the subscriber to a family member whose<br>subscription would then terminate upon their death and is not then transferable.</p><p>Conflicts and Disclosure Policy. We do not permit any employee or officer of 1234investors.com to hold positions in individual stocks, unless it is fully disclosed at the time a stock appears on any research published by 1234investors.com.</p><p>Privacy. Our policy with respect to the collection and use of your personal information is set forth in our Privacy Policy.</p><p>Code of Conduct. While using the Services you agree not to:</p><p>a) Restrict or inhibit any other visitor or member from using the Services, including, without limitation, by means of "hacking" or "cracking" or defacing any portion of any of the Services;<br>b) Use the Services for any unlawful purpose; Express or imply that any statements you make are endorsed by us, without our prior written consent; Transmit (a) any content or information that is unlawful, fraudulent, threatening, harassing, abusive, libelous, defamatory, obscene or otherwise objectionable, or infringes on our or any third party’s intellectual property or other rights; (b) any material, non public information about companies without the authorization to do so; (c) any trade secret of any third party; or (d) any advertisements, solicitations, chain letters, pyramid schemes, investment opportunities, or other unsolicited commercial communication (except as otherwise expressly permitted by us); Engage in spamming or flooding; Transmit any software or other materials that contain any virus, worm, time bomb, Trojan horse, or other harmful or disruptive component; Modify, adapt, sublicense, translate, sell, reverse engineer, decompile or disassemble any portion of the Services; Remove any copyright, trademark, or other proprietary rights notices contained in the Services; "Frame", "mirror" or link to any part of the Services without our prior written authorization; Use any robot, spider, Services search/retrieval application, or other manual or automatic device or process to retrieve, index, "data mine," or in any way reproduce or circumvent the navigational structure or presentation of the Services or its content; Harvest or collect information about Services visitors or members without their express consent; copy, download or rebroadcast any video files made available as part of the Services or permit anyone else not residing in Subscriber’s household to use any of the Services through your subscription, username or password.<br>While using the Services you agree to comply with all applicable laws, rules and regulations.</p><p>Submissions. Please note that, because we receive many emails and suggestions from our members, and sometimes redistribute materials you give us, we need to obtain certain rights in those materials. By sending or transmitting to us creative suggestions, ideas, notes, concepts, information, or other materials (collectively, "Materials"), you grant us and our designees a worldwide, non exclusive, sublicensable (through multiple tiers), assignable, royalty free, perpetual, irrevocable right to use, reproduce, distribute (through multiple tiers), create derivative works of, publicly perform, publicly display, digitally perform, make, have made, sell, offer for sale and import such Materials in any media now known or hereafter developed, for any purpose whatsoever, commercial or otherwise, without compensation to the provider of the Materials. None of the Materials disclosed shall be subject to any obligation, whether of confidentiality, attribution, or otherwise, on our part and we shall not be liable for any use or disclosure of any Materials.</p><p>Services and Tools. Your use of certain Services may be governed by additional rules, which are available on 1234Investors.com.com or by hyperlink from other Sites, in connection with the service. By using any service you are acknowledging that you have reviewed all corresponding rules and agree to be bound by them. Some of the services may have been provided by third parties for your use. You expressly acknowledge and agree that your use of all services is solely at your risk.</p><p>Sweepstakes, Contests, and Games. Any sweepstakes, contests, and games that are accessible through the Services are governed by specific rules. By entering such sweepstakes or contests or participating in such games you will become subject to those rules. Furthermore, your participation in such sweepstakes, contests or games constitutes your acknowledgment that your participation is not in violation of the rules of the state in which you reside.</p><p>Claims of Copyright Infringement. The Digital Millennium Copyright Act of 1998 (the "DMCA") provides recourse for copyright owners who believe that material appearing on the Internet infringes their rights under U.S. copyright law. If you believe in good faith that any material hosted by 1234 Investors. infringes your copyright, you (or your agent) may send us a notice requesting that the material be removed, or access to it blocked. If you believe in good faith that a notice of copyright infringement has been wrongly filed against you, the DMCA permits you to send us a counter notice. Notices and counter notices must meet the then current statutory requirements imposed by the DMCA; see <a href="https://www.copyright.gov/" target="_blank" rel="noopener">http://www.loc.gov/copyright/</a> for details. Notices and counter notices with respect to the Services should be sent via electronic mail to <a href="mailto:info@1234investors.com">info@1234investors.com</a> We suggest that you consult your legal advisor before filing a notice or counter notice. Also, be aware that there can be penalties for false claims under the DMCA.</p><p>Ownership and Restrictions on Use. The Services are owned and operated by us in conjunction with others pursuant to contractual arrangements. You may only access and use the materials on the Services, and download and/or print out only one copy of any materials on the Services, solely for your personal use. You may not republish, upload, post, transmit or distribute materials from the Services in any way, without our prior written permission. Modification of the materials or use of the materials for any other purpose is a violation of our copyright and other proprietary rights, and is strictly prohibited. You acknowledge that you do not acquire any ownership rights by using the Services.</p><p>1234investors.com.com is a trademark and/or service mark of 1234investors.com All other trademarks, service marks, and logos used by 1234investors.com are the trademarks, service marks, or logos of their respective owners.</p><p>Jurisdictional Issues. The Services are solely directed to individuals residing in the United States. We make no representation that materials in the Services are appropriate or available for use in other locations. Those who choose to access the Services from other locations do so on their own initiative and at their own risk, and are responsible for compliance with local laws. We reserve the right to limit the availability of the Services and/or the provision of any service or product described thereon to any person, geographic area, or jurisdiction, at any time and in our sole discretion, and to limit the quantity or type of any such service or product that we provide.</p><p>Links to Other Websites. 1234Investors.com.com may contain links to other Internet websites or resources. We neither control nor endorse such other website, nor have we reviewed or approved any content that appears on such other website.</p><p>You acknowledge and agree that we shall not be held responsible for the legality, accuracy, or inappropriate nature of any content, advertising, products, services, or information located on or through any other websites, nor for any loss or damages caused or alleged to have been caused by the use of or reliance on any such content.</p><p>Disclaimers. The services, the materials provided by the services, and any product or service obtained or accessed through the services are provided "as is" and without representations or warranties of any kind, either express or implied. To the fullest extent permissible pursuant to applicable law, 1234 Investors. and its suppliers, advertisers, and agents disclaim all warranties, express, implied or statutory, including, but not limited to, implied warranties of title, non infringement, merchantability, and fitness for a particular purpose. Applicable law may not allow the exclusion of implied warranties, so the above exclusions may not apply to you. 1234 Investors. and its suppliers, agents and sponsors do not warrant that your use of the services will be uninterrupted, error free, or secure, that defects will be corrected, or that the services or the server on which 1234Investors.com, and its affiliated sites, are hosted are free of viruses or other harmful components. You acknowledge that you are responsible for obtaining and maintaining all telephone, computer hardware and other equipment needed to access and use the services, and all charges related thereto. You assume total responsibility and risk for your use of the services and your reliance thereon. No opinion, advice, or statement of 1234 Investors. or its suppliers, agents, members, or visitors, whether made through the services or otherwise, shall create any warranty. Your use of the services and any materials provided by the services are entirely at your own risk.</p><p>You acknowledge that the internet and email are not 100% reliable. There may be times where you do not receive an email communication from 1234investors.com. You agree that the email service is provided as a convenience and that the primary source of the Services is the 1234investors.com website. You agree that it is your responsibility, if you do not receive any expected email communication, to check any spam filters or folders and to access any 1234investors.com information on the 1234investors.com website, www.1234investors.com, and use your assigned username and password. 1234investors.com is not responsible for your failure to receive any specific email communication, access the website or view any videos published on the website.</p><p>A possibility exists that the Services could include inaccuracies or errors, or materials that violate these Terms of Service (specifically, the Code of Conduct above). Additionally, a possibility exists that unauthorized alterations could be made by third parties to the Services. Although we attempt to ensure the integrity of the Services, we make no guarantees as to the Services’ completeness or correctness. In the event that such a situation arises, please contact us via electronic mail to <a href="mailto:info@1234investors.com">info@1234investors.com</a> with, if possible, a description of the material to be checked and the location (URL) where such material can be found, as well as information sufficient to enable us to contact you. We will try to address your concerns as soon as reasonably practicable. For copyright infringement claims, see the section on "Claims of Copyright Infringement" above.</p><p>Limitation of Liability. Neither 1234investors.com nor its suppliers, advertisers, affiliates, or agents or sponsors are responsible or liable for any direct, indirect, incidental, consequential,special, exemplary, punitive or other damages under any contract, negligence, strict liability or other theory arising out of or relating in any way to the services and/or content contained on the services, or any product or service purchased through the services.</p><p>Your sole remedy for dissatisfaction with the services and/or content contained within the services is to stop using the services. The sole and exclusive maximum liability for all damages, losses, and any causes of action whether in contract, tort (including, without limitation, negligence), or otherwise shall be the total amount paid by you, if any, to access the services.</p><p>Indemnification. You agree to indemnify, defend and hold us, our officers, directors, employees, agents and representatives harmless from and against any and all claims, damages, losses, costs (including reasonable attorneys’ fees), or other expenses that arise directly or indirectly out of or from (a) your breach of this Agreement, including any violation of the Code of Conduct above; (b) any allegation that any materials that you submit to us or transmit to the Services infringe or otherwise violate the copyright, trademark, trade secret or other intellectual property or other rights of any third party; and/or (c) your activities in connection with the Services.</p><p>Arbitration. Any controversy or claim, other than an action for injunction, arising out of or relating to this contract, or the breach thereof, the content of the Services or the use of any information obtained from 1234Investorsregardless of the medium of transmission, shall be settled by arbitration administered by the American Arbitration Association under its Commercial Arbitration Rules, in front of one arbitrator, and judgment on the award rendered by the arbitrator(s) may be entered in any court having jurisdiction thereof. Any such arbitration shall take place in Palm Beach County, Florida. You agree that the prevailing party in an arbitration between you and 1234Investorsshall be entitled to reimbursement of reasonable attorneys’ fees and costs and that the arbitrator shall include an amount of attorneys’ fees and costs in the award.</p><p>Miscellaneous. This Agreement is governed by and construed in accordance with the laws of the State of Florida, United States of America, without regards to its principles of conflicts of law. You agree to the personal jurisdiction of the federal and state courts located in Palm Beach County, Florida, United States of America, and waive any jurisdictional, venue, or inconvenient Forum objections to such courts for any dispute not otherwise governed by the agreement to arbitrate contained herein. If any provision of this Agreement is found to be unlawful, void, or for any reason unenforceable, then that provision shall be deemed severable from this Agreement and shall not affect the validity and enforceability of any remaining provisions. This Agreement is not assignable, transferable or sublicensable by you except with our prior written consent. You agree that the prevailing party in any court proceeding shall be entitled to an award of reasonable attorneys’ fees and costs.</p><p>No waiver by either party of any breach or default hereunder shall be deemed to be a waiver of any preceding or subsequent breach or default. Any heading, caption or section title contained in this Agreement is inserted only as a matter of convenience and in no way defines or explains any section or provision hereof. This, together with all of 1234Investorspolicies referred to herein, constitutes the entire Agreement between us relating to the subject matter herein and supersedes and any all prior or contemporaneous written or oral Agreements between us.</p><p>Legal Notices. Under California Civil Code Section 1789.3, California residents are entitled to the following specific consumer rights information:</p><p>Pricing Information: Current rates for our Services may be obtained via electronic mail at <a href="mailto:info@1234investors.com">info@1234investors.com</a> We reserve the right to change fees, surcharges, other periodic subscription fees or to institute new fees at any time as provided in this Agreement. However, any future price increases will NEVER affect your current subscription rate.</p><p>Complaints should be directed to: <a href="mailto:info@1234investors.com">info@1234investors.com</a></p>
                </span>
            </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-success" data-dismiss="modal" id="btn_signup_accept">I Accept</button>
      </div>
  </div>
</div>
</div>
<script>
            //data-toggle="modal" data-target="#signup_modal"
            jQuery(document).ready(function(){
                jQuery("#stripe-submit").parent().addClass('signup-btn');
                jQuery("#stripe-payment-form .form-row.signup-btn").append('<button type="submit" data-click="1" class="btn-submit" id="submit-signup-term" >Submit Payment</button>');
                jQuery("#stripe-submit").attr('type','button');
                jQuery("#stripe-submit").css('display','none');

                jQuery(document).on('click', '#btn_signup_accept', function(){
                    jQuery("#stripe-submit").attr('type','submit');
                    setTimeout(function(){
                        jQuery("#signup_modal").css("cssText", "display: none;");
                        jQuery('#submit-signup-term').attr('data-click','2');
                        jQuery("#stripe-submit").click();
                    }, 500);
                });

                jQuery('#freePlan').click(function(){
                    jQuery('#freePlan .sh-iconbox-content').html('');
                    jQuery('#freePlan .sh-iconbox-content').html('<p><span style="font-weight: 400"><b><span class="plan-price">$8 </b>of first month</span>  </span></p>');
                });

                jQuery('#terms_id').click(function(){
                	

                	jQuery('#signup_modal').modal('show');
                	jQuery('#signup_modal .modal-footer').css('display','none');

                });
                

            });
        </script>
        <div id="refund_policy_modal" class="modal fade" role="dialog">
            <div class="modal-dialog" >

                <!-- Modal content-->
                <div class="modal-content" >
                    <div class="modal-header" style="border-bottom: 0px;">
                        <button type="button" class="close" data-dismiss="modal" style="font-size: 50px;">&times;</button>
                        <h1 class="modal-title text-center">Refund Policy</h1>
                    </div>
                    <div class="modal-body" >
                        <img src="https://1234investors.com/wp-content/uploads/2018/01/30dayguarantee092316.jpg" alt="refundpolicy">
                        <h3 class="text-center" style="padding:10px">The 1234Investors’s Refund Policy</h3>
                        <ul>
                            <li>The first <a>30-day (trial)</a> period of any subscription is always $8 unless otherwise specified. Your credit card will be charged immediately, and if you do not cancel during the 30-day trial period the full subscription cost will be charged on the 31st day after the trial; however, only one $8 trial period is offered per person or address.</li>
                            <li>Your credit card will not be charged until the 31st day of subscription service, and ONLY if you do not cancel during the 30-day trial period</li>
                            <li>If you do not cancel within seven calendar days after the 30 day period of the subscription has started then you will not be granted a refund.</li>
                            <li>If you cancel within seven calendar days after the 30 day period of the subscription then the full amount of the monthly subscription will be refunded to you</li>
                            <li>Example 1 | Preeti subscribed to the trial for $8 and doesn’t use the subscription at all but she forgets to cancel. The subscription begins on day 31 and her credit card is charged $199. <a>Six</a> calendar days go by and she sees the charge on her online banking portal, she cancels his subscription on the seventh calendar day and <a>is INDEED entitled to a refund of $199</a>.</li>
                            <li>Example 2 | Kumar subscribed to the trial for $8 and also doesn’t use the subscription at all and also forgets to cancel. The subscription begins on day 31 and his credit card is charged $199. <a>Seven</a> calendar days go by and he sees the charge on his online banking portal on the eighth calendar day of his subscription, he cancels his subscription however he <a>is NOT entitled to a refund of $199</a> because he did not cancel within 7 calendar days of the subscription start date. His subscription will remain active until the 31st day of his subscription and <a>will NOT renew</a> and he <a>will NOT be charged again</a>.</li>
                        </ul>
                        <p>In all cases, please proceed to: <a href="https://1234investors.com/my-account/">www.1234investors.com/cancel</a> to initiate cancellation.</p>
                        <p class="text-center"><button type="button" data-dismiss="modal" value="Accept Policy" class="btn btn-success" id="accept_policy">Accept Policy</button></p>
                    </div>
                    
                </div>

            </div>
        </div>

        <script>
         jQuery(document).ready(function(){

          jQuery('.sign_upform_div').hide();
          jQuery('#accept_policy').click(function(){
              if ( jQuery( ".sign_upform_div" ).is( ":hidden" ) ) {
                 jQuery( ".sign_upform_div" ).slideDown( "slow" );
    							//jQuery('#accept_policy').css('display','none');
                           }

                       });
          jQuery('.membership-plan-new a.sh-pricing-button').click(function(){
              if(jQuery( ".sign_upform_div" ).is( ":hidden" )){
                 jQuery('#refund_policy_modal').modal('show');
             }
             else{
                 jQuery('#refund_policy_modal').css('display','none');
             }

         });
      });
  </script>
  <div id="registration_modal" class="modal fade" role="dialog">
      <div class="modal-dialog" >
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background: linear-gradient(to right,white, green);">
                <!--<button type="button" class="close" data-dismiss="modal" style="color: white;font-weight: bold;">&times;</button>-->
                <h4 class="modal-title" style="background: linear-gradient(to right,green, white); -webkit-background-clip: text;-webkit-text-fill-color: transparent;">1234investors.com</h4>
            </div>
            <div class="modal-body">
                <h3 style="text-align:center;"><img src="https://1234investors.com/wp-content/themes/jevelin/img/download.jpg" /></h3>

                <h3 style="text-align:center;">Success</h3>
                <p>You have successfully subscribed to <span style="color: green;font-weight: 600;">1234investors site.</span></p>
                <p>You will get SMS related to trading information soon.</p>
                <!--<p>You can now use your Username and Password to sign in into the 1234investors.com Click on Continue button to login.</p>-->
                                    <!--<p><button type="button"  value="continue" class="btn btn-success" id="cont_login">Click here to login</button>
                                    -->
                                    <p style="text-align:center;"><button type="button" style="background-color:#2bab2b;" class="btn btn-success" data-dismiss="modal">OK</button></p>
                                </div>
                                
                            </div>

                        </div>
                    </div>
                    <script>
                      jQuery(document).ready(function(){
                          jQuery('#cont_login').click(function(){
                              url = 'http://rockerstech.com/design/1234investor2/sign-in/';
                              location.href = url; 
                          }); 
                      });
                  </script>
                  <style>
              /*@media (max-width: 767px){
                  #registration_modal .modal-dialog{
                      width: 100% !important;
                  } 
                  }*/
                  #registration_modal .modal-dialog{
                      height: 100%;
                      max-width: 400px;
                      margin: auto;
                      min-height: calc(100vh - 60px);
                      display: flex;
                      flex-direction: column;
                      justify-content: center;
                      overflow: auto;
                  }
                  #registration_modal{
                      z-index: 9999;
                  }
              </style>
              <div id="payment_failed_modal" class="modal fade" role="dialog">
                  <div class="modal-dialog" >
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header" style="background: linear-gradient(to right,white, green);">
                            <!--<button type="button" class="close" data-dismiss="modal" style="color: white;font-weight: bold;">&times;</button>-->
                            <h4 class="modal-title" style="background: linear-gradient(to right,green, white); -webkit-background-clip: text;-webkit-text-fill-color: transparent;">1234investors.com</h4>
                        </div>
                        <div class="modal-body">
                            <h3 style="text-align:center;"><img src="https://1234investors.com/wp-content/themes/jevelin/img/faild_payments.jpg" /></h3>

                            <h3 style="text-align:center;">Payment failed</h3>
                            <p style="text-align:center;">Please Try again.</p>

                            <!--<p>You can now use your Username and Password to sign in into the 1234investors.com Click on Continue button to login.</p>-->
                                    <!--<p><button type="button"  value="continue" class="btn btn-success" id="cont_login">Click here to login</button>
                                    -->
                                    <p style="text-align:center;"><button type="button" style="background-color:#2bab2b;" class="btn btn-success" data-dismiss="modal">OK</button></p>
                                </div>
                                
                            </div>

                        </div>
                    </div>
                    <style>
              /*@media (max-width: 767px){
                  #registration_modal .modal-dialog{
                      width: 100% !important;
                  } 
                  }*/
                  #payment_failed_modal .modal-dialog{
                      height: 100%;
                      max-width: 400px;
                      margin: auto;
                      min-height: calc(100vh - 60px);
                      display: flex;
                      flex-direction: column;
                      justify-content: center;
                      overflow: auto;
                  }
                  #payment_failed_modal{
                      z-index: 9999;
                  }
              </style>
              <?php
          }
      }

//add_filter( 'woocommerce_account_menu_items', 'add_my_menu_items', 99, 1 );



      add_filter( 'woocommerce_account_menu_items', 'add_my_menu_items', 99, 1 );

      function add_my_menu_items( $items ) {

        $items = array(
    //  endpoint   => label

            'edit-account' => __( 'Personal Details', 'my_plugin' ),
            'dashboard' => __( 'Plan Details', 'my_plugin' ),
            'customer-logout' => __( 'Log Out', 'my_plugin' ),
        );

        return $items;
    }

    function save_additional_account_details( $user_ID ){
    //print_r($user_ID);

        $phone = ! empty( $_POST['account_phone_number'] ) ? wc_clean( $_POST['account_phone_number'] ) : '';

        update_user_meta( $user_ID, 'phone', $phone );
    }
    add_action( 'woocommerce_save_account_details', 'save_additional_account_details' );


add_action('wp_ajax_cancel_membership', 'cancel_membership'); //cancel_transax_membership
add_action('wp_ajax_nopriv_cancel_membership', 'cancel_membership');

function cancel_membership(){
    //echo '1';
    $current_user_id = get_current_user_id();
    //echo $current_user_id;
    $user_meta = get_user_meta($current_user_id);

    $subscription_id_data = $user_meta['stripe_subscription_id'];
    $subscription_id=$subscription_id_data[0];
   // echo $subscription_id;
    global $stripe_options;

    require_once __DIR__ . '/stripe-php/init.php'; 
    if(isset($stripe_options['test_mode']) && $stripe_options['test_mode']) {
        $secret_key = $stripe_options['test_secret_key'];
    } else {
        $secret_key = $stripe_options['live_secret_key'];
    }
    //echo $secret_key;

    try {

        \Stripe\Stripe::setApiKey($secret_key);

        $subscription = \Stripe\Subscription::retrieve($subscription_id);
        $subscription->cancel(); 

        /*echo '<pre>';
        print_r($subscription);*/
        echo 'success';
        //echo '</pre>';
        $itemID = $subscription->items->data[0]->id;
        //echo 'item'.$itemID.'<br>';
        $created_dt = $subscription->items->data[0]->created;
        $x=date('Y:m:d H:i:s',$created_dt);
       // echo 'dt'. $x;
        update_user_meta($current_user_id,'stripe_subscription_id','');
        // $subscription_id = get_user_meta($user_id, 'stripe_subscription_id', true);
        $d=strtotime("now");
        $current_time = strtotime('Y:m:d H:i:s',$d);
        update_user_meta($current_user_id,'current_period_end',$current_time);
       // update_user_meta($current_user_id,'current_period_start','');

    }catch(Exception $e){


      echo 'fail';

  }


  die();     

}

add_action('wp_logout','auto_redirect_after_logout');
function auto_redirect_after_logout(){
    $referrer = $_SERVER['HTTP_REFERER'];
    if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
     wp_redirect( 'http://rockerstech.com/design/1234investor2/sign-in/');
     exit();
 }

}

function copyright_year( $atts ){
    ob_start();
    echo date("Y");
    return ob_get_clean();
}
add_shortcode( 'copyrightyear', 'copyright_year' );

/*
 * Custom page Shortcode
*/
function example() {

    $count_posts = wp_count_posts('example-trade');
    $published_posts = $count_posts->publish;
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; 
    $user_id = get_current_user_id();
    $stripe_subscription_id = get_user_meta($user_id, 'stripe_subscription_id', true);
    $subscription_end = get_user_meta($user_id, 'current_period_end', true);
                //$current_date = strtotime(date('Y/m/d h:i:s'));
    $current_date = (date('Y/m/d h:i:s'));
    $d=strtotime("now");
    $current_dt=date('Y-m-d H:i:s',$d);

    if( isset($_GET['plan_update']) && $_GET['plan_update'] == 'true' ) {
        echo '<p class="plan-update-success">' . __('Thank you for updating your subscription plan.', 'pippin_stripe') . '</p>';
    } elseif  (isset($_GET['plan_update']) && $_GET['plan_update'] == 'false' ) {
        echo '<p class="plan-update-error">' . __('Sorry your subscription plan not updated. Please try again.', 'pippin_stripe') . '</p>';
    }

    if( is_super_admin() ) {
        $posts_per_page = get_option( 'posts_per_page' );
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    } else if( is_user_logged_in() &&  $subscription_end >= $current_date ) {
        $posts_per_page = get_option( 'posts_per_page' );
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; 
    } else {
        $posts_per_page = 5;
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; 
    }
    $loop = new WP_Query( array('post_type' => 'example-trade','orderby'=>'menu_order', 'post_status' => 'publish', 'showposts'   => $posts_per_page, 'paged'=>$paged) );
                // var_dump(have_posts());die;
    if ( $loop->have_posts() ) {
        while ( $loop->have_posts() ) : $loop->the_post();


            get_template_part( 'content-format-example-trades' );


        endwhile;
    } else {
        get_template_part( 'content', 'none' );
    }
                        //echo $subscription_end.' '.' '.$current_dt;        
    if( is_super_admin() ) {
        jevelin_pagination();
    }else if( is_user_logged_in() && $subscription_end >= $current_date ) {
        jevelin_pagination();
    } else if( is_user_logged_in() && !is_super_admin() && $subscription_end <= $current_dt) {
                            //echo $subscription_end.' '.$current_date;
        echo '<div class="load-more"><a data-toggle="StripePlanUpdate" data-target="#StripePlanUpdate" href="#StripePlanUpdate" class="btn-submit">Update Plan</a></div>';
        echo do_shortcode('[stripe-plan-update]');
    } else if( !is_user_logged_in() ) {
        echo '<div class="load-more">
        <h3>And There Is Much More!</h3>                
        <a href="'. site_url().'/choose-plan" class="btn-submit">Click here to Sign Up Now</a>               
        </div>' ;

    } 

}
add_shortcode( 'example-trade', 'example' );

//applies when wrong login credentials given
add_filter('login_redirect', 'my_login_redirect', 10, 3);
function my_login_redirect($redirect_to, $request, $user) {

 $referrer = $_SERVER['HTTP_REFERER'];

 if (is_wp_error($user)) {
        //Login failed, find out why...
    $error_types = array_keys($user->errors);
        //Error type seems to be empty if none of the fields are filled out
    $error_type = 'both_empty';
        //Otherwise just get the first error (as far as I know there
        //will only ever be one)
    if (is_array($error_types) && !empty($error_types)) {
        $error_type = $error_types[0];
    }

        //cindition for wp-admin
    if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
     wp_redirect("http://rockerstech.com/design/1234investor2/sign-in/?login=failed&reason=" . $error_type ); 
     exit;
 }

} 
else {
        //Login OK - redirect to another page?
    if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
           //return home_url();
     wp_redirect('http://rockerstech.com/design/1234investor2/account'); 
     exit();
 }
 else{
    wp_redirect('http://rockerstech.com/design/1234investor2/wp-admin/');
    exit();
}


}
}

//function to remove login credentials set by s2member plugin
function fix_login_redirect() {
    remove_filter( "init", 'c_ws_plugin__s2member_login_redirects_r::remove_login_redirect_filters', 11 );

}
add_action( 'init', 'fix_login_redirect', 1, 3 );


add_action('wp_ajax_send_googlesheet_notification', 'send_googlesheet_notification'); //cancel_transax_membership
add_action('wp_ajax_nopriv_send_googlesheet_notification', 'send_googlesheet_notification');

function send_googlesheet_notification(){

    require_once 'googleapi.php'; 
    $key = 'AIzaSyDiqBezYfLX4QTUW5w-R1gOrdwXdVt6Fso';
    $googer = new GoogleURLAPI($key);
    


   // $shortDWName = $googer->shorten("https://docs.google.com/spreadsheets/d/1upmwSMwoXpIAiFLa2to6PSG_snfGb1xf_1LrXgr7VYE/edit?usp=sharing");
    // $body_post = '123';
    //$shortDWName = $googer->shorten("https://docs.google.com/spreadsheets/d/1UOY7JqKxxMxU0jdN3uKj7lc0rko2d7ZI1ZnpA3eogis/edit?usp=sharing");
    /*$shortDWName = $shortDWName = $googer->shorten("https://drive.google.com/file/d/1J7iXebusPHw0XFD2VFOeXbEE5GWUiXQ1/view");
    $shortDWName = $shortDWName = $googer->shorten("https://docs.google.com/spreadsheets/d/1UOY7JqKxxMxU0jdN3uKj7lc0rko2d7ZI1ZnpA3eogis/edit?usp=sharing");
    $spreadsheet_url = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vSYOB7JHyLKj0NHD_3aAvG6FHdaqoXofeTNCivy9nF-d2slXdELgX5z9mQ2pj5zUFEK8SwVMA4uVeYQ/pub?output=csv';
    $csv = file_get_contents($spreadsheet_url);

    $rows = explode("\n",$csv);
    $data = array();
    $names = array();
    for($i=0; $i<count($rows); $i++) {
        if($i==0){
            $names = str_getcsv($rows[$i]);
        }else{
        $data[] = str_getcsv($rows[$i]);
        }
    }
    $row_data = $data['2'];*/ // get entire 4th row
   // echo $names['1']." : ".$row_data['1'].'<br>';
   // echo $names['5']." : ".$row_data['5'].'<br>';
    $rows = getsheetdata1();
    $cnt = 0;
    foreach($rows as $r){
    //print_r($r['ddd']);
        $cnt++;
        if($cnt=='2'){
            $str = $r['ddd'];
        //print_r($str);
        //echo '<br>'.'************************'.'<br>';
            $row_1 = explode(",",$str);
        /*echo '<pre>';print_r($row_1); echo '</pre>';
        echo '<br>';
        echo $row_1['0'].'<br>';
        echo $row_1['4'].'<br>';*/
        $b1 = str_replace('symbol', 'Ticker',$row_1['1']);

        $b2 = $row_1['2'];  
        $b3 = $row_1['18'];
        $b4 = $row_1['15'];
        $b5 = $row_1['16'];
        $b6 = $row_1['11'].','.$row_1['12'].','.$row_1['13'];
        $b1 = str_replace('ticker:', 'Ticker - ',$row_1['1'] );
        $b2 = str_replace('alerttype:','Alert Type - ',$row_1['2']);
        $b3 = str_replace('stoploss:','Stop Loss - ',$row_1['15']);    
        $b4 = str_replace('target1:','Target1 - ',$row_1['16']);
        $b5 = str_replace('target2:','Target 2 - ',$row_1['17']);
        $b6 = str_replace('expiration:','Expiration - ',$row_1['11'].','.$row_1['12'].','.$row_1['13']); 
       // $b2 = str_replace("buystrikeprice","Buy Strike Price",$b2);
    }
    
      //  echo '<br>';
}
   /* $body_post = "Latest Treding Update: \n";
    $body_post .= $names['1']." : ".$row_data['1']."\n";
    $body_post .= $names['5']." : ".$row_data['5']."\n";
    $body_post .= "Visit the link ".$shortDWName ." for more updates"; */
    //$shortDWName = 'https://goo.gl/rr9CrD';
    //$body_post = "Latest Options Alert: \n";
    $body_post .= $b1."\n";
    $body_post .= $b2."\n";
    $body_post .= $b3."\n";
    $body_post .= $b4."\n";
    $body_post .= $b5."\n";
    $body_post .= $b6."\n";
    //$body_post .= "Visit the link ".$shortDWName ." for more updates";
    echo $body_post;
    die();
    
    $users = get_users( array( 'role'   => 'subscriber','fields' => array( 'ID' ) ) );
    foreach($users as $user_id){

        $data=get_user_meta( $user_id->ID);

        
        $phone = $data['phone'];
        $subscriber_end = $data['current_period_end'];
        $d=strtotime("now");
        if($subscriber_end[0] >= date('Y-m-d H:i:s',$d)){

       // echo '1 '.$subscriber_end[0];
      //  echo '2'.date('Y-m-d H:i:s',$d);


            if(!empty($phone[0]))
            {

             $AccountSid = "ACb571271fc8117397fe7567832413099c"; //ACf7d74450e825b3c738425fa876d1c682
             $AuthToken = "5913b9b1588377aca6e3dd5f434ac274"; //1a524f648d8052033deb39a7e9ca82fe
           //  $AccountSid = "ACf7d74450e825b3c738425fa876d1c682"; //ACf7d74450e825b3c738425fa876d1c682
           //  $AuthToken = "1a524f648d8052033deb39a7e9ca82fe"; //1a524f648d8052033deb39a7e9ca82fe
             
             //dhruti twilio test account
           //  $AccountSid = "ACf7d74450e825b3c738425fa876d1c682"; 
            // $AuthToken = "1a524f648d8052033deb39a7e9ca82fe"; 
             $client = new Client($AccountSid, $AuthToken);
                        // Use the client to do fun stuff like send text messages!
             try { 
                 $client->messages->create(
                            // the number you'd like to send the message to
                    $phone[0],

                    array(
                                // A Twilio phone number you purchased at twilio.com/console
                                'from' => '+18043125563', //19104057872   //+18043125563
                              //    'from' => '+19104057872', //dhruti
                                // the body of the text message you'd like to send
                                'body' => $body_post,
                                //'mediaUrl' => '"'.$url.'"',
                            )
                );
             } catch (Exception $e) {
                 print_r($e->getMessage());
             }



         }
     }
 }

 die();
 
} 

function my_login_logo_one() { 
    ?> 
    <style type="text/css"> 
    body.login div#login h1 a {
        background-image: url(https://1234investors.com/wp-content/uploads/2017/12/1234-Final-Logo.png);  /*Add your own logo image in this url */
        padding-bottom: 30px; 
    } 
</style>
<?php 
} add_action( 'login_enqueue_scripts', 'my_login_logo_one' );


function getsheetdata1(){
    //echo '1';


    class GData {
     public static function getSpreadsheetData($url, $rowFormatArr) {
      $content = file_get_contents($url);
      $contentArr = json_decode($content, true);
      $rows = array();
      /*echo '<pre>';
      print_r($contentArr);
      echo '</pre>';*/
      foreach($contentArr['feed']['entry'] as $row) {
       if ($row['title']['$t'] == '-') {
        continue;
    }
    $rowItems = array();
    foreach($rowFormatArr as $item) {
        $rowItems[$item] = self::getRowValue($row['content']['$t'], $rowFormatArr, $item);
    }
    $rows[] = $rowItems;
}
return $rows;
}
static function getRowValue($row, $rowFormatArr, $column_name) {
     // echo "getRowValue[$column_name]:$row";
  if (empty($column_name)) {
   throw new Exception('column_name must not empty');
}
$begin = strpos($row, $column_name);
    //  echo "begin:$begin"."<br>";

if ($begin == -1) {
   return '';
}
      //echo $begin.' '.strlen($column_name).' '.$column_name.'<br>';
$begin = $begin + strlen($column_name) + 1;
      // echo $begin;
$end = -1;
$found_begin = false;
foreach($rowFormatArr as $entity) {
        // echo "checking:$entity";
   if ($found_begin && strpos($row, $entity) != -1) {
    $end = strpos($row, $entity) - 2;
           // echo "end1:$end";
    break;
}
if ($entity == $column_name) {
    $found_begin = true;
}
         #check if last element
if (substr($row, strlen($row) - 1) == $column_name) {
    $end = strlen($row);
} else {
    if ($end == -1) {
     $end = strlen($row);
 } else {
     $end = $end + 2;
 }
}
}
     // echo "end:$end";
     // echo "$column_name:$row";


$value = substr($row, $begin, $end - $begin);
      //echo $value;
$value = trim($value);

     // echo "${column_name}[${begin}-${end}]:[$value]"."<br>";
      //return $value;
return $row;
}
}

//$url = 'https://spreadsheets.google.com/feeds/list/1upmwSMwoXpIAiFLa2to6PSG_snfGb1xf_1LrXgr7VYE/od6/public/basic?alt=json';
//$url = 'https://spreadsheets.google.com/feeds/list/1UOY7JqKxxMxU0jdN3uKj7lc0rko2d7ZI1ZnpA3eogis/default/public/basic?alt=json'; url for worksheet1 (Master)
$url = 'https://spreadsheets.google.com/feeds/list/1xTkvf2TarK2W040jVW3evIv98yOZvUPSzHbg0sOPmJ0/1/public/basic?alt=json';
 // this url is for getting data from worksheet 2 (Alets and  PNL OPtion Alerts)
$formatArr = array('ddd');
$rows = GData::getSpreadsheetData($url, $formatArr);
return $rows;
}

add_action('wp_ajax_send_googlesheet_notification11', 'send_googlesheet_notification11'); //cancel_transax_membership
add_action('wp_ajax_nopriv_send_googlesheet_notification11', 'send_googlesheet_notification11');
function send_googlesheet_notification11(){

    require_once 'googleapi.php'; 
    $key = 'AIzaSyDiqBezYfLX4QTUW5w-R1gOrdwXdVt6Fso';
    $googer = new GoogleURLAPI($key);
    
    //$url = 'http://gsx2json.com/api?id=1UOY7JqKxxMxU0jdN3uKj7lc0rko2d7ZI1ZnpA3eogis&sheet=2';
    $url = 'http://gsx2json.com/api?id=1xTkvf2TarK2W040jVW3evIv98yOZvUPSzHbg0sOPmJ0&sheet=1';
   // $shortDWName = $googer->shorten("https://1234investors.com/pl/");
    $ch=curl_init($url);
    $data_string = json_encode($fields);

    
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
   // curl_setopt($ch, CURLOPT_POSTFIELDS, array('params'=>$data_string));

    
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    $response = curl_exec($ch);
    $status = curl_getinfo($ch);
    //$result = curl_exec($ch);
    curl_close($ch);
    $body_post = '';

    $json_res = json_decode($response);
    $cnt = 0;
    foreach($json_res->rows as $key => $row){
    	$cnt ++ ;
    	if($cnt == '1'){
    		/*echo '<pre>';
    	    print_r($row);
    	    echo '</pre>';*/
    	    $arr [] =$row;
    	    $i = 'A';
    	   // echo 'get data for 3rd row'.'<br>';
    	    foreach ($row as $key => $value) {
    	    	# code...
                /*if(($i == 'C')) {
                	//echo '2'.$i.' '.$key .' '.$value.'<br>';
                	$c2row = $key.' - '.$value;
                    $c2row = $value;
                } 
    	    	if($i == 'D'){
    	    		//echo '2'.$i.' '.$key .' '.$value.'<br>';
    	    		$d2row = $key.' - '.$value;
    	    		$d2row = $value;
    	    	}
    	    	if($i == 'O'){
    	    		//echo '2'.$i.' '.$key .' '.$value.'<br>';
    	    		$r2row = $key.' - '.$value;
    	    		$r2row = $value;
    	    	}
    	    	if($i == 'P'){
    	    		//echo '2'.$i.' '.$key .' '.$value.'<br>';
    	    		$o2row = $key.' - '.$value;
    	    		$o2row = $value;
    	    	}
    	    	if($i == 'Q'){
    	    		//echo '2'.$i.' '.$key .' '.$value.'<br>';
    	    		$p2row = $key.' - '.$value;
    	    		$p2row = $value;
    	    	}
    	    	if($i == 'M'){
    	    		//echo '2'.$i.' '.$key .' '.$value.'<br>';
    	    		$m2row = $key.' - '.$value;
    	    		$m2row = $value;
    	    	}*/
                if(($value == '-')||($value == '0')){

                }
                else{

                    $body_post .= $key.' - '.$value."\n";
                }
                
                $i++;
            }
        }

    }
   // $shortDWName = 'https://goo.gl/rr9CrD';
    //$body_post = "Latest Options Alert: \n"; 
   /* $body_post .= 'Ticker - '.$c2row."\n";
    $body_post .= 'Alert Type - '.$d2row."\n";
    $body_post .= 'Stop Loss - '.$r2row."\n";
    $body_post .= 'Target1 -  '.$o2row."\n";
    $body_post .= 'Target 2 - '.$p2row."\n";
    $body_post .= 'Expiration - '.$m2row."\n";*/
   // $body_post .= "Visit the link ".$shortDWName ." for more updates";
    

   //echo $body_post;


    
    $users = get_users( array( 'role'   => 'subscriber','fields' => array( 'ID' ) ) );
    foreach($users as $user_id){

        $data=get_user_meta( $user_id->ID);

        
        $phone = $data['phone'];
        $subscriber_end = $data['current_period_end'];
        $d=strtotime("now");
        if($subscriber_end[0] >= date('Y-m-d H:i:s',$d)){

       // echo '1 '.$subscriber_end[0];
      //  echo '2'.date('Y-m-d H:i:s',$d);


            if(!empty($phone[0]))
            {

             $AccountSid = "ACb571271fc8117397fe7567832413099c"; //ACf7d74450e825b3c738425fa876d1c682
             $AuthToken = "5913b9b1588377aca6e3dd5f434ac274"; //1a524f648d8052033deb39a7e9ca82fe
           //  $AccountSid = "ACf7d74450e825b3c738425fa876d1c682"; //ACf7d74450e825b3c738425fa876d1c682
           //  $AuthToken = "1a524f648d8052033deb39a7e9ca82fe"; //1a524f648d8052033deb39a7e9ca82fe
             
             //dhruti twilio test account
           //  $AccountSid = "ACf7d74450e825b3c738425fa876d1c682"; 
            // $AuthToken = "1a524f648d8052033deb39a7e9ca82fe"; 
             $client = new Client($AccountSid, $AuthToken);
                        // Use the client to do fun stuff like send text messages!
             try { 
                 $client->messages->create(
                            // the number you'd like to send the message to
                    $phone[0],

                    array(
                                // A Twilio phone number you purchased at twilio.com/console
                                'from' => '+18043125563', //19104057872   //+18043125563
                              //    'from' => '+19104057872', //dhruti
                                // the body of the text message you'd like to send
                                'body' => $body_post,
                                //'mediaUrl' => '"'.$url.'"',
                            )
                );
             } catch (Exception $e) {
                 print_r($e->getMessage());
             }



         }
     }
 }

 die();
 
} 
?>