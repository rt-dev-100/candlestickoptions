<?php
/**
 * Single Page
 */
 
if(is_page('pl')){
	if(!is_user_logged_in()){
		$url = home_url();
		wp_redirect($url);
		exit();
	}
}
if( jevelin_post_option( get_the_ID(), 'page_layout' ) == 'sidebar-right' || jevelin_post_option( get_the_ID(), 'page_layout' ) == 'sidebar-left' ) :
	$layout_sidebar = esc_attr( jevelin_post_option( get_the_ID(), 'page_layout' ) );
endif;

$class = '';
if( function_exists('fw_ext_page_builder_is_builder_post') && !fw_ext_page_builder_is_builder_post( get_queried_object_id() ) ) {
	$class = ' page-default-content';
}

get_header(); ?>


	<div id="content" class="abcd page-content <?php if( isset($layout_sidebar) && $layout_sidebar ) : ?>content-with-<?php echo esc_attr( $layout_sidebar ); endif; ?><?php echo esc_attr( $class ); ?>">

		<?php
			while ( have_posts() ) : the_post();
				the_content();
			endwhile;
		?>

		<?php /* Clear unclosed floats */ ?>
		<div class="sh-clear"></div>

		<?php 
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

			if ( is_singular() ) :
				wp_enqueue_script( 'comment-reply' );
			endif;
		?>
<?php// echo do_shortcode('[stripe-plan-update]'); die(); ?>
	</div>
	<?php if( isset($layout_sidebar) && $layout_sidebar ) : ?>
		<div id="sidebar" class="<?php echo esc_attr( $layout_sidebar ); ?>">
			<?php get_sidebar(); ?>
		</div>
	<?php endif; ?>

<?php 
if (isset($_GET['reason'])){

    if($_GET['reason'] == 'incorrect_password'){
    	?>
    	 <script>
    	 	jQuery('#error_msg').html('Check your login credentials. Incorrect username or  password.');
    	 </script>
    <?php }

    if($_GET['reason'] == 'invalid_username'){ ?>

    	<script>
    	 	jQuery('#error_msg').html('Check your login credentials. Incorrect username or  password.');
    	 </script>

    <?php } //both_empty
    if($_GET['reason'] == 'both_empty'){ ?>

    	<script>
    	 	jQuery('#error_msg').html('Username and password required');
    	 </script>

    <?php }
    else{ ?>
        <script>
    	 	jQuery('#error_msg').html('Login details are incorrect');
    	 </script>

    <?php }
    
}
    ?>
<?php get_footer(); ?>

