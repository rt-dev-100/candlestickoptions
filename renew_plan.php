<?php

include_once 'wp-load.php';
//require_once('stripe-php/init.php');

global $stripe_options;

$input = @file_get_contents("php://input");
$event_json = json_decode($input);

$event_type = $event_json->type;
$stripe_customer_id = $event_json->data->object->customer;
$payment_status = $event_json->data->object->paid;
$current_period_start = $event_json->data->object->lines->data[0]->period->start;
$current_period_end = $event_json->data->object->lines->data[0]->period->end;


//testing : write in file to check if webhook is responsing here or not
$myfile = fopen("test.txt", "w");

fwrite($myfile, $input);
fclose($myfile);


$args = array(
        'meta_key' => 'stripe_customer_id',
        'meta_value' => $stripe_customer_id,
        'compare' => 'EXISTS',
    );
$user = get_users($args);
$user_id = $user[0]->data->ID;


if($user_id != '' && $event_type == 'invoice.payment_succeeded' && $payment_status == true) {
	update_user_meta($user_id, 'stripe_response', $event_json);
	update_user_meta($user_id, 'current_period_start', $current_period_start);
	update_user_meta($user_id, 'current_period_end', $current_period_end);
} else {
	update_user_meta($user_id, 'stripe_response', $event_json);
}


/*$event_type = $event_json->type;
$chargeId = $event_json->data->object->charge;
$subcriptionKey = $event_json->data->object->lines->data[0]->id;
$customerId = $event_json->data->object->customer;
$amoutDue = $event_json->data->object->amount_due;
$paidStatus = $event_json->data->object->paid;*/